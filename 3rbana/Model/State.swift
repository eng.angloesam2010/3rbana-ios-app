//
//  State.swift
//  بري و بحري
//
//  Created by Eng Angelo E. Saber  on 4/18/20.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation

public enum State {
    case loading
    case error
    case empty
    case noconnect
    case populated
}

public enum StatePopulate {
    case  empty
    case loading
    case populated
}


