//
//  IslandCell.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/24/20.
//

import UIKit
import Kingfisher
class IslandCell: UITableViewCell {
    
    var identifier = "IslandCell"
    var heightCell : CGFloat = 170.0
   
    @IBOutlet weak var kuwait: UIImageView!
    @IBOutlet weak var boatBackground: UIImageView!
    @IBOutlet weak var islandImageView : UIImageView!
    @IBOutlet weak var lessionImageImageView: UIImageView!
    @IBOutlet weak var islandBttn: UIButton!
    @IBOutlet weak var subjectNameLbl: UILabel!
    
    static var identifier: String {
        return String(describing: self)
    }
    
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configure(with islandModel : CatModel) {
//        var endPointUrl = EndPoint.pathImage(pathImage: islandModel.imagePath!)
//        self.islandImageView.kf.setImage(with: URL(string:endPointUrl.url), placeholder:UIImage(named: "icons3a"))
//        endPointUrl = EndPoint.pathImage(pathImage: islandModel.subjectImagePath!)
//        self.lessionImageImageView.kf.setImage(with: URL(string:endPointUrl.url),placeholder:UIImage(named: "island"))
//        self.islandBttn.setTitle(islandModel.nameAr, for: UIControl.State.normal)
//        if let SubjectName = islandModel.nameEn {
//            self.subjectNameLbl.text = "\(SubjectName)"
            
//        }
      
    }
    
}
