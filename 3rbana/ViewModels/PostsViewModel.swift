//
//  PostsViewModel.swift
//  3rbana
//
//  Created by Mac on 19/04/2021.
//

import Foundation
import UIKit
import Alamofire




public struct Post{
    var postId        : Int?
    var title         : String?
    var avatarName    : String?
    var isSpeical     : Int?
    var userId        : Int?
    var phone         : String?
    var createdDate   : String?
    var totalViews    : Int
    var price         : String?
}

// MARK: - Class for Model View
final class PostsViewModel : NSObject {
    var widthPostCollectV  : CGFloat {
        get{
            return  ( Util.screenWidth() / 2.0 - 10.0 )
        }
    }
    
    var heightPostCollectV  : CGFloat {
        get{
            return  250.0
        }
    }
    
    var postsArray          = [Post]()
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var delegate : CountryTableViewDelegate?
    var alertMessage : String? = ""  {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var governoratesId : String? = "0"
    
    var Url: String = ""
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    // MARK: - Fetch Data
    public func fetchPosts() {
        
        let params  = [ : ] as NSDictionary
        let headers: HTTPHeaders = [:]
        self.state  = .loading
        
        // MARK: - Request for api
        var endPoint  = EndPoint.fetchPostUrl(subSubCategoryId: "1")
        if self.governoratesId != "0" {
            endPoint = EndPoint.fetchPostUrlGoverID(subSubCategoryId: "1", governoratesId:governoratesId!)
        }
        self.Url     = endPoint.url
        print("\(self.Url)")
        self.apiService.fetchGenericData(url: Url , withParameters: params, httpMethod: .get, withHeader: headers ) {
            (response: ResultApi<postRootClass, APIServiceError>) in
            switch response {
            case .failure(let error):
                if error.localizedDescription == "unKnown" {
                    self.state        = .noconnect
                    self.alertMessage = (Session.Language == "en") ? INTERNET_ENG_MESSAGE : INTERNET_AR_MESSAGE
                }else{
                    self.state        = .error
                    self.alertMessage = error.localizedDescription
                }
                
                break
            case .success(let result):
                if let status = result.status {
                    if status {
                        if let body = result.body {
                            if !body.isEmpty {
                                self.state = .populated
                                self.appendGovernmente(body: body)
                            }else{
                                self.state        = .empty
                            }
                        }else{
                            self.state        = .empty
                        }
                    }else{
                        self.state        = .error
                        self.alertMessage = Login_Ar_Msg
                    }
                    
                }else{
                    self.state        = .error
                    self.alertMessage = Login_Ar_Msg
                }
                
                break
            }
        }
    }
    
    private func appendGovernmente(body : [postBody]?) {
        body?.forEach {
            let post = $0
            let pathPth = (post.images!.count > 0) ? post.images![0].thumbnailAvatarPath : ""
            var totalViewsInt = 0
            if let totalViews = post.totalViews {
                totalViewsInt = totalViews
            }
//            if !"\(post.totalViews)".isEmpty{
//                totalViews = post.totalViews!
//            }
            postsArray.append(Post.init(postId: post.postId, title: post.title, avatarName:pathPth, isSpeical: post.isSpeical, userId: post.userId, phone: post.phone, createdDate: post.createdDate,totalViews: totalViewsInt ,price: post.price))
        }
    }
    
    
    
}
extension PostsViewModel : UICollectionViewDelegate,UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.postsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:PostGridCell.identifier, for: indexPath) as! PostGridCell
        
        let titlePost   = self.postsArray[indexPath.row].title as String?
        let pricsStr    = self.postsArray[indexPath.row].price as String?
        var viewsNumStr = ""
        if self.postsArray[indexPath.row].totalViews > 0 {
            viewsNumStr = ("\(self.postsArray[indexPath.row].totalViews)" as String?)!
        }else{
            viewsNumStr = "0"
        }

        let teleStr     = self.postsArray[indexPath.row].phone           as String?
        let imgPath     = self.postsArray[indexPath.row].avatarName      as String?
        let isSpecial   = self.postsArray[indexPath.row].isSpeical
        let dateStr     = self.postsArray[indexPath.row].createdDate     as String?
        cell.configureCell(titlePost: titlePost!, pricsStr: pricsStr! , viewsNumStr: viewsNumStr, teleStr: teleStr!, imgPath:imgPath!,isSpeaical: isSpecial!,dateStr:dateStr!)
        
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: widthPostCollectV, height: heightPostCollectV)
    }
    
    
//        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//            //        let countryModel = self.goverArr[indexPath.row]
//            //        if let countryID : Int = self.goverArr[indexPath.row].goverId {
//            //            self.setSelectedCountryID(countryID)
//            //        }
//            //
//            //        self.delegate?.didSelectCell(countryID: countryModel.countryID!)
//        }
}

extension PostsViewModel : UITableViewDataSource ,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PostListCell.identifier , for: indexPath) as! PostListCell
        let titlePost   = self.postsArray[indexPath.row].title as String?
        let pricsStr    = self.postsArray[indexPath.row].price as String?
        let viewsNumStr = "\(self.postsArray[indexPath.row].totalViews)" as String?
        let teleStr     = self.postsArray[indexPath.row].phone           as String?
        let imgPath     = self.postsArray[indexPath.row].avatarName      as String?
        let isSpecial   = self.postsArray[indexPath.row].isSpeical
        let dateStr     = self.postsArray[indexPath.row].createdDate     as String?
        cell.configureCell(titlePost: titlePost!, pricsStr: pricsStr! , viewsNumStr: viewsNumStr!, teleStr: teleStr!, imgPath:imgPath!,isSpeaical: isSpecial!,dateStr:dateStr!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.postsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 275.0
    }
    
}




