//
//  CountryAvatar.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on March 8, 2021

import Foundation

struct CountryAvatar  : Codable {
    let avatarHeight : Int?
    let avatarId : Int?
    let avatarName : String?
    let avatarPath : String?
    let avatarSize : String?
    let avatarType : String?
    let avatarWidth : Int?
    let createdDate : String?
    let lowAvatarPath : String?
    let modifiedDate : String?
    let thumbnailAvatarPath : String?
    let uuid : String?
    enum CodingKeys: String, CodingKey {
         case avatarHeight = "avatarHeight"
            case avatarId = "avatarId"
            case avatarName = "avatarName"
            case avatarPath = "avatarPath"
            case avatarSize = "avatarSize"
            case avatarType = "avatarType"
            case avatarWidth = "avatarWidth"
            case createdDate = "createdDate"
            case lowAvatarPath = "lowAvatarPath"
            case modifiedDate = "modifiedDate"
            case thumbnailAvatarPath = "thumbnailAvatarPath"
            case uuid = "uuid"
    }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                avatarHeight = try values.decodeIfPresent(Int.self, forKey: .avatarHeight)
                avatarId = try values.decodeIfPresent(Int.self, forKey: .avatarId)
                avatarName = try values.decodeIfPresent(String.self, forKey: .avatarName)
                avatarPath = try values.decodeIfPresent(String.self, forKey: .avatarPath)
                avatarSize = try values.decodeIfPresent(String.self, forKey: .avatarSize)
                avatarType = try values.decodeIfPresent(String.self, forKey: .avatarType)
                avatarWidth = try values.decodeIfPresent(Int.self, forKey: .avatarWidth)
                createdDate = try values.decodeIfPresent(String.self, forKey: .createdDate)
                lowAvatarPath = try values.decodeIfPresent(String.self, forKey: .lowAvatarPath)
                modifiedDate = try values.decodeIfPresent(String.self, forKey: .modifiedDate)
                thumbnailAvatarPath = try values.decodeIfPresent(String.self, forKey: .thumbnailAvatarPath)
                uuid = try values.decodeIfPresent(String.self, forKey: .uuid)
        }

}
