//
//  UITableViewCell.swift
//  3rbana
//
//  Created by Mac on 09/04/2021.
//

import Foundation
import UIKit
extension UITableViewCell {
    func clearCellColor() {
        self.selectionStyle = .default
        self.selectedBackgroundView?.backgroundColor = UIColor.clear
    }
}
