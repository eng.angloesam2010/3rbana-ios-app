//
//  SubCategoryVC.swift
//  3rbana
//
//  Created by Mac on 14/04/2021.
//

import Foundation
import UIKit
import AnimatableReload
import EmptyKit
import CFAlertViewController


class SubCategoryVC : UIViewController,BackNavigationDelegate,CategoryProtocal {
    var flag : Int?
    @IBOutlet weak var mainTableView: UITableView!{
        didSet {
            self.mainTableView?.register(UINib(nibName: SliderCell.identifier, bundle: nil), forCellReuseIdentifier: SliderCell.identifier)
        }
    }
  
    var categoryID : Int?
    // MARK: - Property of view controller
    @IBOutlet weak var navBarView: BackNavigationBar!
    // MARK: - init for Level VC
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        reloadTableView()
        setPropertyTitleForNav()
    }
    // MARK: - set property of custome navigator
    fileprivate func setPropertyTitleForNav() {
        self.navBarView?.delegateProfile = self  // set delegate for Navigator bar
    }
    // MARK: - init Collectionview
    func initTableView() {
        self.mainTableView?.register(UINib(nibName: SliderCell.identifier, bundle: nil), forCellReuseIdentifier: SliderCell.identifier)
        self.mainTableView.rowHeight = UITableView.automaticDimension
        self.mainTableView.estimatedRowHeight = self.mainTableView.rowHeight
        self.mainTableView.separatorColor     = UIColor.clear


    }
    // MARK: - Reload Collectionview
    func reloadTableView() {
        initTableView()
        self.mainTableView?.dataSource = self
        self.mainTableView?.delegate = self
        self.mainTableView?.reloadData()
        self.startAnimation()
        
    }
    // MARK: - Start Animation for view controller
    func startAnimation() {
        AnimatableReload.reload(tableView: self.mainTableView, animationDirection: "down")
    }
    

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
   
    
    
    func backViewController() {
        navigationController?.popViewController(animated: true)
    }
    
    func notificationViewController() {
        self.pushViewController(id: 6)
    }
    
    
    func didSelectIndex(id: Int, categoryID: Int, flag: Int) {
        self.pushViewController(id: id, categoryID: categoryID,flag:flag)
    }
}


