//
//  GoldGridVC.swift
//  3rbana
//
//  Created by Mac on 28/04/2021.
//

import UIKit
import AnimatableReload
import EmptyKit
class GoldGridVC : UIViewController , EmptyDataSourceProtocal{
    var typeInt: Int = 0
    @IBOutlet weak var postCollectionView: UICollectionView!
    lazy var postListViewModel : AdvertViewModel = {
        return AdvertViewModel()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        initCollectionView()
        startFetchPost()
        NotificationCenter.default.addObserver(self, selector: #selector(startFetchObserver),
                                               name: Notification.Name("startFetchObserver"), object: nil)
    }
    
    
    @objc func startFetchObserver(notification: NSNotification){
        if let governmentId = notification.userInfo?["goverSelectedIndex"] as? Int {
            self.postListViewModel.governoratesId = "\(governmentId)"
            startFetchPost()
        }
    }
    
    func initCollectionView(){
        // Do any additional setup after loading the view, typically from a nib.
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:5.0, left: 5.0, bottom: 5.0, right: 5.0)
        layout.itemSize = CGSize(width:self.postListViewModel.widthPostCollectV , height: self.postListViewModel.heightPostCollectV)
        layout.minimumInteritemSpacing = 5.0
        layout.minimumLineSpacing      = 5.0
        self.postCollectionView!.collectionViewLayout = layout
    }
    
}
extension GoldGridVC {
    // MARK: - Fetch Level Part
    func startFetchPost(){
        // MARK: - when colusre was updaete alert will be shown
        
        self.postListViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.postListViewModel.state {
                case  .error:
                    self.typeInt = 2
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                case .empty :
                    self.typeInt = 4
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    DispatchQueue.main.async {
                        self.reloadTableView()
                    }
                case .noconnect:
                    self.typeInt = 1
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                }
            }
        }
        
        self.postListViewModel.fetchPosts()
        
    }
    // MARK: - Reload Collectionview
    func reloadTableView() {
        self.postCollectionView?.dataSource = self.postListViewModel
        self.postCollectionView?.delegate   = self.postListViewModel
        self.postCollectionView?.reloadData()
        self.startAnimation()
        
    }
    
    // MARK: - Set datasource for collectionview
    func setEmptyDataSet(){
        self.postCollectionView?.dataSource = nil
        self.postCollectionView?.delegate   = nil
        self.postCollectionView?.reloadData()
        self.postCollectionView.ept.dataSource = self as EmptyDataSource
        self.postCollectionView.ept.delegate   = self as EmptyDelegate
        self.postCollectionView.ept.reloadData()
        self.postCollectionView.alwaysBounceVertical = false
        self.postCollectionView.isScrollEnabled = false
        self.postCollectionView.isPagingEnabled = false
        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
        
    }
    
    // MARK: - Start Animation for view controller
    func startAnimation(){
        AnimatableReload.reload(collectionView:self.postCollectionView, animationDirection: "left")
    }
    
    
    func didSelectCell(countryID: Int) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let tbc   = storyBoard.instantiateViewController(withIdentifier:"tabbar") as! UITabBarController
        tbc.selectedIndex = 0
        tbc.modalPresentationStyle = .fullScreen
        self.present(tbc, animated: true, completion:nil)
        
    }
    
    
    @objc(emptyButton:tappedIn:) func emptyButton(_ button: UIButton, tappedIn view: UIView) {
        self.startFetchPost()
    }

}
