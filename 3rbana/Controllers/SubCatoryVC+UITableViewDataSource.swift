//
//  SubCatoryVC+UITableViewDataSource.swift
//  3rbana
//
//  Created by Mac on 14/04/2021.
//

import UIKit
import Alamofire

// MARK: - extension for TableView to manage datasource and delegate
extension SubCategoryVC : UITableViewDataSource,UITableViewDelegate,ReloadCellDelegate,StoryProtocal {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SliderCell.identifier , for: indexPath) as! SliderCell
            cell.fetchSlider()
            cell.delegate = self
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CategoryTableViewCell.identifier , for: indexPath) as! CategoryTableViewCell
            if let pointer = flag {
                cell.flag = pointer
            }
            cell.startFetchCategory()
            cell.delegate = self
            cell.delegateCategory = self
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 280.0
        }
        return UITableView.automaticDimension
    }
    
    func reloadCell(){
        self.mainTableView.beginUpdates()
        self.mainTableView.endUpdates()
    }
    
    func navigateStoryScreen(){
        Session.start()
        if  Session.Username.isEmpty {
            self.tabBarController?.selectedIndex = 4
        }else{
            self.pushViewController(id: 10)
        }
    }
}

