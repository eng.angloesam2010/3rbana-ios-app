//
//  UIDevice.swift
//  al_Jalboot_app
//
//  Created by Mac on 12/24/20.
//

import Foundation
import UIKit

public extension UIDevice {

   class var isPhone: Bool {
       return UIDevice.current.userInterfaceIdiom == .phone
   }

   class var isPad: Bool {
       return UIDevice.current.userInterfaceIdiom == .pad
   }

   class var isTV: Bool {
       return UIDevice.current.userInterfaceIdiom == .tv
   }

   class var isCarPlay: Bool {
       return UIDevice.current.userInterfaceIdiom == .carPlay
   }
}
