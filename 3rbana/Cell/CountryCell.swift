//
//  CountryCell.swift
//  3rbana
//
//  Created by Mac on 19/01/2021.
//

import UIKit

class CountryCell : UICollectionViewCell {
    var orderCellIdentifier = "CountryCell"
    @IBOutlet weak var ctryLogo: UIImageView!
    @IBOutlet weak var ctryTitleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    static var identifier: String {
          return String(describing: self)
    }
    
    func configureCell(model:CountryListModel)  {
        let endPointUrl = EndPoint.pathImage(pathImage: model.countryLogoPath!)
        self.ctryLogo.kf.setImage(with: URL(string:endPointUrl.url),placeholder:UIImage(named: "new_logo"))
        if let countryEnName = model.countryEnName , let  countryArName = model.countryArName {
             self.ctryTitleLbl.text = (Session.Language == "en") ? countryEnName : countryArName
        }

    }
      

}
