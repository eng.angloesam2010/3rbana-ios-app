

import Foundation
struct CatItem : Codable {
    let categoryId : Int?
	let name : String?
	let arabicName : String?
	let isActive : Bool?
    let avatar : CountryAvatar?

	enum CodingKeys: String, CodingKey {
		case categoryId = "categoryId"
		case name = "description"
		case arabicName = "arabicName"
		case isActive = "isActive"
		case avatar = "avatar"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		categoryId = try values.decodeIfPresent(Int.self, forKey: .categoryId)
		name = try values.decodeIfPresent(String.self, forKey: .name)
		arabicName = try values.decodeIfPresent(String.self, forKey: .arabicName)
		isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        if let avatar =  try values.decodeIfPresent(CountryAvatar.self, forKey: .avatar) {
            self.avatar = avatar
        }else {
            self.avatar = nil
        }
	}

}
