//
//  RootClass.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on November 30, 2020
//
import Foundation

struct LessionsRoot: Codable {

	let lessons : [Lessons]
	let LessonsCount: Int

	private enum CodingKeys: String, CodingKey {
		case lessons = "Lessons"
		case LessonsCount = "LessonsCount"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
        lessons = try values.decode([Lessons].self, forKey: .lessons)
		LessonsCount = try values.decode(Int.self, forKey: .LessonsCount)
	}


}
