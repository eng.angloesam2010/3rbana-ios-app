//
//  PhotoCell.swift
//  3rbana
//
//  Created by Mac on 08/05/2021.
//

import UIKit

class PhotoCell: UICollectionViewCell {
    static let identifier = "PhotoCell"
    @IBOutlet weak var imgView   : UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
