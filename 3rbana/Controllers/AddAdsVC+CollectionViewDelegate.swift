//
//  AddAdsVC+CollectionViewDelgate.swift
//  3rbana
//
//  Created by Mac on 08/05/2021.
//

import Foundation
import UIKit
import BSImagePicker
import Photos

extension AddAdsVC:UICollectionViewDelegate,UICollectionViewDataSource {
    
    func initCollectionView(){
        self.photoCollection?.dataSource = self
        self.photoCollection?.delegate   = self
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return (self.evenAssets.count == 0) ? 1 :  ((self.evenAssets.count < 5) ? self.evenAssets.count + 1 : 5)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: PhotoCell.identifier , for: indexPath) as? PhotoCell {
            if self.evenAssets.count <= indexPath.row {
                cell.imgView.image = self.evenAssets[indexPath.row].
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        openGalleryAndCamera()
    }
    
    
    func reloadCollectionView(){
        self.photoCollection.reloadData()
    }
    
    func increaeRowsNumCollectionView(){
        if rowsNumCollectionView < 5 {
            rowsNumCollectionView = rowsNumCollectionView + 1
        }
    }
    
    func decreaseRowsNumCollectionView(){
        if rowsNumCollectionView < 5 {
            rowsNumCollectionView = rowsNumCollectionView - 1
        }
    }
    
    func addPhotoTotCell(){
        if let collectionView = self.photoCollection,
//           let indexPath = collectionView.indexPathsForSelectedItems?.first,
//           let cell = collectionView.cellForItem(at: indexPath) as? PhotoCell{
//            // add photo
////            cell.imgView.image = selectedImage
////            self.increaeRowsNumCollectionView()
            self.reloadCollectionView()
//        }
        
    }
    
    
    
}
