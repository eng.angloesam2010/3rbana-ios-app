
import Foundation
import UIKit


protocol BackNavigationDelegate {
    func backViewController()
    func notificationViewController()
}


final class BackNavigationBar : UIView{
    
    private static let NIB_NAME = "BackNavigationBar"
    @IBOutlet private var view: UIView!
    var delegateProfile : BackNavigationDelegate? 
    
    @IBOutlet weak var backBttn: UIButton! {
        didSet {
            backBttn.setImage((Session.Language == "en" ? UIImage(named: "left-arrow 7") :  UIImage(named: "right-arrow 7") ), for: .normal)
        }
    }
    
    override func awakeFromNib() {
        initWithNib()
       
    }
 
    @IBAction func popViewController(_ sender: Any) {
         delegateProfile?.backViewController()
      
    }
    
    @IBAction func noificationViewController(_ sender: Any) {
        delegateProfile?.notificationViewController()
    }
    private func initWithNib() {
        Bundle.main.loadNibNamed(BackNavigationBar.NIB_NAME, owner: self, options: nil)
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        setupLayout()
        
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate(
            [
                view.topAnchor.constraint(equalTo: topAnchor),
                view.leadingAnchor.constraint(equalTo: leadingAnchor),
                view.bottomAnchor.constraint(equalTo: bottomAnchor),
                view.trailingAnchor.constraint(equalTo: trailingAnchor),
            ]
        )
    }
}

