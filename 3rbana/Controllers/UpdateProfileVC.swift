//
//  ProfileVC.swift
//  3rbana
//
//  Created by Mac on 29/03/2021.
//

import UIKit
import AYPopupPickerView
import Kingfisher

class UpdateProfileVC : UIViewController{
    
    // MARK: - image picker
    var picker = UIImagePickerController();
    var alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
    var viewController = UIImagePickerController()
    var pickImageCallback : ((UIImage) -> ())?;
    var imgRequest : ImageRequest!
    // MARK: - image picker
    var countryArr : [String] = []
    @IBOutlet weak var BackNavigationBar: BackNavigationBar!
    var registerModel : RegisterModel?
    @IBOutlet weak var profileImageView: UIImageView!
    
    lazy var countryListViewModel : CountryViewModel = {
        return CountryViewModel()
    }()
    
    //@IBOutlet weak var profileImageView : UIImageView!
    
    lazy var updateViewModel : UpdateProfileViewModel = {
        return UpdateProfileViewModel()
    }()
    @IBOutlet weak var profileImge: UIImageView!
    
    var doneStr : String!{
        get {
            if Session.Language == "en" {
                return "done".localizableString(loc:"en")
                
            }else{
                return "done".localizableString(loc:"ar-EG")
            }
        }
    }
    
    var cancelStr: String!{
        get {
            if Session.Language == "en" {
                return "cancel".localizableString(loc:"en")
                
            }else{
                return "cancel".localizableString(loc:"ar-EG")
            }
        }
    }
    
    var countryFlag = 0
    var typeInt: Int = 0
    @IBOutlet weak var titleStrLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.titleStrLbl.text = "ChangePassword".localizableString(loc:"en")
                self.titleStrLbl.font = AppTheme.FontArabic(fontSize: 28.0)
            }else{
                self.titleStrLbl.font = AppTheme.FontArabic(fontSize: 28.0)
                self.titleStrLbl.text = "ChangePassword".localizableString(loc:"ar-EG")
            }
            self.titleStrLbl.textColor = AppTheme.setGoldenColor()
        }
    }
    @IBOutlet weak var emailTxtField: UITextField!{
        didSet {
            if Session.Language == "en" {
                self.emailTxtField.attributedPlaceholder = self.emailTxtField.setPlaceholder(string: "Email".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.emailTxtField.textAlignment = .left
            }else{
                self.emailTxtField.attributedPlaceholder = self.emailTxtField.setPlaceholder(string: "Email".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.emailTxtField.textAlignment = .right
            }
            self.emailTxtField.textColor = AppTheme.setGoldenColor()
            self.emailTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    
    @IBOutlet weak var userNameTxtField : UITextField!{
        didSet {
            if Session.Language == "en" {
                self.userNameTxtField.attributedPlaceholder = self.userNameTxtField.setPlaceholder(string: "UserName".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.userNameTxtField.textAlignment = .left

            }else{
                self.userNameTxtField.attributedPlaceholder = self.userNameTxtField.setPlaceholder(string: "UserName".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.userNameTxtField.textAlignment = .right
            }
            self.userNameTxtField.textColor = AppTheme.setGoldenColor()
            self.userNameTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    
    
    @IBOutlet weak var addressTxtField: UITextField!{
        didSet {
            if Session.Language == "en" {
                self.addressTxtField.attributedPlaceholder = self.addressTxtField.setPlaceholder(string: "address".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.addressTxtField.textAlignment = .left
              
            }else{
                
                self.addressTxtField.attributedPlaceholder = self.addressTxtField.setPlaceholder(string: "address".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.addressTxtField.textAlignment = .right
            }
            self.addressTxtField.textColor = AppTheme.setGoldenColor()
            self.addressTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    
    @IBOutlet weak var telephoneTxtField: UITextField!{
        didSet {
            if Session.Language == "en" {
                self.telephoneTxtField.attributedPlaceholder = self.telephoneTxtField.setPlaceholder(string: "Telep".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                
                self.telephoneTxtField.textAlignment = .left
        
            }else{
                
                self.telephoneTxtField.attributedPlaceholder = self.telephoneTxtField.setPlaceholder(string: "Telep".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.telephoneTxtField.textAlignment = .right
            }
            self.telephoneTxtField.textColor = AppTheme.setGoldenColor()
            self.telephoneTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    @IBOutlet weak var updateBttn: UIButton!{
        didSet {
            self.updateBttn.setTitleColor(AppTheme.setBlackColor(), for: UIControl.State.normal)
            if Session.Language == "en"{
                self.updateBttn.setTitle("edit_bttn".localizableString(loc:"en"), for: UIControl.State.normal)
                self.updateBttn.titleLabel?.font = AppTheme.FontArabic(fontSize: 20.0)
                
            }
            else{
                self.updateBttn.titleLabel?.font = AppTheme.FontArabic(fontSize: 20.0)
                self.updateBttn.setTitle("edit_bttn".localizableString(loc:"ar-EG"), for: UIControl.State.normal)
            }
        }
    }
    @IBOutlet weak var countryBttn: UIButton!{
        didSet {
            //  self.countryBttn.setTitleColor(AppTheme.setGoldenColor(), for: UIControl.State.normal)
            self.countryBttn.setTitleColor(AppTheme.setGoldenColor(), for: UIControl.State.normal)
            if Session.Language == "en"{
                self.countryBttn.setTitle("Country".localizableString(loc:"en"), for: UIControl.State.normal)
            }
            else{
                self.countryBttn.setTitle( "Country".localizableString(loc:"ar-EG"), for: UIControl.State.normal)
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        startFetchCountry()
        self.BackNavigationBar.delegateProfile = self
    }
    
  
    @IBAction func showPickerView(_ sender: Any) {
        dismissKeyboard()
        let popupPickerView = AYPopupPickerView()
        popupPickerView.headerView.backgroundColor = .black
        popupPickerView.doneButton.setTitle(doneStr, for: .normal)
        popupPickerView.cancelButton.setTitle(cancelStr, for: .normal)
        popupPickerView.doneButton.setTitleColor(AppTheme.setGoldenColor(), for: .normal)
        popupPickerView.cancelButton.setTitleColor(AppTheme.setGoldenColor(),for: .normal)
        // popupPickerView.backgroundColor  = .black
        //        pop
        popupPickerView.display(itemTitles: countryArr, doneHandler: { [self] in
            let selectedIndex  = popupPickerView.pickerView.selectedRow(inComponent: 0)
            self.countryBttn.setTitle("\(self.countryArr[selectedIndex])", for: .normal)
            self.countryFlag = self.countryListViewModel.getSelectedCountryID(countryNameStr: "\(self.countryArr[selectedIndex])")
        })
    }
    
    @IBAction func openGalleryCamera(_ sender: Any) {
        self.openGalleryAndCamera()
    }
    func loadProfileData() {
        Session.start()
        print(Session.UserMail)
        if let name  = Session.Username as String? {
            self.userNameTxtField.text = String("\(name)")
        }
        if let Phone = Session.Phone as String?{
            self.telephoneTxtField.text = Phone
        }
        if let address = Session.address as String?{
            self.addressTxtField.text = address
        }
        if let emailStr = Session.UserMail as String?{
            self.emailTxtField.text = emailStr
        }
        if let CountryID = Session.CutID as String?{
            countryFlag = (CountryID as NSString).integerValue
            self.countryBttn.setTitle("\(self.countryListViewModel.getCountryName(countryID : countryFlag))", for: .normal)
        }
        if let avatar = Session.avatar as String?  {
            let endPointUrl = EndPoint.pathImage(pathImage:avatar)
//            print("\()")
            self.profileImge.kf.setImage(with: URL(string:endPointUrl.url),placeholder:UIImage(named: "new_logo"))
        }
        Session.save()
    }
    
    
    
    
    
    
    @IBAction func updateProfile(_ sender: Any) {
        dismissKeyboard()
        Session.start()
        self.updateViewModel.registerModel = RegisterModel(password: Session.password, name: self.userNameTxtField.text! , email:self.emailTxtField.text!, phone: self.telephoneTxtField.text! , countryId: "\(countryFlag)", block: "1", avenue:  "1", street:  "1", houseNumber:  "1", avatar:  "1", confirmPass: "",accessToken:Session.accessToken,refreshToken:Session.refreshToken)


        // MARK: - when colusre was updaete alert will be shown
        self.updateViewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.updateViewModel.alertMessage {
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    let alert = AlertActionManager.Error
                    alert.getAlert(errorDescribe: message)
                }
            }
        }

        self.updateViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.updateViewModel.state {
                case .empty, .noconnect:
                    DispatchQueue.main.async {
                        if let message = self.updateViewModel.alertMessage {
                            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                            let alert = AlertActionManager.Error
                            alert.getAlert(errorDescribe: message)
                        }
                    }
                case .error :
                    DispatchQueue.main.async {
                        if let message = self.updateViewModel.alertMessage {
                            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                            let alert = AlertActionManager.Error
                            alert.getAlert(errorDescribe: message)
                        }
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    if let message = self.updateViewModel.alertMessage {
                        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                        let alert = AlertActionManager.succes
                        alert.getAlert(errorDescribe: message)
                        self.pushViewController(id: 6)
                    }
                    
                    
                }
            }
        }
        self.updateViewModel.updateProfile()
    }
}

extension UpdateProfileVC {
    
    // MARK: - Fetch Level Part
    func startFetchCountry(){
        // MARK: - when colusre was updaete alert will be shown
        self.countryListViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.countryListViewModel.state {
                case .empty, .error:
                    self.typeInt = 2
                    if let message = self.countryListViewModel.alertMessage {
                        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                        let alert = AlertActionManager.Error
                        alert.getAlert(errorDescribe: message)
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    DispatchQueue.main.async {
                        self.fillArrayCountry()
                        self.loadProfileData()
                    }
                case .noconnect:
                    self.typeInt = 1
                    DispatchQueue.main.async {
                        
                        
                    }
                }
            }
        }
        
        self.countryListViewModel.fetchCountry()
        
    }
    
    // MARK: fill array country
    func fillArrayCountry() {
        for i in 0..<self.countryListViewModel.countryArr.count {
            let country = self.countryListViewModel.countryArr[i]
            if let name = country.countryEnName{
                countryArr.append(name)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let x = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
            return true
        } else {
            return false
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    
}

