//
//  postBody.swift
//  3rbana
//
//  Created by Mac on 19/04/2021.
//

import Foundation

struct postBody : Codable {
    let createdDate : String?
    let postId : Int?
    let title : String?
    let images : [postImage]?
    let isSpeical : Int?
    let phone : String?
    let price : String?
    let totalViews : Int?
    let userId : Int?

    enum CodingKeys: String, CodingKey {
        case createdDate = "createdDate"
        case title       = "title"
        case price       = "price"
        case phone       = "phone"
        case totalViews  = "totalViews"
        case images      = "images"
        case isSpeical   = "isSpeical"
        case postId      = "postId"
        case userId      = "userId"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        createdDate = try values.decode(String.self, forKey: .createdDate)
        title = try values.decode(String.self, forKey: .title)
        price = try values.decode(String.self, forKey: .price)
        phone = try values.decode(String.self, forKey: .phone)
        if let totalViews = try? values.decode(Int.self, forKey: .totalViews){
            self.totalViews = totalViews
        }else{
            self.totalViews = nil
        }
        if let images = try? values.decode([postImage].self, forKey: .images){
            self.images = images
        }else{
            self.images = nil
        }
        isSpeical = try values.decode(Int.self, forKey: .isSpeical)
        postId = try values.decode(Int.self, forKey: .postId)
        userId = try values.decode(Int.self, forKey: .userId)
    }
    
}

