//
//  Avatar.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on January 30, 2021

import Foundation

struct Avatar : Codable {
    
    let thumbnailAvatarPath : String?
    init(thumbnailPath: String) {
        self.thumbnailAvatarPath = thumbnailPath
    }
    
    enum CodingKeys: String, CodingKey {
        case thumbnailAvatarPath = "thumbnailAvatarPath"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        thumbnailAvatarPath = try values.decodeIfPresent(String.self, forKey: .thumbnailAvatarPath)
        
    }
    
}

