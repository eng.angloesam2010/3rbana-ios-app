//
//  UserCell.swift
//  3rbana
//
//  Created by Bilal on 26/04/2021.
//

import UIKit

class UserCell: UICollectionViewCell {
    
    static let identifier = "UserCell"
    
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var imageContainer: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var userName: UILabel!
    
    
    @IBOutlet weak var indicator: UIView!
    
    
    
    
}
