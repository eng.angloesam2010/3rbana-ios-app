//
//  SubjectViewModel.swift
//  al_Jalboot_app
//
//  Created by Mac on 12/17/20.
//

import Foundation
import UIKit
import Alamofire


// MARK: - Class for Model View
final class CategoryViewModel : NSObject {
    var catCellModel        = [CatModel]()
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var alertMessage : String? = ""  {
        didSet {
            self.showAlertClosure?()
        }
    }
    var Url: String = ""
    var rootID : String! = ""
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    // MARK: - Fetch Data
    public func fetcCategory() {
        let params  = [ : ] as NSDictionary
        let headers: HTTPHeaders = [:]
        self.state  = .loading
        
        var endPoint = EndPoint.catUrl
        print(self.rootID)
        if let root = self.rootID as String? {
            if rootID! == "1" {
                endPoint = EndPoint.subCatUrl(ID:"")
            }else if rootID! == "2" {
                endPoint =  EndPoint.subSuCatUrl(ID:"")
            }
        }
 

//        
        self.Url = endPoint.url
        print("\(self.Url)")
        self.apiService.fetchGenericData(url: Url , withParameters: params, httpMethod: .get, withHeader: headers ) {
            (response: ResultApi<Cat, APIServiceError>) in
            switch response {
            case .failure(let error):
                if error.localizedDescription == "unKnown" {
                    self.state        = .noconnect
                    self.alertMessage = (Session.Language == "en") ? INTERNET_ENG_MESSAGE : INTERNET_AR_MESSAGE
                }else{
                    self.state        = .error
                    self.alertMessage = error.localizedDescription
                    print("\(error.localizedDescription)")
                    
                }
                
                break
            case .success(let result):
                if let status = result.status {
                    if status {
                        self.state = .populated
                        self.appendCatModelToCellCatModel(catModel: result)
                        print("\(self.catCellModel)")
                        
                    }else{
                        self.state        = .error
                        self.alertMessage = Login_Ar_Msg
                    }
                    
                }else{
                    self.state        = .error
                    self.alertMessage = Login_Ar_Msg
                }
                
                break
            }
            
            
        }
        
        
    }
    private func appendCatModelToCellCatModel(catModel : Cat) {
        if let catItem = catModel.catItem {
            for cat in catItem {
                //                print("\(cat.avatar?.thumbnailAvatarPath)")
                self.catCellModel.append(CatModel.init(ID: cat.categoryId, imagePath: cat.avatar?.thumbnailAvatarPath, nameAr: cat.arabicName, nameEn: cat.name))
            }
        }
    }
    
    
}
