/* 
Copyright (c) 2020 Swift Models Generated from JSON powered by http://www.json4swift.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar

*/

import Foundation
struct VideosModel : Codable {
	let stage : String?
	let subjectName : String?
	let subjectID : Int?
	let subjectImage : String?
	let islandImage : String?
    let DescriptionAR : String?
	let islandNameAR : String?
	let islandNameEN : String?
	let lessonURL : String?

	enum CodingKeys: String, CodingKey {

		case stage = "Stage"
		case subjectName = "SubjectName"
		case subjectID = "SubjectID"
		case subjectImage = "SubjectImage"
        case DescriptionAR = "DescriptionAR"
		case islandImage = "IslandImage"
		case islandNameAR = "IslandNameAR"
		case islandNameEN = "IslandNameEN"
		case lessonURL = "LessonURL"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		stage = try values.decodeIfPresent(String.self, forKey: .stage)
		subjectName = try values.decodeIfPresent(String.self, forKey: .subjectName)
		subjectID = try values.decodeIfPresent(Int.self, forKey: .subjectID)
		subjectImage = try values.decodeIfPresent(String.self, forKey: .subjectImage)
		islandImage = try values.decodeIfPresent(String.self, forKey: .islandImage)
        DescriptionAR = try values.decodeIfPresent(String.self, forKey: .DescriptionAR)
		islandNameAR = try values.decodeIfPresent(String.self, forKey: .islandNameAR)
		islandNameEN = try values.decodeIfPresent(String.self, forKey: .islandNameEN)
		lessonURL = try values.decodeIfPresent(String.self, forKey: .lessonURL)
	}

}
