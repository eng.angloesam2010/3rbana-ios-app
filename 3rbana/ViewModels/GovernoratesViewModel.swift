//
//  GovernoratesViewModel.swift
//  3rbana
//
//  Created by Mac on 18/04/2021.
//

import Foundation
import UIKit
import Alamofire

let widthGoverCollView = 179.0
let heightGoverCollView = 45.0

public struct Governorates{
    var countryId     : Int?
    var goverId       : Int?
    var arName        : String?
    var enName        : String?
}


// MARK: - Class for Model View
final class GovernoratesViewModel : NSObject {
    //let cellWidth           =  Util.screenWidth() / 2.0
    var goverArr            = [Governorates]()
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var delegate : CountryTableViewDelegate?
    var alertMessage : String? = ""  {
        didSet {
            self.showAlertClosure?()
        }
    }
    var Url: String = ""
  
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    // MARK: - Fetch Data
    public func fetchGovernment() {
       
        let params  = [ : ] as NSDictionary
        let headers: HTTPHeaders = [:]
        self.state  = .loading
        
        // MARK: - Request for api
        Session.start()
        Session.getSelectedCountry()
//        if (Session.Username.isEmpty) {
        let endPoint = EndPoint.governorateUrl(countryId: "\(Session.SelectedCountry)")
            self.Url     = endPoint.url
            print("\(self.Url)")
            self.apiService.fetchGenericData(url: Url , withParameters: params, httpMethod: .get, withHeader: headers ) {
                (response: ResultApi<Governorate, APIServiceError>) in
                switch response {
                case .failure(let error):
                    if error.localizedDescription == "unKnown" {
                        self.state        = .noconnect
                        self.alertMessage = (Session.Language == "en") ? INTERNET_ENG_MESSAGE : INTERNET_AR_MESSAGE
                    }else{
                        self.state        = .error
                        self.alertMessage = error.localizedDescription
                    }
                  
                    break
                case .success(let result):
                    if let status = result.status {
                        if status {
                        self.state = .populated
                            if let body = result.body {
                                self.appendGovernmente(body: body)
                            }
                       }else{
                            self.state        = .error
                            self.alertMessage = Login_Ar_Msg
                      }
                       
                    }else{
                        self.state        = .error
                        self.alertMessage = Login_Ar_Msg
                    }

                    break
                }
            }

//
    
    
    }
    
    private func appendGovernmente(body : [GovernorateBody]) {
        print("\(body)")
        body.forEach {
            let goverItem = $0
            print("\(goverItem.arabicName)")
            goverArr.append(Governorates.init(countryId: goverItem.countryId, goverId: goverItem.governoratesId, arName: goverItem.arabicName, enName: goverItem.name))
        }
      }

}
extension GovernoratesViewModel : UICollectionViewDelegate,UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.goverArr.count + 1
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:GovenCell.identifier, for: indexPath) as! GovenCell
        if indexPath.row == 0 {
            let governStr = ( Session.Language == "en" ) ?  allGoverEN : allGoverAR
            cell.setCellConfigure(governStr:governStr)
        }else{
            let governStr = ( Session.Language == "en" ) ? self.goverArr[indexPath.row - 1].enName! : self.goverArr[indexPath.row - 1 ].arName!
            cell.setCellConfigure(governStr: governStr)
        }
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return  CGSize(width:widthGoverCollView, height:  heightGoverCollView)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("Clicked ")
        if indexPath.row == 0 {
            let goverSelectedIndex : [String: Int?] = ["goverSelectedIndex": 0]
            NotificationCenter.default.post(name: Notification.Name("refetchGovernmente"), object: nil,userInfo: goverSelectedIndex as [AnyHashable : Any])
        }else{
            let goverSelectedIndex : [String: Int?] = ["goverSelectedIndex": self.goverArr[indexPath.row-1].goverId]
            NotificationCenter.default.post(name: Notification.Name("refetchGovernmente"), object: nil,userInfo: goverSelectedIndex as [AnyHashable : Any])
        }
        
        


    }
    func setSelectedCountryID(_ CountryID : Int) -> Void {
        Session.start()
        Session.SelectedCountry = CountryID
        Session.setSelectedCountry()
    }
    
    func getWidthCollectionView() -> Double {
        return (widthGoverCollView * Double(self.goverArr.count)) + 50.0
    }
    
}

