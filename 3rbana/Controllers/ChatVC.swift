//
//  ChatVC.swift
//  3rbana
//
//  Created by Mac on 09/03/2021.
//


import Foundation
import UIKit
import SDWebImage
import Combine
import Firebase
import AgoraRtcKit


class ChatViewModel {
    enum State {
        case isLoading
        case failed(String)
        case loaded([UserItemViewModel])
    }
    
    @Published private(set) var state: State = .isLoading
    private var cancellables: [AnyCancellable] = []
    private lazy var ref: DatabaseReference = Database.database().reference()
    private var users: [UserItemViewModel] = []
    
    
    init() {
        loadUsersFromFirebase()
    }
    
    
    fileprivate func loadUsersFromFirebase() {
        let userRef = ref.child("users")
        userRef.observe(.value) { (snapshot) in
            self.users.removeAll()
            var liveUsers: [UserItemViewModel] = []
            for (index, child) in snapshot.children.enumerated() {
                if let childSnapshot = child as? DataSnapshot {
                    if let childDict = childSnapshot.value as? [String: Any] {
                        let name = childDict["name"] as? String ?? ""
                        let isLive = childDict["isLive"] as? Bool ?? false
                        let imagePath = childDict["image"] as? String ?? ""
                        let meetingId = childDict["currentLive"] as? String ?? ""
                        if isLive {
                            
                            liveUsers.append(.init(user: .init(name: name, isActive: isLive, imagePath: imagePath), meetingId: meetingId))
                        }else {
                            self.users.append(.init(user: .init(name: name, isActive: isLive, imagePath: imagePath), meetingId: meetingId))
                        }
                       
                        if index == snapshot.children.allObjects.count - 1 {
                            for user in liveUsers {
                                self.users.append(user)
                            }
                            if liveUsers.count > 0 {
                                self.users.reverse()
                            }
                            let avatarImage = self.users.first(where: {$0.name == Session.Username})?.avatarPath?.absoluteString ?? ""
                            print("Avatar Image is \(avatarImage)")
                            Session.keyChain["avatar"] = avatarImage
                            Session.avatar = avatarImage
                            self.users.removeAll(where: {$0.name == Session.Username})
                            
                            self.state = .loaded(self.users)
                        }
                        
                    }
                }else {
                    self.state = .failed("Unable to get the users")
                }
            }
        }
        
      
    }
    
}



class ChatVC : UIViewController {

    private lazy var agoraKit: AgoraRtcEngineKit = {
        let engine = AgoraRtcEngineKit.sharedEngine(withAppId: AgoraAppId, delegate: nil)
        engine.setLogFilter(AgoraLogFilter.info.rawValue)
        engine.setLogFile(FileCenter.logFilePath())
        return engine
    }()
    
    

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var dataSource: CollectionViewDataSource<UserItemViewModel, UserCell>!
    
    private var users: [UserItemViewModel] = []
    private var cancellables: [AnyCancellable] = []
    private var viewModel = ChatViewModel()
    private var auctionViewModel = AuctionViewModel()
    private var settings = LiveSettings()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupCollectionView()
        observeUsers()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    func observeUsers() {
        self.viewModel.$state.sink { (state) in
            switch state {
            case .isLoading:
                break
            case .loaded(let items):
                self.users = items
                self.setupCollectionView()
            case .failed(let error):
                break
            }
        }.store(in: &cancellables)
    }
    
    @IBAction func streamTapped(_ sender: Any) {
        let goLiveVC = GoLiveVC.GoLiveVC()
        goLiveVC.delegate = self
        self.navigationController?.pushViewController(goLiveVC, animated: true)
    }
    
    


    fileprivate func setupCollectionView() {
        let dataSource = CollectionViewDataSource.makeDataSource(for: self.users) { (cell) in
        }
        self.dataSource = dataSource
        collectionView.dataSource = dataSource
        collectionView.delegate = self
        collectionView.reloadData()
    }

}

extension ChatVC: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (UIScreen.main.bounds.width / 2) - 70.0, height: 175)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if  self.users[indexPath.row].isLive {
            
            settings.roomName = "\(self.users[indexPath.row].name)-live"
            settings.frameRate = .fps30
            settings.role = .audience
            auctionViewModel.joinStreaming(organizerUserName: self.users[indexPath.row].name, meetingId: self.users[indexPath.row].currentMeetingId, userName: Session.Username)
            
            let vc = LiveRoomViewController.LiveRoomViewController()
           
            vc.liveInfo = StreamingInfo(organizerUserName: self.users[indexPath.row].name, meetingId: self.users[indexPath.row].currentMeetingId, userImageUrl: Session.avatar, organizerImageUrl: self.users[indexPath.row].avatarPath?.absoluteString ?? "")
            vc.dataSource = self
            vc.modalPresentationStyle = .fullScreen
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}


extension ChatVC: LiveVCDataSource {
    func liveVCNeedSettings() -> LiveSettings {
        return settings
    }
    
    func liveVCNeedAgoraKit() -> AgoraRtcEngineKit {
        return agoraKit
    }
}


extension CollectionViewDataSource where Model == UserItemViewModel, Cell == UserCell {
    static func makeDataSource(for models: [UserItemViewModel],
                                    reuseIdentifier: String = UserCell.identifier, callBack: callBack) -> CollectionViewDataSource {
        return CollectionViewDataSource(
            models: models,
            reuseIdentifier: reuseIdentifier
        ) { (model, cell) in
            if let model = model {
                cell.indicator.backgroundColor = model.isLive ? .green : .red
                cell.userName.text = model.name
                cell.imageView.sd_setImage(with: model.avatarPath, completed: nil)
                callBack?(cell)
            }
        }
    }
}



extension ChatVC: GoLiveDelegate {
    func VCDismissed(role: AgoraClientRole, meetindId: String, organizerId: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let vc = BiddingResultsVC.BiddingResultsVC()
            vc.meetindId = meetindId
            vc.meetindOrganizerId = organizerId
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

