//
//  UserItemViewModel.swift
//  3rbana
//
//  Created by Bilal on 26/04/2021.
//

import Foundation
class UserItemViewModel {
    private var user: User
    private var meetingId: String
    
    init(user: User, meetingId: String) {
        self.user = user
        self.meetingId = meetingId
    }
    
    var name: String {
        user.name
    }
    
    var isLive: Bool {
        return user.isActive
    }
    
    var avatarPath: URL? {
        return URL(string: user.avatar?.thumbnailAvatarPath ?? "")
    }
    
    var currentMeetingId: String {
        return self.meetingId
    }
    
    
}
