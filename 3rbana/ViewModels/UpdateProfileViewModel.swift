//
//  UpdateViewModel.swift
//  3rbana
//
//  Created by Mac on 08/04/2021.
//

import Foundation
import UIKit
import Alamofire

struct ImageRequest{
    let image      : UIImage?
    let fileName   : String?
}

// MARK: - Company Protocal
internal protocol UpdateProfileProtocal  {
    var  registerModel               : RegisterModel?          { get set }
    var  accessToken                 : String?                 { get set}
    var  apiService                  : APIServiceType          { get }
    var  state                       : State                   { get set }   //enum to change UI/UX
    var  updateLoadingStatus         : (()->())?               { get }       // clouserr to update status for UI
    var  showAlertClosure            : (()->())?               { get }       // clouserr to  alert for UI
    var  alertMessage                : String?                 { get set }
    var  sudentInfoArray             : StudentInfoViewModel?   { get set }
    
    mutating func updateProfile()
}

// MARK: - Class for Model View
final class UpdateProfileViewModel : NSObject, UpdateProfileProtocal {
   
    var validation          : Validation?{
        return Validation()
    }
    var accessToken: String? {
        didSet{
            if let accessToken  =  self.registerModel?.accessToken {
                self.accessToken = accessToken
            }
        }
    }
    var decodedData         : NSData?
    var sudentInfoArray     : StudentInfoViewModel?
    var imageRequest        : ImageRequest?
    var registerModel       : RegisterModel?
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var alertMessage : String? = "" {
        didSet {
            self.showAlertClosure?()
        }
    }
    var Url : String!
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    // MARK: - Fetch Data
    func saveUpdatedData(_ result: (UpdateBody)) {
        Session.start()
        print("\(result)")
        if let name  = result.name as String? {
            Session.Username       = name
        }
        if let pass  = self.registerModel?.password as String? {
            Session.password       = pass
        }
        if let Phone  = result.phone as String? {
            Session.Phone       = Phone
        }
        if let email  = result.email as String? {
            Session.UserMail       = email
        }
        if let address  = result.address as String? {
            Session.address       = address
        }
        if let CutID  = result.countryId as String? {
            Session.CutID       = CutID
        }
        if let accessToken  = self.registerModel?.accessToken as String? {
            Session.accessToken       = accessToken
        }
        if let refreshToken  = self.registerModel?.refreshToken as String? {
            Session.refreshToken       = refreshToken
        }
        if let avatar  = result.avatar?.thumbnailAvatarPath as String? {
            print(avatar)
            Session.avatar       = avatar
        }
        Session.save()
    }
    
    public func updateProfile() {
        
        self.state  = .loading
        
        // validation classe
        guard let name = registerModel?.name ,let phone = registerModel?.phone,
              let mail = registerModel?.email,let   country = registerModel?.countryId else {
            self.state         = .empty
            self.alertMessage  = Empty_Filed_Arabic
            
            return
        }
        
        if (name.isEmpty || phone.isEmpty || country.isEmpty || mail.isEmpty) {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? Empty_Filed_English : Empty_Filed_Arabic
            return
        }
        
        let isValidatEmail = self.validation!.validateEmailId(emailID: mail)
        
        if  !isValidatEmail {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? CHECKMAILEN : CHECKMAILAR
            return
        }
        let isValidatePhone = self.validation!.validaPhoneNumber(phoneNumber: phone)
        if (isValidatePhone != false) {
            self.state         = .empty
            self.alertMessage  =  (Session.Language == "en") ? CHECKELEPHONEEN : CHECKTELEPHONEAR
            return
        }else {
            
            let endPoint = EndPoint.updateProfileUrl
            self.Url     = endPoint.url
            
            print("\(self.Url)")
            
            let params  = [ "password"    : registerModel!.password    as Any,
                            "name"        : registerModel!.name        as Any,
                            "email"       : registerModel!.email       as Any,
                            "phone"       : registerModel!.phone       as Any,
                            "countryId"   : registerModel!.countryId   as Any,
                            "address"     : registerModel!.block       as Any,
                            "avenue"      : registerModel!.avenue      as Any,
                            "street"      : registerModel!.street      as Any,
                            "houseNumber" : registerModel!.houseNumber as Any]    as NSDictionary
            
            print("\(params)")
            print("\(registerModel!.accessToken)")
            
            self.state  = .loading

           
            self.apiService.apiPostMultipartFormData(url:  self.Url, withParameters: params, access_token: registerModel!.accessToken, imageRequest: self.imageRequest!){
                    [self] (response: ResultApi<Update, APIServiceError>) in
                    switch response {
                    case .failure(let error):
                        print("\(error)")
                        if error.localizedDescription == "unKnown" {
                            self.state        = .noconnect
                            self.alertMessage = (Session.Language == "en") ? INTERNET_ENG_MESSAGE : INTERNET_AR_MESSAGE
                        }else{
                            self.state        = .error
                            self.alertMessage = (Session.Language == "en") ?    ConnectionIssueEn : ConnectionIssueAr
                        }
                        break

                    case .success(let result):
                        if let status = result.status {
                            if status {
                                print("\(result)")
                                self.state = .populated
                                saveUpdatedData(result.body!)
                                self.alertMessage     = result.message
                            }else{
                                self.state        = .error
                                self.alertMessage     = result.message
                            }

                        }else{
                            self.state        = .error
                            self.alertMessage = result.message
                        }

                        break
                    }
            }
        }
    }
}
