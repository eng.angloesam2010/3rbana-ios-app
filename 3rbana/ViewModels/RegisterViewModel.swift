//
//  LoginViewModel.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 12/8/20.
//

import Foundation
import UIKit
import Alamofire

// MARK: - Model View for Cell
struct RegisterModel {
    var password        : String
    var name            : String
    var email           : String
    var phone           : String
    var countryId       : String
    var block           : String
    var avenue          : String
    var street          : String
    var houseNumber     : String
    var avatar          : String
    var confirmPass     : String
    var accessToken     : String
    var refreshToken    : String
}

// MARK: - Company Protocal
internal protocol RegisterProtocal  {
    var  registerModel               : RegisterModel?             { get set }
    var  apiService                  : APIServiceType          { get }
    var  state                       : State                   { get set }   //enum to change UI/UX
    var  updateLoadingStatus         : (()->())?               { get }       // clouserr to update status for UI
    var  showAlertClosure            : (()->())?               { get }       // clouserr to  alert for UI
    var  alertMessage                : String?                  { get set }
    var  sudentInfoArray             : StudentInfoViewModel?   { get set }
    mutating func registerNewUser()
}

// MARK: - Class for Model View
final class RegisterViewModel : NSObject, RegisterProtocal {
    var validation          : Validation?{
        return Validation()
    }
    var sudentInfoArray     : StudentInfoViewModel?
    var registerModel       : RegisterModel?
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var alertMessage : String? = "" {
        didSet {
            self.showAlertClosure?()
        }
    }
    var Url: String = ""
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    // MARK: - Fetch Data
    public func registerNewUser() {
        
        self.state  = .loading
        
        // validation classe
        guard let name = registerModel?.name ,let password = registerModel?.password ,let confirmPass = registerModel?.confirmPass,let phone = registerModel?.phone,
              let mail = registerModel?.email,let   country = registerModel?.countryId else {
            self.state         = .empty
            self.alertMessage  =  (Session.Language == "en") ? Empty_Filed_English  : Empty_Filed_Arabic
            
            return
        }
        
        if (name.isEmpty  ||  password.isEmpty  || confirmPass.isEmpty    || phone.isEmpty || country.isEmpty || mail.isEmpty) {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? Empty_Filed_English : Empty_Filed_Arabic
            return
        }
        
        let passwordLengthValidation = self.validation!.checkPasswordLength(passStr: password)
        
        
        if  passwordLengthValidation {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? PASSWORDLENTHEN : PASSWORDLENTHAR
            return
        }
        
        let isValidateConfirmPassword = (confirmPass != confirmPass)
        
        if  isValidateConfirmPassword {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? Confirm_Password_Message_En : Confirm_Password_Message_Ar
            return
        }
        
        
        
        
        let isValidatEmail = self.validation!.validateEmailId(emailID: mail)
        
        if  !isValidatEmail {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? CHECKMAILEN : CHECKMAILAR
            return
        }
        let isValidatePhone = self.validation!.validaPhoneNumber(phoneNumber: phone)
        if (isValidatePhone != false) {
            self.state         = .empty
            self.alertMessage  =  (Session.Language == "en") ? CHECKELEPHONEEN : CHECKTELEPHONEAR
            return
        }
        if (isValidateConfirmPassword == true ) {
            self.alertMessage  = (Session.Language == "en") ? Confirm_Password_Message_En : Confirm_Password_Message_Ar
        }else{
            
            let params  = [ "password"  : registerModel!.password as? Any,
                            "name"      : registerModel!.name  as? Any,
                            "email"     : registerModel!.email as? Any,
                            "phone"     : registerModel!.phone  as? Any,
                            "countryId" : registerModel!.countryId  as? Any,
                            "block" : registerModel!.block  as? Any,
                            "avenue" : registerModel!.avenue  as? Any,
                            "street" : registerModel!.street  as? Any,
                            "houseNumber" :registerModel!.houseNumber as? Any,
                            "avatar" : ""  ] as NSDictionary
            print("\(params)")
            let headers: HTTPHeaders = [:]
            self.state  = .loading
            let endPoint = EndPoint.regUrl
            self.Url     = endPoint.url
            print("\(Url)")
            self.apiService.fetchGenericData(url: Url , withParameters: params, httpMethod: .post, withHeader: headers ) {
                (response: ResultApi<Message, APIServiceError>) in
                switch response {
                case .failure(let error):
                    //                    print("\(error)")
                    if error.localizedDescription == "unKnown" {
                        self.state        = .noconnect
                        self.alertMessage = (Session.Language == "en") ? INTERNET_ENG_MESSAGE : INTERNET_AR_MESSAGE
                    }else{
                        self.state        = .error
                        self.alertMessage = (Session.Language == "en") ?    ConnectionIssueEn : ConnectionIssueAr
                    }
                    break
                    
                case .success(let result):
                    if let status = result.status {
                        if status {
                            self.state = .populated
                            
                        }else{
                            self.state        = .error
                            if let msg = result.message {
                                self.alertMessage     = msg
                            }
                        }
                        
                    }else{
                        self.state        = .error
                        if let msg = result.message {
                            self.alertMessage     = msg
                        }
                    }
                    
                    break
                }
            }
            
        }
    }
}
