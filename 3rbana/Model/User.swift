//
//  User.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 30, 2021
//
import Foundation

struct User: Codable {

    let createdDate: String
    let name: String
    let email: String
    let username: String
    let phone: String
    let block: String
    let avenue: String
    let street: String
    let houseNumber: String
    let countryId: String
    let lastLogged: String
    let isActive: Bool
    let userId: Int
    let key: Int
    let avatar : Avatar?

    private enum CodingKeys: String, CodingKey {
        case createdDate = "createdDate"
        case name = "name"
        case email = "email"
        case username = "username"
        case phone = "phone"
        case block = "block"
        case avenue = "avenue"
        case street = "street"
        case houseNumber = "houseNumber"
        case countryId = "countryId"
        case lastLogged = "lastLogged"
        case isActive = "isActive"
        case userId = "userId"
        case key = "key"
        case avatar = "avatar"
    }
    
    
    init(name: String, isActive: Bool, imagePath: String) {
        self.name = name
        self.isActive = isActive
        let avatar = Avatar(thumbnailPath: imagePath)
        self.avatar = avatar
        
        self.email = ""
        self.phone = ""
        self.username = ""
        self.avenue = ""
        self.block = ""
        self.street = ""
        self.houseNumber = ""
        self.createdDate = ""
        self.lastLogged = ""
        self.userId = 0
        self.key = 0
        self.countryId = "0"
        
        
        
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        createdDate = try values.decode(String.self, forKey: .createdDate)
        name = try values.decode(String.self, forKey: .name)
        email = try values.decode(String.self, forKey: .email)
        username = try values.decode(String.self, forKey: .username)
        if let phoneStr = try? values.decode(String.self, forKey: .phone){
            self.phone = phoneStr
        }else{
            self.phone = ""
        }
        
        if let blockStr = try? values.decode(String.self, forKey: .block) {
            self.block = blockStr
        }else{
            self.block = ""
        }
        
        if let  avenueStr = try? values.decode(String.self, forKey: .avenue){
            self.avenue  = avenueStr
        }else{
            self.avenue = ""
        }
        
        if let street = try? values.decode(String.self, forKey: .street) {
            self.street =  street
        }else{
            self.street = ""
        }
        if let houseNumber = try? values.decode(String.self, forKey: .houseNumber){
            self.houseNumber = houseNumber
        }else{
            self.houseNumber = ""
        }
        countryId = try values.decode(String.self, forKey: .countryId)
        lastLogged = try values.decode(String.self, forKey: .lastLogged)
        isActive = try values.decode(Bool.self, forKey: .isActive)
        userId = try values.decode(Int.self, forKey: .userId)
        key = try values.decode(Int.self, forKey: .key)
        
        if let avatar = try? values.decode(Avatar.self, forKey: .avatar){
            self.avatar = avatar
        }else{
            self.avatar = nil
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(createdDate, forKey: .createdDate)
        try container.encode(name, forKey: .name)
        try container.encode(email, forKey: .email)
        try container.encode(username, forKey: .username)
        try container.encode(phone, forKey: .phone)
        try container.encode(block, forKey: .block)
        try container.encode(avenue, forKey: .avenue)
        try container.encode(street, forKey: .street)
        try container.encode(houseNumber, forKey: .houseNumber)
        try container.encode(countryId, forKey: .countryId)
        try container.encode(lastLogged, forKey: .lastLogged)
        try container.encode(isActive, forKey: .isActive)
        try container.encode(userId, forKey: .userId)
        try container.encode(key, forKey: .key)
    }

}

