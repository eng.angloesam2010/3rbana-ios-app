//
//  UIImage.swift
//  3rbana
//
//  Created by Mac on 08/04/2021.
//

import Foundation
import UIKit

extension UIImage {
    func resized(withPercentage percentage: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: size.width * percentage, height: size.height * percentage)
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    func resized(toWidth width: CGFloat, isOpaque: Bool = true) -> UIImage? {
        let canvas = CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))
        let format = imageRendererFormat
        format.opaque = isOpaque
        return UIGraphicsImageRenderer(size: canvas, format: format).image {
            _ in draw(in: CGRect(origin: .zero, size: canvas))
        }
    }
    func toData (options: NSDictionary, type: CFString) -> Data? {
         guard let cgImage = cgImage else { return nil }
         return autoreleasepool { () -> Data? in
             let data = NSMutableData()
             guard let imageDestination = CGImageDestinationCreateWithData(data as CFMutableData, type, 1, nil) else { return nil }
             CGImageDestinationAddImage(imageDestination, cgImage, options)
             CGImageDestinationFinalize(imageDestination)
             return data as Data
         }
     }
    
    
    
    
  
}
