//
//  CategoryTableViewCell+UICollectionView.swift
//  3rbana
//
//  Created by Mac on 09/02/2021.
//

import Foundation
import UIKit
extension CategoryTableViewCell :   UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return  (self.categoryViewModel.catCellModel.count > 0 ) ? self.categoryViewModel.catCellModel.count : 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryCollectionViewCell.identifier , for: indexPath) as! CategoryCollectionViewCell
        cell.configureCell(with: (self.categoryViewModel.catCellModel[indexPath.row]))
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: cellWidth - spacing / 3, height: 128.0)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print("flag : \(flag)")
        if flag == 0 {
            delegateCategory?.didSelectIndex(id: 9, categoryID:indexPath.row,flag:1)
        }else if flag == 1{
            delegateCategory?.didSelectIndex(id: 10, categoryID:indexPath.row,flag:2)
        }else if flag == 2 {
            delegateCategory?.didSelectIndex(id: 12, categoryID:indexPath.row,flag:3)
        }
        
    }
    
    
}
