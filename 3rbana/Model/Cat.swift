

import Foundation
struct Cat : Codable {
	let status : Bool?
	let message : String?
	let catItem : [CatItem]?

	enum CodingKeys: String, CodingKey {

		case status  = "status"
		case message = "message"
		case catItem     = "body"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status  = try values.decodeIfPresent(Bool.self, forKey: .status)
		message  = try values.decodeIfPresent(String.self, forKey: .message)
        catItem  = try values.decodeIfPresent([CatItem].self, forKey: .catItem)
	}

}
