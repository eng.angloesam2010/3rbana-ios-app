//
//  LevelCell.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/5/20.
//

import Foundation

struct StagesRoot : Codable {
    
    let stages : [Stages]?
    let stagesCount : Int?
    
    enum CodingKeys: String, CodingKey {
        case stages      = "Stages"
        case stagesCount = "StagesCount"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        stages = try values.decodeIfPresent([Stages].self, forKey: .stages)
        stagesCount = try values.decodeIfPresent(Int.self, forKey: .stagesCount)
    }
    
}


