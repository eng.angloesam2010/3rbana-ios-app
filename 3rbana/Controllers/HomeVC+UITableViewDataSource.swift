//
//  SubjectsVC+UITableViewDataSource.swift
//  al_Jalboot_app
//
//  Created by Mac on 12/19/20.
//

import UIKit
import Alamofire

// MARK: - extension for TableView to manage datasource and delegate
extension HomeVC : UITableViewDataSource,UITableViewDelegate,ReloadCellDelegate,StoryProtocal {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: SliderCell.identifier , for: indexPath) as! SliderCell
            cell.fetchSlider()
            cell.delegate = self
            return cell
        }else if indexPath.row == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: StoryTbleViewCell.identifier , for: indexPath) as! StoryTbleViewCell
            return cell
            
        }else if indexPath.row == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: CategoryTableViewCell.identifier , for: indexPath) as! CategoryTableViewCell
            cell.startFetchCategory()
            cell.delegate = self
            cell.flag = 0
            cell.delegateCategory = self
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 280.0
        }else if indexPath.row == 1 {
            return 150.0
        }
        return UITableView.automaticDimension
    }
    
    func reloadCell(){
        self.mainTableView.beginUpdates()
        self.mainTableView.endUpdates()
    }
    
    func navigateStoryScreen(){
        Session.start()
        if  Session.Username.isEmpty {
            self.tabBarController?.selectedIndex = 4
        }else{
            self.pushViewController(id: 10)
        }
    }
}
















