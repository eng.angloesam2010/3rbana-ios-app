//
//  ApiRoute.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/10/20.
//

import Foundation

extension String{
    static func makeForEndpoint(_ endpoint:String) -> String{
        return "https://www.3rbana.com/api/"+endpoint
    }
    ///api/media/render
    static func makeForEndpointMedia(_ endpoint:String) -> String{
        return "https://www.3rbana.com/api/"+endpoint
    }
    static func makeForEndpointImage(_ endpoint:String) -> String{
        return "http://app.moe.edu.kw/PrimaryVideo/"+endpoint
    }
    static func checkLoginEndPoint(_ endpoint:String) -> String{
        return "http://app.moe.edu.kw/srvvideos/api/"+endpoint
    }
}
enum EndPoint {
    case loadStages(pageIndex : String,pageSize : String = "20",lang  : String = "ar")
    case pathImage(pathImage : String)
    case listsubjects(stageId : String,pageindex : String,pageSize : String = "20",lang  : String = "ar")
    case subjects(stageId : String)
    case regUrl
    case updateProfileUrl
    case loginUserUrl
    case catUrl
    case videoListUrl(subjectID : String)
    case countrylist
    case governorateUrl(countryId : String)
    case changePassword
    case logoutUrl(token : String)
    case sliderUrl
    case subCatUrl(ID : String)
    case subSuCatUrl(ID : String)
    case fetchPostUrlGoverID(subSubCategoryId : String , governoratesId : String = "")
    case fetchPostUrl(subSubCategoryId : String)
    case fetchAdvertiseUrl(governoratesId : String,limit: String,offset: String,count : String)
    //https://3rbana.com/api/advertisement/list
}

extension EndPoint {
    var url : String {
        switch self {
        case .loginUserUrl:
            return .makeForEndpoint("auth/login")
        case .loadStages(let pageIndex, let pageSize,let lang):
            return .makeForEndpoint("Stages/LoadStages?pageIndex=\(pageIndex)&&pageSize=\(pageSize)&&lang=\(lang)")
        case .pathImage(let pathImage):
            return .makeForEndpointMedia("media/render/?path=\(pathImage)")
        case .listsubjects(let stageId, let pageindex,let pageSize,let lang):
            return .makeForEndpoint("Subjects/LoadStageSubjects?stageId=\(stageId)&pageindex=\(pageindex)&pageSize=\(pageSize)&lang=\(lang)")
        case .subjects(let stageId):
            return .makeForEndpoint("Subjects/\(stageId)")
        case .catUrl:
            return .makeForEndpoint("category/list")
        case .subCatUrl(let ID):
            return .makeForEndpoint("category/list?\(ID)")
        case .subSuCatUrl(let ID):
            return .makeForEndpoint("sub-sub-category/list?\(ID)")
        //http://www.3rbana.com/api/sub-sub-category/list
        case .videoListUrl(let subjectID):
            return .checkLoginEndPoint("/Subjects/GetSubject?SID=\(subjectID)")
        case .countrylist:
            return .makeForEndpoint("country/list")
        case .regUrl:
            return .makeForEndpoint("user/signup")
        case .updateProfileUrl:
            return .makeForEndpoint("user/edit")
        case .logoutUrl(let token):
            return .makeForEndpoint("auth/logout/?token=\(token)")
        case .changePassword:
            return .makeForEndpoint("user/change-password")
        case .sliderUrl:
            return .makeForEndpoint("slider/list")
        case .governorateUrl(let countryId):
            return .makeForEndpoint("governorates/list?countryId=\(countryId)")
        case .fetchPostUrlGoverID(let subSubCategoryId,let governoratesId):
            return .makeForEndpoint("post/list?subSubCategoryId=\(subSubCategoryId)&governoratesId=\(governoratesId)")
        case .fetchPostUrl(let subSubCategoryId):
            return .makeForEndpoint("post/list?subSubCategoryId=\(subSubCategoryId)")
        case .fetchAdvertiseUrl(let governoratesId,let limit,let offset,let count):
            return .makeForEndpoint("advertisement/list?limit=0&offset=51")
        }
        
    }
}

