//
//  AddAdsVC+, UIImagePickerControllerDelegate.swift
//  3rbana
//
//  Created by Mac on 08/05/2021.
//

import Foundation
import UIKit
import BSImagePicker
extension AddAdsVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func openGalleryAndCamera(){
        let imagePicker = ImagePickerController()
        
        imagePicker.settings.selection.max = 5
        imagePicker.settings.selection.unselectOnReachingMax = true
        imagePicker.settings.fetch.assets.supportedMediaTypes = [.image]
        imagePicker.albumButton.tintColor = UIColor.green
        imagePicker.cancelButton.tintColor = UIColor.red
        imagePicker.doneButton.tintColor = UIColor.purple
        imagePicker.navigationBar.barTintColor = .black
        imagePicker.settings.theme.backgroundColor = .black
        imagePicker.settings.theme.selectionFillColor = UIColor.gray
        imagePicker.settings.theme.selectionStrokeColor = UIColor.yellow
        imagePicker.settings.theme.selectionShadowColor = UIColor.red
        imagePicker.settings.theme.previewTitleAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16),NSAttributedString.Key.foregroundColor: UIColor.white]
        imagePicker.settings.theme.previewSubtitleAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 12),NSAttributedString.Key.foregroundColor: UIColor.white]
        imagePicker.settings.theme.albumTitleAttributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 18),NSAttributedString.Key.foregroundColor: UIColor.white]
        imagePicker.settings.list.cellsPerRow = {(verticalSize: UIUserInterfaceSizeClass, horizontalSize: UIUserInterfaceSizeClass) -> Int in
            switch (verticalSize, horizontalSize) {
            case (.compact, .regular): // iPhone5-6 portrait
                return 2
            case (.compact, .compact): // iPhone5-6 landscape
                return 2
            case (.regular, .regular): // iPad portrait/landscape
                return 3
            default:
                return 2
            }
        }
        
        
        presentImagePicker(imagePicker, select: { (asset) in
            // User selected an asset. Do something with it. Perhaps begin processing/upload?
        }, deselect: { (asset) in
            // User deselected an asset. Cancel whatever you did when asset was selected.
        }, cancel: { (assets) in
            // User canceled selection.
        }, finish: { (assets) in
            print("Finished with selections: \(assets)")
            for asset in assets{
                self.evenAssets.append(asset)
                self.addPhotoTotCell()
            }
        })
    }
    
    
    
    
}


