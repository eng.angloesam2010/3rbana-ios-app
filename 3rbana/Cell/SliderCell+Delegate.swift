//
//  SliderCell+Delegate.swift
//  3rbana
//
//  Created by Mac on 31/01/2021.
//

import Foundation
import UIKit
import FSPagerView
import Kingfisher

extension SliderCell{
    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        print("\(self.sliderViewModel.sliderArr.count)")
        self.pageControl.numberOfPages = self.sliderViewModel.sliderArr.count
        return self.sliderViewModel.sliderArr.count
    }
    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell        = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        if let imagePath = fullPath(index: index) as String? {
            let endPointUrl = EndPoint.pathImage(pathImage:imagePath)
            cell.imageView?.kf.setImage(with: URL(string:endPointUrl.url),placeholder:UIImage(named: "new_logo"))
        }

        cell.imageView?.contentMode   = .scaleAspectFill
        cell.imageView?.clipsToBounds = true
        self.pageControl.currentPage = index
        return cell
    }
    
    func fullPath(index:Int) -> String {
//        print("\(self.sliderViewModel.sliderArr[index].imgPath!)")
        guard let path = self.sliderViewModel.sliderArr[index].imgPath  else {
            return ""
        }
        return path
    }
    
    

    
    
}
