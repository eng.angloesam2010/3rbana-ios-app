//
//  BiddingResultsCell.swift
//  3rbana
//
//  Created by Bilal on 29/04/2021.
//

import UIKit

class BiddingResultsCell: UITableViewCell {
    
    static let identifier = "BiddingResultsCell"
    
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var phoneLbl: UILabel!
    @IBOutlet weak var userPhoneLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var productPriceLbl: UILabel!
    
    
    
}
