//
//  Util.swift
//  عيسى الهولي
//
//  Created by Eng Angelo E Saber on 1/7/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation
import SwiftMessages
import UIKit
import Alamofire
//import AZDialogView
//import JGProgressHUD



public class Util {
    class func  screenWidth() -> CGFloat {
        return UIScreen.main.bounds.width
    }
    class func  screenHeight() -> CGFloat {
        return UIScreen.main.bounds.height
    }
    
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
    
    class func showErrorAlert(caller:UIViewController,message:String) -> Void {
            Util.showAlert(caller: caller, title: "Error", message: message)
        
    }
    
    class func showAlert(caller:UIViewController,title:String?,message:String) -> Void {
        DispatchQueue.main.async {
            let alert = UIAlertController(title:title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            caller.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showAlertWithButton(caller:UIViewController,title: String, message: String,buttonTitle:String = "Cancel" ,buttonStyle: UIAlertAction.Style = UIAlertAction.Style.cancel , buttonHandler: ((UIAlertAction) -> Void)? = nil ) -> Void {

        DispatchQueue.main.async {
            let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: buttonTitle, style: buttonStyle, handler:buttonHandler))
            caller.present(alert, animated: true, completion: {
            })
        }
    }
    
    class func showAlertWithTwoButtons(caller:UIViewController,title: String, message: String,firstButtonTitle:String = "Cancel" ,firstButtonStyle: UIAlertAction.Style = UIAlertAction.Style.cancel , firstButtonHandler: ((UIAlertAction) -> Void)? = nil , secondButtonTitle: String,secondButtonStyle: UIAlertAction.Style = UIAlertAction.Style.cancel , secondButtonHandler: ((UIAlertAction) -> Void)? = nil ) -> Void {
        DispatchQueue.main.async {
            let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: firstButtonTitle, style: firstButtonStyle, handler:firstButtonHandler))
            alert.addAction(UIAlertAction(title: secondButtonTitle, style: secondButtonStyle, handler:secondButtonHandler))
            
            caller.present(alert, animated: true, completion: {
            })
        }
    }
    
    
    
    class func showAlertWithThreeButtons(caller:UIViewController,title: String, message: String,firstButtonTitle:String ,firstButtonStyle: UIAlertAction.Style = UIAlertAction.Style.default , firstButtonHandler: ((UIAlertAction) -> Void)? = nil , secondButtonTitle: String,secondButtonStyle: UIAlertAction.Style = UIAlertAction.Style.default , secondButtonHandler: ((UIAlertAction) -> Void)? = nil, thirdButtonTitle: String,thirdButtonStyle: UIAlertAction.Style = UIAlertAction.Style.default , thirdButtonHandler: ((UIAlertAction) -> Void)? = nil ) -> Void {
        
        DispatchQueue.main.async {
            let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: firstButtonTitle, style: firstButtonStyle, handler:firstButtonHandler))
            alert.addAction(UIAlertAction(title: secondButtonTitle, style: secondButtonStyle, handler:secondButtonHandler))
            alert.addAction(UIAlertAction(title: thirdButtonTitle, style: thirdButtonStyle, handler:thirdButtonHandler))
            
            caller.present(alert, animated: true, completion: {
            })
        }
    }
    
    
    class func showTextFieldAlertWithTwoButtons(caller:UIViewController,title: String, message: String,configurationTextField:((UITextField) -> Swift.Void)? = nil,firstButtonTitle:String = "Cancel" ,firstButtonStyle: UIAlertAction.Style = UIAlertAction.Style.cancel , firstButtonHandler: ((UIAlertAction) -> Void)? = nil , secondButtonTitle: String,secondButtonStyle: UIAlertAction.Style = UIAlertAction.Style.cancel , secondButtonHandler: ((UIAlertAction) -> Void)? = nil ) -> Void {
        DispatchQueue.main.async {
                let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
                alert.addTextField(configurationHandler: configurationTextField)
                
                alert.addAction(UIAlertAction(title: firstButtonTitle, style: firstButtonStyle, handler:firstButtonHandler))
                alert.addAction(UIAlertAction(title: secondButtonTitle, style: secondButtonStyle, handler:secondButtonHandler))
                
                caller.present(alert, animated: true, completion: {
                })
        }
    }
    
    
    class func showTwoTextFieldWithTwoButtons(caller:UIViewController,title: String, message: String,firstTextFieldConfiguration:((UITextField) -> Swift.Void)? = nil, secondTextFieldConfiguration:((UITextField) -> Swift.Void)? = nil  ,firstButtonTitle:String = "Cancel" ,firstButtonStyle: UIAlertAction.Style = UIAlertAction.Style.cancel , firstButtonHandler: ((UIAlertAction) -> Void)? = nil , secondButtonTitle: String,secondButtonStyle: UIAlertAction.Style = UIAlertAction.Style.cancel , secondButtonHandler: ((UIAlertAction) -> Void)? = nil ) -> Void {
        DispatchQueue.main.async {
                let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
                alert.addTextField(configurationHandler: firstTextFieldConfiguration)
                alert.addTextField(configurationHandler: secondTextFieldConfiguration)
                alert.addAction(UIAlertAction(title: firstButtonTitle, style: firstButtonStyle, handler:firstButtonHandler))
                alert.addAction(UIAlertAction(title: secondButtonTitle, style: secondButtonStyle, handler:secondButtonHandler))
                caller.present(alert, animated: true, completion: {
                })
        }
    }
    
    
    
    
    
    
    class func showTextFieldAlertButton(caller:UIViewController,title: String, message: String,configurationTextField:((UITextField) -> Swift.Void)? = nil,buttonTitle:String = "Cancel" ,buttonStyle: UIAlertAction.Style = UIAlertAction.Style.cancel , buttonHandler: ((UIAlertAction) -> Void)? = nil ) -> Void {
        
        DispatchQueue.main.async {
            
            let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
            alert.addTextField(configurationHandler: configurationTextField)
            
            alert.addAction(UIAlertAction(title: buttonTitle, style: buttonStyle, handler:buttonHandler))
            
            caller.present(alert, animated: true, completion: {
            })
        }
    }

}

