//
//  LaunchVC.swift
//  3rbana
//
//  Created by Mac on 05/04/2021.
//

import UIKit

class LogoVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        ProgressHUDViewModel.sharedInstance.showGProgressHUD(timeFor: 10.0)
        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
            Session.start()
            DirectionViewModel.sharedManager.setSemanticContentAttribute()
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let tbc   = storyBoard.instantiateViewController(withIdentifier:"tabbar") as! UITabBarController
            tbc.selectedIndex = 0
            tbc.modalPresentationStyle = .fullScreen
            self.present(tbc, animated: true, completion:nil)
          }
    }
}
