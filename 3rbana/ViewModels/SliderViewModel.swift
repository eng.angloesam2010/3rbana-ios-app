//
//  CountryViewModel.swift
//  3rbana
//
//  Created by Mac on 18/01/2021.
//

import Foundation
import UIKit
import Alamofire

public struct SliderModel {
    var imgPath    : String?
    var imgLink    : String?
    
}

// MARK: - Class for Model View
final class SliderViewModel : NSObject {
    let cellWidth           =  Util.screenWidth() / 2.0
    var sliderArr          = [SliderModel]()
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : StatePopulate = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var alertMessage : String? = ""  {
        didSet {
            self.showAlertClosure?()
        }
    }
    var Url: String = ""
    
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    // MARK: - Fetch Data
    public func fetcSlider() {
        
        let params  = [ : ] as NSDictionary
        let headers: HTTPHeaders = [:]
        self.state  = .loading
        
        // MARK: - Request for api
        Session.start()
        if (Session.Username.isEmpty) {
            let endPoint = EndPoint.sliderUrl
            self.Url     = endPoint.url
            print("\(self.Url)")
            self.apiService.fetchGenericData(url: Url , withParameters: params, httpMethod: .get, withHeader: headers ) {
                (response: ResultApi<Slider, APIServiceError>) in
                switch response {
                case .failure(_):
                    break
                case .success(let result):
                    if let status = result.status {
                        if status {
                            print("\(result)")
                            self.state = .populated
                            self.appendSlider(body:result.body)
                        }
                    }
                    
                    break
                }
            }
            
        }
        
        
    }
    private func appendSlider(body : [SliderBody]?) {
        body!.forEach {
            let sliderItem = $0
            if let imgPath = sliderItem.avatar?.thumbnailAvatarPath!, let imgLink  = sliderItem.websiteLink {
                sliderArr.append(SliderModel(imgPath: imgPath , imgLink: imgLink))
            }
        }
    }
    
    
}
