//
//  CountryViewModel.swift
//  3rbana
//
//  Created by Mac on 18/01/2021.
//

import Foundation
import UIKit
import Alamofire

public struct CountryListModel {
    var countryID            : Int?
    var countryArName        : String?
    var countryEnName        : String?
    var countryLogoPath      : String?
}

// MARK: - Delegate for click cell
protocol CountryTableViewDelegate {
    func didSelectCell(countryID: Int)
}

// MARK: - Class for Model View
final class CountryViewModel : NSObject {
    let cellWidth           =  Util.screenWidth() / 2.0
    var countryArr          = [CountryListModel]()
    var loginArray          : LoginModel?
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var delegate : CountryTableViewDelegate?
    var alertMessage : String? = ""  {
        didSet {
            self.showAlertClosure?()
        }
    }
    var Url: String = ""
  
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    // MARK: - Fetch Data
    public func fetchCountry() {
       
        let params  = [ : ] as NSDictionary
        let headers: HTTPHeaders = [:]
        self.state  = .loading
        
        // MARK: - Request for api
        Session.start()
//        if (Session.Username.isEmpty) {
            let endPoint = EndPoint.countrylist
            self.Url     = endPoint.url
            print("\(self.Url)")
            self.apiService.fetchGenericData(url: Url , withParameters: params, httpMethod: .get, withHeader: headers ) {
                (response: ResultApi<Country, APIServiceError>) in
                switch response {
                case .failure(let error):
                    if error.localizedDescription == "unKnown" {
                        self.state        = .noconnect
                        self.alertMessage = (Session.Language == "en") ? INTERNET_ENG_MESSAGE : INTERNET_AR_MESSAGE
                    }else{
                        self.state        = .error
                        self.alertMessage = error.localizedDescription
                    }
                  
                    break
                case .success(let result):
                    if let status = result.status {
                        if status {
                        self.state = .populated
                            self.appendCountry(body: result.body)
                        
                       }else{
                            self.state        = .error
                            self.alertMessage = Login_Ar_Msg
                      }
                    }else{
                        self.state        = .error
                        self.alertMessage = Login_Ar_Msg
                    }
                    break
                }
            }
    }
    private func appendCountry(body : [Body]?) {
        body!.forEach {
            let country = $0
            countryArr.append(CountryListModel(countryID: country.countryId, countryArName: country.arabicName, countryEnName: country.name,countryLogoPath: country.avatar?.thumbnailAvatarPath))
        }
      }
    func getSelectedCountryID(countryNameStr :  String)->Int {
        var countryID = 0
        countryArr.forEach {
            if $0.countryEnName == countryNameStr {
                countryID = $0.countryID!
            }
        }
        return countryID
    }
    func getCountryName(countryID : Int)->String {
        var countryName = ""
        countryArr.forEach {
            if $0.countryID == countryID {
                countryName = (Session.Language == "en" ) ? $0.countryEnName! : $0.countryArName!
            }
        }
        return countryName
    }
}
extension CountryViewModel :UICollectionViewDelegate,UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.countryArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:CountryCell.identifier, for: indexPath) as! CountryCell
        cell.configureCell(model:self.countryArr[indexPath.row])
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: cellWidth , height: 90.0 )
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let countryModel = self.countryArr[indexPath.row]
        if let countryID : Int = self.countryArr[indexPath.row].countryID {
            self.setSelectedCountryID(countryID)
        }
        
        self.delegate?.didSelectCell(countryID: countryModel.countryID!)
    }
    func setSelectedCountryID(_ CountryID : Int) -> Void {
        Session.start()
        Session.SelectedCountry = CountryID
        Session.setSelectedCountry()
    }
    
}

