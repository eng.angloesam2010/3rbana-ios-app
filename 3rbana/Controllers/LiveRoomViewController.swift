//
//  LiveRoomViewController.swift
//  OpenLive
//
//  Created by GongYuhua on 6/25/16.
//  Copyright © 2016 Agora. All rights reserved.
//

import UIKit
import AgoraRtcKit
import Combine



extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        var rgbValue: UInt64 = 0
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}


class AppTextField: UITextField {
    var padding: UIEdgeInsets = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    
    @IBInspectable
    var conRadius: CGFloat = 26.5 {
        didSet {
            self.setCornerRadius()
        }
    }
    
    @IBInspectable
    override var borderWidth: CGFloat  {
        didSet {
            self.layer.cornerRadius = borderWidth
        }
    }
    
    
    @IBInspectable
    override var borderColor: UIColor? {
        didSet {
            self.layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable
    var leftViewEnabled: Bool = false {
        didSet {
            if leftViewEnabled {
                rightViewEnabled = false
                padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 10)
            }
        }
    }
    
    
    @IBInspectable
    var rightViewEnabled: Bool = false {
        didSet {
            if rightViewEnabled {
                leftViewEnabled = false
                padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 60)
            }
        }
    }
    
    @IBInspectable
    override var placeHolderColor: UIColor? {
        didSet {
           // self.attributedPlaceholder = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedString.Key.foregroundColor: placeHolderColor ?? .white, NSAttributedString.Key.font:  UIFont.systemFont(ofSize: 13)])
        }
    }
    

    override init(frame: CGRect){
        super.init(frame: frame)
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    func setCornerRadius() {
        self.layer.cornerRadius = cornerRadius
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        setup()
    }
    
    
    
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
           return bounds.inset(by: padding)
       }

       override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
           return bounds.inset(by: padding)
       }

       override open func editingRect(forBounds bounds: CGRect) -> CGRect {
           return bounds.inset(by: padding)
       }
    
    func setup() {
        self.borderStyle = .none
        self.backgroundColor = .clear
        self.layer.cornerRadius = 26.5
    }
    
}

struct StreamingInfo {
    var organizerUserName: String
    var meetingId: String
    var userImageUrl: String
    var organizerImageUrl: String
}


protocol LiveVCDataSource: NSObjectProtocol {
    func liveVCNeedAgoraKit() -> AgoraRtcEngineKit
    func liveVCNeedSettings() -> LiveSettings
}


protocol LiveRoomVCDelegate: class {
    func VCDismissed(role: AgoraClientRole, meetindId: String, organizerId: String)
}

class LiveRoomViewController: UIViewController {
    
    
    
    @IBOutlet weak var broadcastersView: AGEVideoContainer!
    @IBOutlet weak var placeholderView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var videoMuteButton: UIButton!
    @IBOutlet weak var audioMuteButton: UIButton!
    @IBOutlet weak var beautyEffectButton: UIButton!
    @IBOutlet weak var chatTableContainer: UIView!
    @IBOutlet weak var chatTextField: AppTextField!
    @IBOutlet weak var chatTabelView: UITableView!
    private var cancellables: [AnyCancellable] = []
    @IBOutlet weak var chatContainer: UIView!
    @IBOutlet weak var usersOnline: UILabel!
    
    @IBOutlet weak var currentProductPrice: UILabel!
    @IBOutlet weak var buttonsContainer: UIView!
    @IBOutlet weak var chatTextContainer: UIView!
    
    @IBOutlet weak var userNameLbl: UILabel!
    
    @IBOutlet weak var storeDescLbl: UILabel!
    
    var isChatMode: Bool = false {
        didSet {
            if isChatMode {
                self.buttonsContainer.isHidden = true
                self.chatTextContainer.isHidden = false
            }else {
                self.chatTextContainer.isHidden = true
                self.buttonsContainer.isHidden = false
            }
        }
    }
    
    
    private var viewModel = AuctionViewModel()
    
     var liveInfo: StreamingInfo!
    
    
    private var messages: [AuctionMessage] = []
    
    weak var delegate: LiveRoomVCDelegate?
    
    @IBOutlet var sessionButtons: [UIButton]!
    
    private let beautyOptions: AgoraBeautyOptions = {
        let options = AgoraBeautyOptions()
        options.lighteningContrastLevel = .normal
        options.lighteningLevel = 0.7
        options.smoothnessLevel = 0.5
        options.rednessLevel = 0.1
        return options
    }()
    
    private var agoraKit: AgoraRtcEngineKit {
        return dataSource!.liveVCNeedAgoraKit()
    }
    
    private var settings: LiveSettings {
        return dataSource!.liveVCNeedSettings()
    }
    
    private var isMutedVideo = false {
        didSet {
            // mute local video
            agoraKit.muteLocalVideoStream(isMutedVideo)
            videoMuteButton.isSelected = isMutedVideo
        }
    }
    
    private var isMutedAudio = false {
        didSet {
            // mute local audio
            agoraKit.muteLocalAudioStream(isMutedAudio)
            audioMuteButton.isSelected = isMutedAudio
        }
    }
    
    private var isBeautyOn = false {
        didSet {
            // improve local render view
            agoraKit.setBeautyEffectOptions(isBeautyOn,
                                            options: isBeautyOn ? beautyOptions : nil)
            beautyEffectButton.isSelected = isBeautyOn
        }
    }
    
    private var isSwitchCamera = false {
        didSet {
            agoraKit.switchCamera()
        }
    }
    
    private var videoSessions = [VideoSession]() {
        didSet {
            placeholderView.isHidden = (videoSessions.count == 0 ? false : true)
            // update render view layout
            updateBroadcastersView()
        }
    }
    
    
    class func LiveRoomViewController() -> LiveRoomViewController {
        return self.initFromStoryboard() as! LiveRoomViewController
    }
    
    private let maxVideoSession = 4
    
    weak var dataSource: LiveVCDataSource?
    
    
    private var firstProduct: Product?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateButtonsVisiablity()
        loadAgoraKit()
        
        chatTabelView.delegate = self
        chatTabelView.dataSource = self
        
        viewModel.getOnlineUsers(organizerUserName: liveInfo.organizerUserName, meetingId: liveInfo.meetingId) { (counts) in
            DispatchQueue.main.async {
                self.usersOnline.text = String(counts)
            }
        }
        
        
        viewModel.observeMessages(organizerId: liveInfo.organizerUserName, meetingId: liveInfo.meetingId) { (message) in
            self.messages.append(message)
            self.chatTabelView.reloadData()
            
        }
        
        
        chatContainer.layer.cornerRadius = 12
        chatContainer.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        userImageView.sd_setImage(with: URL(string: Session.avatar), completed: nil)
        userNameLbl.text = Session.Username
        storeDescLbl.text = "Realtime bidding platform."
        
        self.currentProductPrice.text = "0 KD"
        viewModel.observeAllProducts(organizerUserName: liveInfo.organizerUserName, meetingId: liveInfo.meetingId) { (products) in
            //let reversedProductes = products.reversed()
            if products.count > 0 {
                self.firstProduct = products.first(where: {$0.isSold == false})
                self.currentProductPrice.text = "\(self.firstProduct?.price ?? 0) KD"
            }
        }
        
        
        
        
    }
    
    
    func observeChat() {
        viewModel.$state.sink { (state) in
            switch state {
            case .isLoading:
                break
            case .loaded(let message):
                print("The message is \(message.message)")
            }
        }.store(in: &cancellables)
    }
    
    
    @IBAction func sendBtnTapped(_ sender: Any) {
        guard self.chatTextField.text!.count > 0 else {return}
        viewModel.sendMessage(message: AuctionMessage(senderId: Session.Username, senderName: Session.Username, senderImage: Session.avatar, message: self.chatTextField.text ?? ""), organizerId: liveInfo.organizerUserName, meetingId: liveInfo.meetingId)
        self.chatTextField.text = ""
        self.isChatMode = false
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    //MARK: - ui action
    @IBAction func doSwitchCameraPressed(_ sender: UIButton) {
        isSwitchCamera.toggle()
    }
    
    @IBAction func doBeautyPressed(_ sender: UIButton) {
        isBeautyOn.toggle()
    }
    
    @IBAction func doMuteVideoPressed(_ sender: UIButton) {
        isMutedVideo.toggle()
    }
    
    @IBAction func doMuteAudioPressed(_ sender: UIButton) {
        isMutedAudio.toggle()
    }
    
    @IBAction func doLeavePressed(_ sender: UIButton) {
        leaveChannel()
        if settings.role == .audience {
            viewModel.leaveStreaming(organizerId: liveInfo.organizerUserName, meetindId: liveInfo.meetingId, userId: Session.Username)
        }else {
            viewModel.exitStreaming(organizerId: liveInfo.organizerUserName)
            
        }
        self.dismiss(animated: true) {
            
            self.delegate?.VCDismissed(role: self.settings.role, meetindId: self.liveInfo.meetingId, organizerId: self.liveInfo.organizerUserName)
        }
    }
    
    
    @IBAction func semoeTapped(_ sender: Any) {
        if settings.role == .broadcaster {
            var pNameTextField: UITextField!
            var pPriceTextField: UITextField!
            Util.showTwoTextFieldWithTwoButtons(caller: self, title: "Add Product", message: "Add the new product for bidding", firstTextFieldConfiguration: { (textField) in
                pNameTextField = textField
                textField.placeholder = "Product Name"
            }, secondTextFieldConfiguration: { (textField) in
                pPriceTextField = textField
                textField.placeholder = "Bidding Price"
            }, firstButtonTitle: "Cancel", firstButtonStyle: .cancel, firstButtonHandler: { (cancelAction) in
                
            }, secondButtonTitle: "Add", secondButtonStyle: .default) { (addAction) in
                guard pNameTextField.text!.count > 0 && pPriceTextField.text!.count > 0 else {return}
                if let price = Float(pPriceTextField.text!) {
                    self.viewModel.addNewProduct(meetingId: self.liveInfo.meetingId, product: Product(id: UUID().uuidString, name: pNameTextField.text!, price: price, isSold: false), completion: { result in
                        if result {
                            print("In the result Section")
                            print("Bidding Price is \(price)")
                            self.viewModel.sendMessage(message: AuctionMessage(senderId: Session.Username, senderName: Session.Username, senderImage: Session.avatar, message: "A new product \(String(describing: pNameTextField.text!)) has been added for bidding at bid price \(price)"), organizerId: self.liveInfo.organizerUserName, meetingId: self.liveInfo.meetingId)
                        }
                    })
                }
            }
        }else {
            var priceTextField: UITextField!
            Util.showTextFieldAlertWithTwoButtons(caller: self, title: "Bid on Product", message: "Enter the price to bid", configurationTextField: { (textField) in
                priceTextField = textField
            }, firstButtonTitle: "Cancel", firstButtonStyle: .cancel, firstButtonHandler: { (cancelHandler) in
                
            }, secondButtonTitle: "Bid", secondButtonStyle: .default) { (bidHandler) in
                guard priceTextField != nil else {return}
                guard priceTextField.text!.count > 0 else {return}
                if let bidAmount = Float(priceTextField.text!) {
                    
                    self.viewModel.sendMessage(message: AuctionMessage(id: UUID().uuidString, messageType: .bidding, senderId: Session.Username, senderName: Session.Username, senderImage: Session.avatar, message: "\(Session.Username) has placed the bid for amount \(bidAmount)", price: bidAmount, isBiddingAccepted: false), organizerId: self.liveInfo.organizerUserName, meetingId: self.liveInfo.meetingId)
                }
            }
        }
    }
    
    @IBAction func commentTapped(_ sender: Any) {
        self.isChatMode = true
    }
    
}

//MARK: - Table View Delegates
extension LiveRoomViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.messages.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let messageText = self.messages[indexPath.row].message
        let frame = messageText.size(withAttributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 14)])
        return frame.height + 70.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "AuctionChatCell") as? AuctionChatCell {
            let message = self.messages[indexPath.row]
            cell.message.text = message.message
            cell.username.text = message.senderName
            cell.userImage.sd_setImage(with: URL(string: message.senderImage), completed: nil)
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let message = self.messages[indexPath.row]
        if message.messageType == .bidding && message.biddingAccepted == false && settings.role == .broadcaster {
            // Handle the bidding scanrio here
            Util.showAlertWithTwoButtons(caller: self, title: "Accept Bidding", message: "Accept Bidding Offer from \(message.senderName) for \(message.biddingPrice) KD", firstButtonTitle: "Accept", firstButtonStyle: .default, firstButtonHandler: { (acceptAction) in
                
                self.viewModel.acceptBiddingOffer(organizerName: self.liveInfo.organizerUserName, meetingId: self.liveInfo.meetingId, productId: self.firstProduct?.id ?? "", productName: self.firstProduct?.name ?? "", bidderId: message.senderName, bidderName: message.senderName, message: self.messages[indexPath.row]) { success,productName,bidderName  in
                    if success {
                        // Bidding Offer Accepted
                        self.messages[indexPath.row].biddingAccepted = true
                        self.viewModel.sendMessage(message: AuctionMessage(id: UUID().uuidString, messageType: .simple, senderId: self.liveInfo.organizerUserName, senderName: self.liveInfo.organizerUserName, senderImage: self.liveInfo.organizerImageUrl, message: "The product \(productName) has been sole to \(bidderName) at bid amount of \(message.biddingPrice) KD", price: message.biddingPrice, isBiddingAccepted: false), organizerId: self.liveInfo.organizerUserName, meetingId: self.liveInfo.meetingId)
                    }
                    
                }
                
            }, secondButtonTitle: "Cancel", secondButtonStyle: .cancel) { (cancelAction) in
                
            }
            
        }
    }
    
    
    
}

private extension LiveRoomViewController {
    func updateBroadcastersView() {
        // video views layout
        if videoSessions.count == maxVideoSession {
            broadcastersView.reload(level: 0, animated: true)
        } else {
            var rank: Int
            var row: Int
            
            if videoSessions.count == 0 {
                broadcastersView.removeLayout(level: 0)
                return
            } else if videoSessions.count == 1 {
                rank = 1
                row = 1
            } else if videoSessions.count == 2 {
                rank = 1
                row = 2
            } else {
                rank = 2
                row = Int(ceil(Double(videoSessions.count) / Double(rank)))
            }
            
            let itemWidth = CGFloat(1.0) / CGFloat(rank)
            let itemHeight = CGFloat(1.0) / CGFloat(row)
            let itemSize = CGSize(width: itemWidth, height: itemHeight)
            let layout = AGEVideoLayout(level: 0)
                        .itemSize(.scale(itemSize))
            
            broadcastersView
                .listCount { [unowned self] (_) -> Int in
                    return self.videoSessions.count
                }.listItem { [unowned self] (index) -> UIView in
                    return self.videoSessions[index.item].hostingView
                }
            
            broadcastersView.setLayouts([layout], animated: true)
        }
    }
    
    func updateButtonsVisiablity() {
        guard let sessionButtons = sessionButtons else {
            return
        }
        
        let isHidden = settings.role == .audience
        
        for item in sessionButtons {
            item.isHidden = isHidden
        }
    }
    
    func setIdleTimerActive(_ active: Bool) {
        UIApplication.shared.isIdleTimerDisabled = !active
    }
}

private extension LiveRoomViewController {
    func getSession(of uid: UInt) -> VideoSession? {
        for session in videoSessions {
            if session.uid == uid {
                return session
            }
        }
        return nil
    }
    
    func videoSession(of uid: UInt) -> VideoSession {
        if let fetchedSession = getSession(of: uid) {
            return fetchedSession
        } else {
            let newSession = VideoSession(uid: uid)
            videoSessions.append(newSession)
            return newSession
        }
    }
}

//MARK: - Agora Media SDK
private extension LiveRoomViewController {
    func loadAgoraKit() {
        guard let channelId = settings.roomName else {
            return
        }
        
        setIdleTimerActive(false)
        
        // Step 1, set delegate to inform the app on AgoraRtcEngineKit events
        agoraKit.delegate = self
        // Step 2, set live broadcasting mode
        // for details: https://docs.agora.io/cn/Video/API%20Reference/oc/Classes/AgoraRtcEngineKit.html#//api/name/setChannelProfile:
        agoraKit.setChannelProfile(.liveBroadcasting)
        // set client role
        agoraKit.setClientRole(settings.role)
        
        // Step 3, Warning: only enable dual stream mode if there will be more than one broadcaster in the channel
        agoraKit.enableDualStreamMode(true)
        
        // Step 4, enable the video module
        agoraKit.enableVideo()
        // set video configuration
        agoraKit.setVideoEncoderConfiguration(
            AgoraVideoEncoderConfiguration(
                size: settings.dimension,
                frameRate: settings.frameRate,
                bitrate: AgoraVideoBitrateStandard,
                orientationMode: .adaptative
            )
        )
        
        // if current role is broadcaster, add local render view and start preview
        if settings.role == .broadcaster {
            addLocalSession()
            agoraKit.startPreview()
        }
        
        // Step 5, join channel and start group chat
        // If join  channel success, agoraKit triggers it's delegate function
        // 'rtcEngine(_ engine: AgoraRtcEngineKit, didJoinChannel channel: String, withUid uid: UInt, elapsed: Int)'
        agoraKit.joinChannel(byToken: AgoraAppId, channelId: channelId, info: nil, uid: 0, joinSuccess: nil)
        
        // Step 6, set speaker audio route
        agoraKit.setEnableSpeakerphone(true)
    }
    
    func addLocalSession() {
        let localSession = VideoSession.localSession()
        localSession.updateInfo(fps: settings.frameRate.rawValue)
        videoSessions.append(localSession)
        agoraKit.setupLocalVideo(localSession.canvas)
    }
    
    func leaveChannel() {
        // Step 1, release local AgoraRtcVideoCanvas instance
        agoraKit.setupLocalVideo(nil)
        // Step 2, leave channel and end group chat
        agoraKit.leaveChannel(nil)
        
        // Step 3, if current role is broadcaster,  stop preview after leave channel
        if settings.role == .broadcaster {
            agoraKit.stopPreview()
        }
        
        setIdleTimerActive(true)
        
        navigationController?.popViewController(animated: true)
    }
}

// MARK: - AgoraRtcEngineDelegate
extension LiveRoomViewController: AgoraRtcEngineDelegate {
    
    /// Occurs when the first local video frame is displayed/rendered on the local video view.
    ///
    /// Same as [firstLocalVideoFrameBlock]([AgoraRtcEngineKit firstLocalVideoFrameBlock:]).
    /// @param engine  AgoraRtcEngineKit object.
    /// @param size    Size of the first local video frame (width and height).
    /// @param elapsed Time elapsed (ms) from the local user calling the [joinChannelByToken]([AgoraRtcEngineKit joinChannelByToken:channelId:info:uid:joinSuccess:]) method until the SDK calls this callback.
    ///
    /// If the [startPreview]([AgoraRtcEngineKit startPreview]) method is called before the [joinChannelByToken]([AgoraRtcEngineKit joinChannelByToken:channelId:info:uid:joinSuccess:]) method, then `elapsed` is the time elapsed from calling the [startPreview]([AgoraRtcEngineKit startPreview]) method until the SDK triggers this callback.
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstLocalVideoFrameWith size: CGSize, elapsed: Int) {
        if let selfSession = videoSessions.first {
            selfSession.updateInfo(resolution: size)
        }
    }
    
    /// Reports the statistics of the current call. The SDK triggers this callback once every two seconds after the user joins the channel.
    func rtcEngine(_ engine: AgoraRtcEngineKit, reportRtcStats stats: AgoraChannelStats) {
        if let selfSession = videoSessions.first {
            selfSession.updateChannelStats(stats)
        }
    }
    
    
    /// Occurs when the first remote video frame is received and decoded.
    /// - Parameters:
    ///   - engine: AgoraRtcEngineKit object.
    ///   - uid: User ID of the remote user sending the video stream.
    ///   - size: Size of the video frame (width and height).
    ///   - elapsed: Time elapsed (ms) from the local user calling the joinChannelByToken method until the SDK triggers this callback.
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid: UInt, size: CGSize, elapsed: Int) {
        guard videoSessions.count <= maxVideoSession else {
            return
        }
        
        let userSession = videoSession(of: uid)
        userSession.updateInfo(resolution: size)
        agoraKit.setupRemoteVideo(userSession.canvas)
    }
    
    /// Occurs when a remote user (Communication)/host (Live Broadcast) leaves a channel. Same as [userOfflineBlock]([AgoraRtcEngineKit userOfflineBlock:]).
    ///
    /// There are two reasons for users to be offline:
    ///
    /// - Leave a channel: When the user/host leaves a channel, the user/host sends a goodbye message. When the message is received, the SDK assumes that the user/host leaves a channel.
    /// - Drop offline: When no data packet of the user or host is received for a certain period of time (20 seconds for the Communication profile, and more for the Live-broadcast profile), the SDK assumes that the user/host drops offline. Unreliable network connections may lead to false detections, so Agora recommends using a signaling system for more reliable offline detection.
    ///
    ///  @param engine AgoraRtcEngineKit object.
    ///  @param uid    ID of the user or host who leaves a channel or goes offline.
    ///  @param reason Reason why the user goes offline, see AgoraUserOfflineReason.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid: UInt, reason: AgoraUserOfflineReason) {
        var indexToDelete: Int?
        for (index, session) in videoSessions.enumerated() where session.uid == uid {
            indexToDelete = index
            break
        }
        
        if let indexToDelete = indexToDelete {
            let deletedSession = videoSessions.remove(at: indexToDelete)
            deletedSession.hostingView.removeFromSuperview()
            
            // release canvas's view
            deletedSession.canvas.view = nil
        }
    }
    
    /// Reports the statistics of the video stream from each remote user/host.
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteVideoStats stats: AgoraRtcRemoteVideoStats) {
        if let session = getSession(of: stats.uid) {
            session.updateVideoStats(stats)
        }
    }
    
    /// Reports the statistics of the audio stream from each remote user/host.
    func rtcEngine(_ engine: AgoraRtcEngineKit, remoteAudioStats stats: AgoraRtcRemoteAudioStats) {
        if let session = getSession(of: stats.uid) {
            session.updateAudioStats(stats)
        }
    }
    
    /// Reports a warning during SDK runtime.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurWarning warningCode: AgoraWarningCode) {
        print("warning code: \(warningCode.description)")
    }
    
    /// Reports an error during SDK runtime.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        print("warning code: \(errorCode.description)")
    }
}

