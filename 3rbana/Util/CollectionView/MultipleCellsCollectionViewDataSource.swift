//
//  MultipleCellsCollectionViewDataSource.swift
//  Block6 Academy
//
//  Created by Bilal Mustafa on 04/11/2020.
//

import Foundation
import UIKit

class MultipleCellsCollectionViewDataSource: NSObject {
    private let dataSources: [UICollectionViewDataSource]
    
    init(dataSources: [UICollectionViewDataSource]) {
        self.dataSources = dataSources
    }
}

extension MultipleCellsCollectionViewDataSource: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return dataSources.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let dataSource = dataSources[section]
        return dataSource.collectionView(collectionView, numberOfItemsInSection: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let dataSource = dataSources[indexPath.section]
        let indexPath = IndexPath(row: indexPath.row, section: 0)
        return dataSource.collectionView(collectionView, cellForItemAt: indexPath)
    }
}

