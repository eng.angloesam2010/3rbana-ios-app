//
//  SliderBody.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 14, 2021

import Foundation

struct SliderBody : Codable {
    let avatar : SliderAvatar?
    let websiteLink : String?
    
    enum CodingKeys: String, CodingKey {
        case avatar = "avatar"
        case websiteLink = "websiteLink"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let  avatar = try values.decodeIfPresent(SliderAvatar.self, forKey:.avatar) {
            self.avatar = avatar
        }else {
            self.avatar = nil
        }
        websiteLink = try values.decodeIfPresent(String.self, forKey: .websiteLink)
    }
    
}
