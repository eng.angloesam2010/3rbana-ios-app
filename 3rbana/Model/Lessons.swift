//
//  Lessons.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on November 30, 2020
//
import Foundation

struct Lessons : Codable {

	let Id: Int
	let Description: String
	let LessonURL: String
	let lkpSubjectMaterialTypeId: Int
	let ImageName: String

	private enum CodingKeys: String, CodingKey {
		case Id = "Id"
		case Description = "Description"
		case LessonURL = "LessonURL"
		case lkpSubjectMaterialTypeId = "lkpSubjectMaterialTypeId"
		case ImageName = "ImageName"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		Id = try values.decode(Int.self, forKey: .Id)
		Description = try values.decode(String.self, forKey: .Description)
		LessonURL = try values.decode(String.self, forKey: .LessonURL)
		lkpSubjectMaterialTypeId = try values.decode(Int.self, forKey: .lkpSubjectMaterialTypeId)
		ImageName = try values.decode(String.self, forKey: .ImageName)
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(Id, forKey: .Id)
		try container.encode(Description, forKey: .Description)
		try container.encode(LessonURL, forKey: .LessonURL)
		try container.encode(lkpSubjectMaterialTypeId, forKey: .lkpSubjectMaterialTypeId)
		try container.encode(ImageName, forKey: .ImageName)
	}

}
