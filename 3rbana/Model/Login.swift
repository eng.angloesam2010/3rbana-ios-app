//
//  RootClass.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 30, 2021
//
import Foundation

struct Login: Codable {

	let status   : Bool?
	let message  : String?
	let bodyLogin    : BodyLogin?

	private enum CodingKeys: String, CodingKey {
		case status = "status"
		case message = "message"
		case bodyLogin = "body"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decode(Bool.self, forKey: .status)
		message = try values.decode(String.self, forKey: .message)
        if let  bodyObj = try? values.decode(BodyLogin.self, forKey: .bodyLogin) {
            self.bodyLogin = bodyObj
        }else {
            self.bodyLogin = nil
        }
		
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(status, forKey: .status)
		try container.encode(message, forKey: .message)
        try container.encode(bodyLogin, forKey: .bodyLogin)
	}

}
