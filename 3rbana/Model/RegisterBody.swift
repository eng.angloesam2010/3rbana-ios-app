//
//  File.swift
//  3rbana
//
//  Created by Mac on 13/04/2021.
//

import Foundation

struct RegisterBody : Codable {
    
    let address : String?
    let countryId : String?
    let email : String?
    let name : String?
    let password : String?
    let phone : Int?
    let userId : Int?
    let username : String?
    
    
    enum CodingKeys: String, CodingKey {
        case address = "address"
        case countryId = "countryId"
        case email = "email"
        case name = "name"
        case password = "password"
        case phone = "phone"
        case userId = "userId"
        case username = "username"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        countryId = try values.decodeIfPresent(String.self, forKey: .countryId)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        phone = try values.decodeIfPresent(Int.self, forKey: .phone)
        userId = try values.decodeIfPresent(Int.self, forKey: .userId)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        
    }
    
}
