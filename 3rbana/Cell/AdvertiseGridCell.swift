//
//  AdvertiseGridCell.swift
//  3rbana
//
//  Created by Mac on 28/04/2021.
//

import UIKit

class AdvertiseGridCell  : UICollectionViewCell {
    var cellIdentifier = "AdvertiseGridCell"

    @IBOutlet weak var imgPost: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.imgPost.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    static var identifier: String {
        return String(describing: self)
    }
    
    func configureCell(imgPath:String){
        if let imagePath = imgPath as String? {
            self.imgPost.contentMode = .scaleToFill
            let endPointUrl = EndPoint.pathImage(pathImage:imagePath)
            self.imgPost?.kf.setImage(with: URL(string:endPointUrl.url),placeholder:UIImage(named: "new_logo"))
        }
  
    }
}
