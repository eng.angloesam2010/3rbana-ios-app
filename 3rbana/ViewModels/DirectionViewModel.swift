//
//  CoreDataManager.swift
//  villa vanilla
//
//  Created by Eng Angelo E Saber on 2/1/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation
import CoreData
import UIKit
class DirectionViewModel {
    static let sharedManager = DirectionViewModel()
    private init() {}
    
    func changeLanguage(lang :String) {
        Session.start()
        Session.getLang()
        Session.Language = lang
        Session.setLang()
    }
    func setSemanticContentAttribute() {
        Session.start()
        Session.getLang()
        if Session.Language == "ar" {
            UIImageView.appearance().semanticContentAttribute      = .forceRightToLeft
            UITextField.appearance().semanticContentAttribute      = .forceRightToLeft
            UIView.appearance().semanticContentAttribute           = .forceRightToLeft
            UIButton.appearance().semanticContentAttribute         = .forceRightToLeft
            UITextView.appearance().semanticContentAttribute       = .forceRightToLeft
            UINavigationBar.appearance().semanticContentAttribute  = .forceRightToLeft
            UIViewController().self.view.semanticContentAttribute  = .forceRightToLeft
            UITabBar.appearance().semanticContentAttribute         = .forceRightToLeft
        }else{
            UITextField.appearance().semanticContentAttribute      = .forceRightToLeft
            UIImageView.appearance().semanticContentAttribute      = .forceRightToLeft
            UITabBar.appearance().semanticContentAttribute         = .forceLeftToRight
            UIView.appearance().semanticContentAttribute           = .forceLeftToRight
            UIButton.appearance().semanticContentAttribute         = .forceLeftToRight
            UITextView.appearance().semanticContentAttribute       = .forceLeftToRight
            UINavigationBar.appearance().semanticContentAttribute  = .forceLeftToRight
            UIViewController().self.view.semanticContentAttribute  = .forceLeftToRight
        }
    }
}



