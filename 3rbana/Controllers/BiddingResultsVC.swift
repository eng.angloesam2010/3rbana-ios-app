//
//  BiddingResultsVC.swift
//  3rbana
//
//  Created by Bilal on 29/04/2021.
//

import UIKit
import SDWebImage

class BiddingResultsVC: UIViewController {
    
    
    @IBOutlet weak var navContainerView: UIView!
    @IBOutlet weak var navLbl: UILabel!
    
    @IBOutlet weak var sepratorView: UIView!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var tableView: UITableView!
    
    private var viewModel = AuctionViewModel()
    
    var meetindOrganizerId: String = ""
    var meetindId: String = ""
    
    private var orders: [BidOrders] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        viewModel.getBiddingOrders(organizerId: meetindOrganizerId, meetindId: meetindId) { (orders) -> (Void) in
            self.orders = orders
            self.tableView.reloadData()
        }
    }
    
    
    class func BiddingResultsVC() -> BiddingResultsVC {
        return self.initFromStoryboard() as! BiddingResultsVC
    }
    
    
    func setupTableView() {
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func notificationsTapped(_ sender: Any) {
    }
}

extension BiddingResultsVC: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: BiddingResultsCell.identifier) as? BiddingResultsCell {
            let orderInfo = self.orders[indexPath.row]
            cell.userNameLbl.text = orderInfo.bidderName
            cell.userPhoneLbl.text = "(+97) - 000 1111 02"
            cell.productName.text = orderInfo.productName
            cell.userImage.sd_setImage(with: URL(string: orderInfo.bidderImage), completed: nil)
            cell.productPriceLbl.text = "\(orderInfo.productPrice) KD"
            return cell
        }
        return UITableViewCell()
    }
    
    
}
