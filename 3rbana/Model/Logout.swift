//
//  Body.swift
//
//  Generated using
//  Created on March 30, 2021
//

import Foundation
struct Logout : Codable {
	let status : Bool?
	let message : String?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(Bool.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
	}

}
