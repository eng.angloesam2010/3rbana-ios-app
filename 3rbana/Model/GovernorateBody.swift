//
//  GovernorateBody.swift
//  3rbana
//
//  Created by Mac on 18/04/2021.
//

import Foundation
struct GovernorateBody : Codable {
    let arabicName : String?
    let countryId : Int?
    let governoratesId : Int?
    let name : String?
    enum CodingKeys: String, CodingKey {
        case arabicName = "arabicName"
        case countryId = "countryId"
        case governoratesId = "governoratesId"
        case name = "name"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        arabicName = try values.decodeIfPresent(String.self, forKey: .arabicName)
        countryId = try values.decodeIfPresent(Int.self, forKey: .countryId)
        governoratesId = try values.decodeIfPresent(Int.self, forKey: .governoratesId)
        name = try values.decodeIfPresent(String.self, forKey: .name)
    }
    
}
