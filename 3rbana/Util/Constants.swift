//
//  ArrayConstants.swift
//  villa vanilla
//
//  Created by Eng Angelo E Saber on 1/19/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation


var appDelegate : AppDelegate?
var Lang = "en"
var fontSizeEmptyBttn = 15.0
var fontSizeEmptyLbl  = 20.0
var priceGlobalValue  : String = ""
var knetParam         : NSMutableDictionary = NSMutableDictionary()
var cashParam         : NSMutableDictionary = NSMutableDictionary()
var Customer_ID = 0 // currently static
let settingsEnMenu = ["Notification","Change Language","Login User"]
let settingsArMenu = [" الإشعارات" , "تغيير اللغة" , " تسجيل الدخول"]
let settingsIcon   = ["icons8-appointment_reminders-1","icons8-language-100 (1)","tabbar3selected"]

let settingsLoginEnMenu = ["Notification","Change Language","Update Profile","Change Password","Logout"]
let settingsLoginArMenu = ["الإشعارات" , "تغيير اللغة" , "تعديل بيانات","تعديل كلمة السر","الخروج"]
let settingsLoginIcon   = ["icons8-appointment_reminders-1","icons8-language-100 (1)","tabbar3selected","icons8-approve_and_update_filled-40","icons8-password","icons8-logout_rounded_up_filled-40"]

let isLoginSettingEnMenu = ["Notification","Change Language","Login User"]
let isLoginSettingArMenu = [" الإشعارات" , "تغيير اللغة" , " تسجيل الدخول"]
let INTERNET_ENG_TITLE  = "Internet Connection"
let INTERNET_AR_TITLE    = "اتصال الإنترنت"
let INTERNET_ENG_MESSAGE  = "Please check your network settings!"
let INTERNET_AR_MESSAGE   = "يرجى التحقق من إعدادات الشبكة الخاصة بك!"
let OK_STR = "موافقة"
let TITLE_AR    = "رسالة"
let TITLE_EN    = "Message"
let CANCEL_AR_STR = "الغاء"
let CANCEL_EN_STR = "close"
let CLOSE_STR          = "اغلاق"
let Ok_AR = "موافق"
let OK_EN = "OK"
let Empty_Filed_Arabic       = "من فضلك املا الحقول الفارغة"
let Empty_Filed_English      = "Please Fill Empty Field"
let TITLE_LOADING            = "جاري التحميل"
let Login_En_Msg             = "Invalid Email or password"
let Login_Ar_Msg             = "خطأ في الرقم المدني أو كلمة مرور"
let Update_Message_En       = "Your profile was updated successfully"
let Update_Message_Ar       = "تم تحديث ملفك الشخصي بنجاح"
let Current_Password_Message_En       = "Current Password is Incorrect"
let Current_Password_Message_Ar       = "كلمة المرور السابقة غير صحيحة الرجاء حاول مرة اخري"
let Confirm_Password_Message_En       = "Confirm password not match"
let Confirm_Password_Message_Ar       =  "تأكيد كلمة المرور غير متطابقة"

let Logout_Message_En       = "Are You Sure to Logout From App ?"
let Logout_Message_Ar       = "هل انت متاكد من الخروج من التطبيق ؟ "
let CHECKMAILAR     = "من فضلك تاكد من ان البريد الالكتروني  مكتوب بطريقة صحيحية"
let CHECKMAILEN     = "Please Check for mail format"

let CHECKTELEPHONEAR     = "من فضلك ادخل رقم التليفون بطريقة صحيحة"
let CHECKELEPHONEEN     = "Please enter the correct phone number"


let PASSWORDLENTHEN     = "Password is must be greater than 5 character"
let PASSWORDLENTHAR     = "يجب أن تكون كلمة المرور أكبر من 5 أحرف "

let Yes_EN       = "YES"
let Yes_AR       =  "نعم"

let NO_EN       = "NO"
let NO_AR       =  "لا"

let Logout_title_En = "Sign out of Account"
let Logout_title_Ar = "تسجيل الخروج من الحساب"

let Created_User_En       = "User was Successfully created !"
let Created_User_Ar       = "تم انشاء المستخدم بنجاح"

let Logged_Before_User_En       = "Email, phone number was already in used"
let Logged_Before_User_Ar       = "البريد الالكتروني او رقم التليفون مستخدم من قبل"


let NewPassSameOldPassEN  = "Please Change New Password Beause this is the same old password"
let NewPassSameOldPassAR   = " .الرجاء تغيير كلمة المرور الجديدة لأن هذه هي نفس كلمة المرور القديمة "


let weakPasswordEn       = "Password must be more than 6 character"
let weakPasswordAr       = "كلمة المرور يجب ان تكون اكبر من ٦ حروف"
let Email_En       = "Email address in correct format"
let Email_Ar       = "صيغة البريد الالكتروني خاطئة"


let Password_Updated_Successfully_En       = "The user password has been modified successfully"
let Password_Updated_Successfully_Ar       = "تم تعديل كلمة مرور المستخدم بنجاح"


let Password_Forget_Successfully_En    = "The Email or Username Is entered Correct, we will send the new password in mail."
let Password_Forget_Successfully_Ar    = "  البريد الإلكتروني أو اسم المستخدم الذي ادخلته صحيح و سوف نرسل كلمة المرور الجديدة عبر البريد الإلكتروني"
let Password_Forget_Fialure_En         = "Failure, Please Try Again. Email or Username is entered was Incorrect ."
let Password_Forget_Fialure_Ar         = "فشل , حاول مرة أخرى. تم إدخال البريد الإلكتروني أو اسم المستخدم غير صحيح ."
let Change_Lang_title_En       = "Change Language"
let Change_Lang_tilte_Ar       =  "تغيير اللغة"
let Change_Lang_Msg_En       = "English"
let Change_Lang_Msg_Ar       =  "العربية"
let Loggined_User_En       = "Welcome Back "
let Loggined_User_Ar       =  "مرحبا بعودتك"
let Logout_User_En       = "You Logout From App!"
let Logout_User_Ar       =  "انت خرجت من التطبيق الان"
let DATE_TIME_EMPTY_FIELD_AR       = " من فضلك اختر وقت وتاريخ التسليم"
let DATE_TIME_EMPTY_FIELD_EN       = "Please choose the time and date of delivery "
let SELECTION_DATE_EN              = "Please, Select Order Delivery Date"

let settingsAr = "الإعدادات"
let settingsEn = "Settings"
let homeAr = "الرئيسية"
let homeEn = "Home"
let RegisterTitleAr = "تسجيل مستخدم جديد"
let RegisterTitleEn = "Register New User"
let loginTitleEn = "Login User"
let loginTitleAr = "تسجيل الدخول" 
let internetConnectionAr     = "لا يوجد اتصال بالإنترنت"
let internetConnectionEn     = "No Internet Connection"
let internetConnectionBttnAr = "المحاولة مرة اخري"
let internetConnectionBttnEn = "Try Again"


let ConnectionIssueAr  =  "حدثت مشكلة في الاتصال يرجى إعادة المحاولة لاحقا"
let ConnectionIssueEn  =  "There was a connection problem, please try again later" 

let ItemEmptyEn      = "No Data"


let NoVideoDescibAr     = "لا يوجد فيديوهات لهذه المادة"

let IssueLoadingEn     = "There is a problem please try again."
let IssueLoadingAr     = "يوجد مشكلة برجاء محاولة مرة اخري "


let Progress             = "جاري التحميل ...";

let FavTitleAr     = "المفضلة"
let FavTitleEn     = "Favorite"


let CartTitleAr     = "سلة المشتريات"
let CartTitleEn     = "Shopping Cart"


let NoPostEn = "No Post in this Category yet"
let NoPostAr =  "لا اعلانات في هذه الفئة حتى الان"


let AdvertTitleAr     = "العروض"
let AdvertTitleEn     = "Offers"


let emptAdvertDescribEn = "There are no Offers now."
let emptAdvertDescribAr = "لا يوجد عروض مسجلة الان "

let TransitionTitleAr     = "إتمام الطلب"
let TransitionTitleEn     = "Compelete Order"


let OrderTitleAr     = "الطلبات السابقة"
let OrderTitleEn     = "Previous orders"

let OrderDescribEn = "No Orders Placed"
let OrderDescribAr = "لا يوجد طلبات سابقة"

let OrderDetailsTitleAr     = "تفاصيل الطلب"
let OrderDetailsTitleEn     = "Order details"

let OrderDetailsDescribEn = "There are no details for this order"
let OrderDetailsDescribAr = "لا يوجد تفاصيل لهذا الطلب"




let SelectSectionAr   = "من فضلك اختر اسم اخر "
let SelectSectionEn    = "Please change username"


let ServerIssuesARMsg = "عفوا يوجد مشكلة في الاتصال بالسيرفر حاول مرة اخري"
let ServerIssuesMsg = "عفوا يوجد مشكلة في الاتصال بالسيرفر حاول مرة اخري"


let return_connection_bttn  = "اعادة الاتصال"

let try_connection_bttn = "حاول مرة أخرى"

let TitleBttnAr = "حاول مرة أخرى"
let TitleBttnEn = "Try Again"

let MESSAGE_HOME        = "مرحبا  بكم في جالبوت"
let VIDEOS_TITLE_HOME   = "هيا بنا نكتشف جزر الكويت"
let TITLE_SCREEN_ONE    = "تحديد المرحلة التعليمية"

let View_Pofile_Title  = " عرض الصفحة الشخصية"
let Logout_Title           = " تسجيل الخروج من التطبيق"
let Application_Title = " تطبيق جلبوت"
let Cancel_Title = "إغلاق"
let Island_Title = "هيا بنا نكتشف جزر الكويت"


let selectCountryEn = "Select Country"
let selectCountryAr = "آختر البلد"

let MenuLoginArrayAr = ["الأسئلة" , "المساعدة" , "اللغة" , "البلد" , "المفضلة" , "الإشعار" , "تعديل كلمة السر","تسجيل الخروج"  ]
let MenuLoginArrayEn = ["Questions","Help","Language","Country","Favoriates","Notifiacation","Change Password","Logout"]
let MenuLoginWithoutArrayAr = ["الأسئلة" , "المساعدة" , "اللغة" , "البلد" ]
let MenuLoginWithoutArray   = ["Questions","Help","Language","Country"]

let MenuImageLogin          = ["help 1","like 1","Country","global 1","Vector","Group 191","image 37","logout 1"]
let MenuImageWithoutLogin   = ["help 1","like 1","Country","global 1"]


let allGoverEN  = "All"
let allGoverAR  = "الكل"

let AgoraAppId: String = "65f70fef502a4db98002e3e4d0ef74a5"
