//
//  UpdatePassVC.swift
//  3rbana
//
//  Created by Mac on 09/04/2021.
//

import UIKit

class UpdatePassVC : UIViewController,BackNavigationDelegate {
    
    @IBOutlet weak var backNavigationBar: BackNavigationBar!
    
    lazy var updatePasswordViewModel: UpdatePasswordViewModel = {
        return UpdatePasswordViewModel()
    }()
    
    @IBOutlet weak var titleStrLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.titleStrLbl.text = "UpdatePasswordTitle" .localizableString(loc:"en")
                self.titleStrLbl.font = AppTheme.FontArabic(fontSize: 22.0)
            }else{
                self.titleStrLbl.font = AppTheme.FontArabic(fontSize: 22.0)
                self.titleStrLbl.text = "UpdatePasswordTitle" .localizableString(loc:"ar-EG")
            }
            self.titleStrLbl.textColor = AppTheme.setGoldenColor()
        }
    }
    
    @IBOutlet weak var passwordTxtField: UITextField!{
        didSet {
            if Session.Language == "en" {
                self.passwordTxtField.attributedPlaceholder = self.passwordTxtField.setPlaceholder(string: "Password".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.passwordTxtField.textAlignment = .left
                
                
            }else{
                self.passwordTxtField.attributedPlaceholder = self.passwordTxtField.setPlaceholder(string: "Password".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                
                self.passwordTxtField.textAlignment = .right
            }
            self.passwordTxtField.textColor = AppTheme.setGoldenColor()
            self.passwordTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    
    @IBOutlet weak var newPasswordTxtField: UITextField!{
        didSet {
            
            if Session.Language == "en" {
                self.newPasswordTxtField.attributedPlaceholder = self.newPasswordTxtField.setPlaceholder(string: "Password".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.newPasswordTxtField.textAlignment = .left
                
            }else{
                self.newPasswordTxtField.attributedPlaceholder = self.newPasswordTxtField.setPlaceholder(string: "Password".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.newPasswordTxtField.textAlignment = .right
            }
            self.newPasswordTxtField.textColor = AppTheme.setGoldenColor()
            self.newPasswordTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    
    @IBOutlet weak var confirmPasswordTxtField: UITextField!{
        didSet {
            
            if Session.Language == "en" {
                self.confirmPasswordTxtField.attributedPlaceholder = self.confirmPasswordTxtField.setPlaceholder(string: "Password".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.confirmPasswordTxtField.textAlignment = .left
                
            }else{
                self.confirmPasswordTxtField.attributedPlaceholder = self.confirmPasswordTxtField.setPlaceholder(string: "Password".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.confirmPasswordTxtField.textAlignment = .right
            }
            self.confirmPasswordTxtField.textColor = AppTheme.setGoldenColor()
            self.confirmPasswordTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    
    @IBOutlet weak var changePasswordBttn: UIButton!{
        didSet {
            self.changePasswordBttn.setTitleColor(AppTheme.setBlackColor(), for: UIControl.State.normal)
            if Session.Language == "en"{
                self.changePasswordBttn.setTitle("ChangePassword".localizableString(loc:"en"), for: UIControl.State.normal)
                self.changePasswordBttn.titleLabel?.font = AppTheme.FontArabic(fontSize: 18.0)
            }
            else{
                self.changePasswordBttn.titleLabel?.font = AppTheme.FontArabic(fontSize: 18.0)
                self.changePasswordBttn.setTitle("ChangePassword".localizableString(loc:"ar-EG"), for: UIControl.State.normal)
            }
        }
    }
    
    fileprivate func setValuetoTxtField() {
        // Do any additional setup after loading the view.
        self.passwordTxtField.text = self.updatePasswordViewModel.oldPasswordString
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backNavigationBar?.delegateProfile = self
        setValuetoTxtField()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func backViewController() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    
    func notificationViewController() {
        self.pushViewController(id: 6)
    }
    
    
    @IBAction func changePassword(_ sender: Any) {
        dismissKeyboard()
        // pass UserName and Password to LoginModel
        self.updatePasswordViewModel.passwordModel = PasswordModel(oldPassword: self.passwordTxtField.text!,
                                                                   newPassword:  self.newPasswordTxtField.text! ,
                                                                   confirmPassword: self.confirmPasswordTxtField.text!)
        
        // MARK: - when colusre was updaete alert will be shown
        self.updatePasswordViewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.updatePasswordViewModel.alertMessage {
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    let alert = AlertActionManager.Error
                    alert.getAlert(errorDescribe: message)
                }
            }
        }
        
        self.updatePasswordViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.updatePasswordViewModel.state {
                case .empty, .error, .noconnect:
                    DispatchQueue.main.async {
                        if let message = self.updatePasswordViewModel.alertMessage {
                            self.passwordTxtField.text        = ""
                            self.newPasswordTxtField.text     = ""
                            self.confirmPasswordTxtField.text = ""
                            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                            let alert = AlertActionManager.Error
                            alert.getAlert(errorDescribe: message)
                        }
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    DispatchQueue.main.async {
                        if let message = self.updatePasswordViewModel.alertMessage {
                            self.passwordTxtField.text        = ""
                            self.newPasswordTxtField.text     = ""
                            self.confirmPasswordTxtField.text = ""
                            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                            let alert = AlertActionManager.succes
                            alert.getAlert(errorDescribe: message)
                        }
                    }
                }
            }
        }
        
        self.updatePasswordViewModel.UpdatePasswordUser()
    }
}
