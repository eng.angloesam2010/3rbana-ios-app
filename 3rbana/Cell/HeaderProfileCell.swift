//
//  HeaderProfileCell.swift
//  3rbana
//
//  Created by Mac on 31/03/2021.
//

import UIKit
protocol UpdateProfileDelegate {
    func goUpdateProfileVC()
}


class HeaderProfileCell: UITableViewCell {
    var cellIdentifier = "HeaderProfileCell"
    
    @IBOutlet weak var userNameLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.userNameLbl.font = AppTheme.FontArabic(fontSize: 22.0)
                self.userNameLbl.textAlignment = .left
                
            }else{
                self.userNameLbl.font = AppTheme.FontArabic(fontSize: 22.0)
                self.userNameLbl.textAlignment = .right
            }
        }
    }

    var delegate : UpdateProfileDelegate?
    @IBOutlet weak var postTitleLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.postTitleLbl.text = "post".localizableString(loc: "en")
               // self.postTitleLbl.font = AppTheme.FontArabic(fontSize: 18.0)
                
            }else{
                
                self.postTitleLbl.text = "post".localizableString(loc: "ar-EG")
                self.postTitleLbl.font = AppTheme.FontArabic(fontSize: 18.0)
            }
        }
    }

    @IBOutlet weak var numPostLbl: UILabel!
    @IBOutlet weak var specialTitleLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.specialTitleLbl.text = "offer".localizableString(loc: "en")
               // self.postTitleLbl.font = AppTheme.FontArabic(fontSize: 18.0)
                
            }else{
                
                self.specialTitleLbl.text = "offer".localizableString(loc: "ar-EG")
                self.specialTitleLbl.font    = AppTheme.FontArabic(fontSize: 18.0)
            }
        }
    }
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var numSpecialLbl: UILabel!
    @IBOutlet weak var followeLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.followeLbl.text = "follow".localizableString(loc: "en")
               // self.postTitleLbl.font = AppTheme.FontArabic(fontSize: 18.0)
                
            }else{
                
                self.followeLbl.text = "follow".localizableString(loc: "ar-EG")
                self.followeLbl.font    = AppTheme.FontArabic(fontSize: 18.0)
            }
        }
    }
    @IBOutlet weak var numbFollowersLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    
    static var identifier: String {
         return String(describing: self)
     }
     
    @IBAction func updateProfileVC(_ sender: Any) {
        self.delegate?.goUpdateProfileVC()
    }
    
    func configureCell(userNameStr : String ,postNumStr : String,specialStr : String,followerStr :  String) {
        self.userNameLbl.text = "\(userNameStr)"
        Session.start()
        if let avatar = Session.avatar as String?  {
            let endPointUrl = EndPoint.pathImage(pathImage:avatar)
            self.avatarImageView.kf.setImage(with: URL(string:endPointUrl.url),placeholder:UIImage(named: "new_logo"))
        }
    }
    
    
    
}
