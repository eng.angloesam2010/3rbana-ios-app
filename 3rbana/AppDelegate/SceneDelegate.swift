//
//  SceneDelegate.swift
//  al Jalboot app
//
//  Created by Eng Angelo E Saber on 11/1/20.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        
        Session.start()
        Session.getSelectedCountry()
        DirectionViewModel.sharedManager.setSemanticContentAttribute()
        guard let _ = (scene as? UIWindowScene) else { return }
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            Session.start()
            if Session.SelectedCountry == -1 || Session.SelectedCountry == 0 {
                let countryVC = storyboard.instantiateViewController(withIdentifier: "CountryVC")
                window.rootViewController = countryVC
                self.window = window
            }else{
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let tbc   = storyBoard.instantiateViewController(withIdentifier:"tabbar") as! UITabBarController

                tbc.selectedIndex = 2
                tbc.modalPresentationStyle = .fullScreen
                window.rootViewController    = tbc
                self.window = window
               
            }
          
            window.makeKeyAndVisible()
        }
    }
    
    
    func restartApp() {
        Session.start()
        Session.getLang()
        Session.getSelectedCountry()
        DirectionViewModel.sharedManager.setSemanticContentAttribute()
        guard let _ = (scene as? UIWindowScene) else { return }
        if let windowScene = scene as? UIWindowScene {
            let window = UIWindow(windowScene: windowScene)
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            if Session.SelectedCountry == -1 {
                let countryVC = storyboard.instantiateViewController(withIdentifier: "CountryVC")
                window.rootViewController = countryVC
                self.window = window
            }else{
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let tbc   = storyBoard.instantiateViewController(withIdentifier:"tabbar") as! UITabBarController
                tbc.selectedIndex = 2
                tbc.modalPresentationStyle = .fullScreen
                window.rootViewController    = tbc
                self.window = window
               
            }
          
            window.makeKeyAndVisible()
        }
    }
    
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}

