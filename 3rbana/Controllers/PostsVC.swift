//
//  PostsVC.swift
//  3rbana
//
//  Created by Mac on 14/04/2021.
//

import UIKit

class PostVC : UIViewController,BackNavigationDelegate, UICollectionViewDelegate{
    
    @IBOutlet weak var widthCollectionView: NSLayoutConstraint!
    @IBOutlet weak var countryCollectionView: UICollectionView!
    @IBOutlet weak var listView: UIView!
    @IBOutlet weak var gridView: UIView!
    @IBOutlet weak var backNavigationBar: BackNavigationBar!
    @IBOutlet weak var titleLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.titleLbl.text = "post_title".localizableString(loc:"en")
                self.titleLbl.textAlignment = .left
            }else{
                self.titleLbl.text = "post_title".localizableString(loc:"ar-EG")
                self.titleLbl.textAlignment = .right
            }
            self.titleLbl.textColor = AppTheme.setGoldenColor()
            self.titleLbl.tintColor = AppTheme.setGoldenColor()
        }
    }
    
    // MARK: - Model for Loading Category
    lazy var governoratesViewModel: GovernoratesViewModel = {
        return GovernoratesViewModel()
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backNavigationBar?.delegateProfile = self
        self.initCollectionView()
        self.startFetchGovernment()
        NotificationCenter.default.addObserver(self, selector: #selector(refetchGovernmente), name: Notification.Name("refetchGovernmente"), object: nil)
    }
    
    
    func initCollectionView(){
        // Do any additional setup after loading the view, typically from a nib.
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top:5.0, left: 5.0, bottom: 5.0, right: 5.0)
        layout.scrollDirection = .horizontal
        layout.itemSize = CGSize(width:widthGoverCollView, height:heightGoverCollView)
        layout.minimumInteritemSpacing = 5.0
        self.countryCollectionView!.collectionViewLayout = layout
        self.countryCollectionView!.isPagingEnabled = true
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func backViewController() {
        navigationController?.popViewController(animated: true)
    }
    
    func notificationViewController() {
        self.pushViewController(id: 6)
    }
    @IBAction func displayListVC(_ sender: Any) {
        self.listView.isHidden = false
        self.gridView.isHidden = true
    }
    @IBAction func displayGridVC(_ sender: Any) {
        self.listView.isHidden = true
        self.gridView.isHidden = false
    }
    
    @objc func refetchGovernmente(notification: NSNotification){
        if let governmentId = notification.userInfo?["goverSelectedIndex"] as? Int {
            
            let goverSelectedIndex : [String: Int?] = ["goverSelectedIndex": governmentId]
            NotificationCenter.default.post(name: Notification.Name("startFetchObserver"), object: nil,userInfo: goverSelectedIndex as [AnyHashable : Any])

        }
    }
    // MARK: - Fetch Level Part
    func startFetchGovernment(){
        // MARK: - when colusre was updaete alert will be shown
        
        self.governoratesViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.governoratesViewModel.state {
                case .empty, .error:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    self.reloadCollectionView()
                case .noconnect:
                    DispatchQueue.main.async {
                    }
                }
            }
        }
        
        self.governoratesViewModel.fetchGovernment()
        
    }
    
    func reloadCollectionView(){
        self.countryCollectionView?.delegate      = self.governoratesViewModel
        self.countryCollectionView?.dataSource     = self.governoratesViewModel
        self.countryCollectionView?.reloadData()
        self.countryCollectionView?.layoutIfNeeded()
        self.countryCollectionView?.setNeedsFocusUpdate()
        self.widthCollectionView.constant = (self.countryCollectionView?.collectionViewLayout.collectionViewContentSize.width)!
        print("\(self.widthCollectionView.constant)")
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
}



