//
//  NewtorlLayer.swift
//  Combine_MVVM
//
//  Created by Mac on 12/29/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import Alamofire

class APIServiceRequest : APIServiceType{
    
    
    func fetchGenericData<T : Decodable>(url : String, withParameters Parameters: NSDictionary,httpMethod: HTTPMethod ,withHeader header : HTTPHeaders,completion: @escaping  (ResultApi<T, APIServiceError>) -> ()){
        AF.request(url, method: httpMethod , parameters: Parameters as? Parameters, encoding: URLEncoding.default, headers: header).decodable(success: { data in
            completion(.success(data))
        }, failure: { error  in
            completion(.failure(APIServiceError.parserError(reason: error!.localizedDescription)))
        })
    }
    
    func requestWithToken<T : Decodable>(url : String,method: HTTPMethod , withParameters parameters: NSDictionary,withHeader header : HTTPHeaders,access_token : String,completion: @escaping  (ResultApi<T, APIServiceError>) -> ()) {
        var request             = URLRequest(url: URL(string: url)!)
        request.timeoutInterval =  10.0
        //request.httpMethod     = "POST"
        request.cachePolicy     = .useProtocolCachePolicy
        request.setValue("Bearer \(access_token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod =  HTTPMethod.post.rawValue
        // Your required parameter in case of Post request
        
        
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters )
        AF.request(request).decodable(success: { data in
            completion(.success(data))
        }, failure: { error  in
            completion(.failure(APIServiceError.parserError(reason: error!.localizedDescription)))
        })
        
    }
    
    func fetchGenericToken<T : Decodable>(url : String,access_token : String,completion: @escaping  (ResultApi<T, APIServiceError>) -> ()) {
        var request             = URLRequest(url: URL(string: url)!)
        request.timeoutInterval =  10.0
        request.cachePolicy     = .useProtocolCachePolicy
        request.setValue("Bearer \(access_token)", forHTTPHeaderField: "Authorization")
        request.httpMethod =  HTTPMethod.get.rawValue
        AF.request(request).decodable(success: { data in
            completion(.success(data))
        }, failure: { error  in
            completion(.failure(APIServiceError.parserError(reason: error!.localizedDescription)))
        })
    }
    
    
    func fetchGenericDataPostWithBody<T : Decodable>(url : String, withParameters parameters: NSDictionary,access_token : String,completion: @escaping(ResultApi<T, APIServiceError>) -> ()) {
        var request             = URLRequest(url: URL(string: url)!)
        request.timeoutInterval =  10.0
        request.cachePolicy     = .useProtocolCachePolicy
        request.setValue("Bearer \(access_token)", forHTTPHeaderField: "Authorization")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpMethod =  HTTPMethod.post.rawValue
        request.httpBody = try! JSONSerialization.data(withJSONObject: parameters, options: [])
        AF.request(request).decodable(success: { data in
            completion(.success(data))
        }, failure: { error  in
            completion(.failure(APIServiceError.parserError(reason: error!.localizedDescription)))
        })
    }
    
    func apiPostMultipartFormData<T : Decodable>(url : String, withParameters parameters: NSDictionary,access_token : String ,imageRequest:ImageRequest,completion: @escaping(ResultApi<T, APIServiceError>) -> ()) {
        AF.upload(multipartFormData: { multipartFormData in
            for (key,value) in parameters {
                multipartFormData.append((value as! String).data(using: .utf8)!, withName: key as! String)
            }
            if let image = imageRequest.image {
                let jpegData = image.jpegData(compressionQuality: 1.0)
                multipartFormData.append(Data((jpegData)!), withName: "avatar",fileName: imageRequest.fileName, mimeType: "image/jpg")
            }
        }, to:url, headers: ["Authorization": "Bearer \(access_token)"]).decodable(success: { data in
            completion(.success(data))
        }, failure: { error  in
            completion(.failure(APIServiceError.parserError(reason: error!.localizedDescription)))
        })

    }
}

extension Alamofire.DataRequest {
    @discardableResult
    func decodable<T: Decodable>(success: @escaping (T) -> Swift.Void, failure: @escaping (APIServiceError?) -> Swift.Void) -> Self {
        response(completionHandler: { response in
            guard let httpResponse = response.response as HTTPURLResponse? else {
                failure(APIServiceError.unknown)
                return
            }
            if (httpResponse.statusCode == 401) {
                failure(APIServiceError.apiError(reason: "Unauthorized"))
                return
            }
            if (httpResponse.statusCode == 403) {
                failure(APIServiceError.apiError(reason: "Resource forbidden"))
                return
            }
            if (httpResponse.statusCode == 404) {
                failure(APIServiceError.apiError(reason: "Resource not found"))
                return
            }
            if (405..<500 ~= httpResponse.statusCode) {
                failure(APIServiceError.apiError(reason: "client error"))
                return
            }
            
            if response.error != nil {
                failure(APIServiceError.urlError(reason: response.error!.localizedDescription))
                return
            }
            if let data = response.data {
                
                print("\(String(describing: try? JSONDecoder().decode(Login.self, from: data)))")
                guard let result = try? JSONDecoder().decode(T.self, from: data)  else {
                    failure(APIServiceError.parserError(reason: "Parse Error"))
                    return
                }
                success(result)
            }
        })
        return self
    }
}




