//
//  String.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 12/10/20.
//

import Foundation
import UIKit

extension String {
    func validateCivilID(civilID: String) -> Bool {
        // print("\(civilID)")
        let regex = "[0-9]{12}"
        let civilIDPredicate = NSPredicate(format: "SELF MATCHES %@", regex)
        if  civilIDPredicate.evaluate(with: civilID) {
            let characters    = Array(civilID)
            let oParameter    = (Int(String(characters[0]))!  * 2)
            let tParameter    = (Int(String(characters[1]))!  * 1)
            let threParameter = (Int(String(characters[2]))!  * 6)
            let fouParameter  = (Int(String(characters[3]))!  * 3)
            let fiParameter   = (Int(String(characters[4]))!  * 7)
            let sixParameter  = (Int(String(characters[5]))!  * 9)
            let seParameter   = (Int(String(characters[6]))!  * 10)
            let eParameter    = (Int(String(characters[7]))!  * 5)
            let nParameter    = (Int(String(characters[8]))!  * 8)
            let tenParameter  = (Int(String(characters[9]))!  * 4)
            let elParameter   = (Int(String(characters[10]))!  * 2)
            //
            let  civilIDDouble  =  11 - ( (oParameter + tParameter + threParameter +
                                            fouParameter + fiParameter + sixParameter + seParameter + eParameter+eParameter
                                            + nParameter + tenParameter + elParameter) % 11)
            
            if civilIDDouble == Int(String(characters[11])){ return true }
            
            return false
        }
        return false
    }
    func localizableString(loc:String) -> String{
        let path   = Bundle.main.path(forResource: loc, ofType: "lproj")
        if let bundle = Bundle(path: path!) {
            return NSLocalizedString(self,tableName: nil,bundle:bundle,value:"",comment: "")
        }else {
            return ""
        }
    }
    
    
    public func isPhone()->Bool {
        if self.isAllDigits() == true {
            let phoneRegex = "[235689][0-9]{6}([0-9]{3})?"
            let predicate = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
            return  predicate.evaluate(with: self)
        }else {
            return false
        }
    }
    
    private func isAllDigits()->Bool {
        let charcterSet  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcterSet)
        let filtered = inputString.joined(separator: "")
        return  self == filtered
    }
    
    func frame(_ size: CGSize, _ font: UIFont) -> CGRect {
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return (self as NSString).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font: font], context: nil)
      }
}



