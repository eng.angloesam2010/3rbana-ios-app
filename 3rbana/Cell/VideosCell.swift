//
//  VideosCell.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/30/20.
//

import UIKit

class VideosCell: UITableViewCell {
    var orderCellIdentifier = "VideosCell"
    @IBOutlet weak var videoImage: UIImageView!
    @IBOutlet weak var lessonTitleLbl: UILabel!
    @IBOutlet weak var subjectNameLbl: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    func configureCell(model videoModel : VideoCellViewModel){
        let endPointUrl = EndPoint.pathImage(pathImage: videoModel.VideoImagePathUrl)
        self.videoImage.kf.setImage(with: URL(string:endPointUrl.url),placeholder: UIImage(named: "video_icon"))
        self.lessonTitleLbl.text = videoModel.VideoTitle
        //self.subjectNameLbl.text = videoModel.SubjectTitle
    }
    static var identifier: String {
        return String(describing: self)
    }
    
}
