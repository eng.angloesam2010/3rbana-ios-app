//
//  SubjectsListVC.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/25/20.
//

import Foundation
import UIKit
import AnimatableReload
import EmptyKit




class VideosListVC :  UIViewController , EmptyDataSourceProtocal,NavigationBarProfileDelegate{
    
    @IBOutlet weak var bgView: UIImageView! {
        didSet{
            Session.start()
            if !Session.avatar.isEmpty {
                self.bgView.image = (Session.avatar == "1") ?  UIImage(named:"bg_b") :  UIImage(named:"bg_g")
            }
        }
    }
    
    @IBOutlet weak var navBarView: NavigationBar!
    @IBOutlet weak var videosTableView: UITableView!{
        didSet {
            self.videosTableView?.register(UINib(nibName: VideosCell.identifier, bundle: nil), forCellReuseIdentifier: VideosCell.identifier)
        }
    }
    // MARK: - Property of view controller
    var typeInt: Int = 0
    var levelCellViewModel : VideosModel?
    lazy var videoListViewModel : VideosListViewModel = {
        return VideosListViewModel()
    }()
    var titleSubject : String!
    // MARK: - init for Level VC
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       startFetchVideos()
        // set property of custome navigator
       setPropertyTitleForNav()
        // start currnt delageta
        self.videoListViewModel.delegate = self 

    }
    // MARK: - set property of custome navigator
    fileprivate func setPropertyTitleForNav(){
        self.navBarView.title           = titleSubject
        self.navBarView.screenTitleStr  = VIDEOS_TITLE_HOME
        self.navBarView.BackButtonImage = "back_bttn"
        self.navBarView.delegateProfile = self // set delegate for Navigator bar
    }
    
    func backViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    // MARK: - Fetch Level Part
    func startFetchVideos(){
        
       
        // MARK: - when colusre was updaete alert will be shown
        
        self.videoListViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.videoListViewModel.state {
                case .empty, .error:
                    self.typeInt = 2
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    DispatchQueue.main.async {
                        self.reloadTableView()
                    }
                case .noconnect:
                    self.typeInt = 1
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                }
            }
        }

        self.videoListViewModel.startFetchSubjectList()
        
    }
    // MARK: - init Collectionview
    func initTableView(){
        self.videosTableView?.register(UINib(nibName: IslandCell.identifier, bundle: nil), forCellReuseIdentifier: IslandCell.identifier)
    }
    // MARK: - Reload Collectionview
    func reloadTableView() {
        initTableView()
        self.videosTableView?.dataSource = self.videoListViewModel
        self.videosTableView?.delegate   = self.videoListViewModel
        self.videosTableView?.reloadData()
        self.startAnimation()
        
    }
    
    // MARK: - Set datasource for collectionview
    func setEmptyDataSet(){
        self.videosTableView.ept.dataSource = self as EmptyDataSource
        self.videosTableView.ept.delegate   = self as EmptyDelegate
        self.videosTableView.ept.reloadData()
        self.videosTableView.alwaysBounceVertical = false
        self.videosTableView.isScrollEnabled = false
        self.videosTableView.isPagingEnabled = false
        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
        
    }
    
    // MARK: - Start Animation for view controller
    func startAnimation(){
        AnimatableReload.reload(tableView:  self.videosTableView, animationDirection: "left")
    }
    
    // MARK: - Back button tom return in back screen
    func popViewController() {
        dismiss(animated: true, completion: nil)
    }
    
    
    
}
extension VideosListVC : EmptyDelegate ,VideoTableViewDelegate {
    func didSelectCell(titleVideo: String, urlVideo: String) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let previewVideoVC = storyBoard.instantiateViewController(withIdentifier: "PreviewVideoVC") as! PreviewVideoVC
        previewVideoVC.titleVideo = titleVideo
        previewVideoVC.UrlVideo   = urlVideo
        previewVideoVC.modalPresentationStyle = .fullScreen
        self.present(previewVideoVC, animated: true, completion: nil)
    }
    
    
    @objc(emptyButton:tappedIn:) func emptyButton(_ button: UIButton, tappedIn view: UIView) {
        self.startFetchVideos()
    }

}




