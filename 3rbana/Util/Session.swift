//
//  SaveManager.swift
//  Sport Finder
//
//  Created by MacBook on 10/25/18.
//  Copyright © 2018 MacBook. All rights reserved.
//
import Foundation
import KeychainAccess
class Session {
    static let  defaults      = UserDefaults.standard
    static let  keyChain      = Keychain(service: "com.eblock6.3rbana")
    static var  Language      = ""
    static var  Username      = ""
    static var  UserMail      = ""
    static var  password     = ""
    static var  Phone    = ""
    static var  Name  = ""
    static var  CutID = ""
    static var  CountryID = ""
    static var  address   = ""
    static var  avatar          = ""
    static var  accessToken   = ""
    static var  refreshToken  = ""
    static var  SelectedCountry  = -1
    
    static func reset()
    {
        keyChain["Username"] = ""
        keyChain["UserMail"] = ""
        keyChain["Password"] = ""
        keyChain["Phone"]    = ""
        keyChain["CutID"]    = ""
        keyChain["address"]  = ""
        keyChain["avatar"]   = ""
        keyChain["accessToken"]  = ""
        keyChain["refreshToken"] = ""
    }
    
    
    static func save()
    {
        keyChain["Username"] = Username
        keyChain["UserMail"] = UserMail
        keyChain["Password"] = password
        keyChain["Phone"]    = Phone
        keyChain["CutID"]    = CutID
        keyChain["address"]  = address
        keyChain["avatar"]   = avatar
        keyChain["accessToken"]  = accessToken
        keyChain["refreshToken"] = refreshToken
    }
    static func start()
    {
        if let Phone =  keyChain["Phone"] {
            Session.Phone = Phone
        }
        if let Email =  keyChain["UserMail"] {
            print("\(Email)")
            Session.UserMail = Email
        }
        if let Username =  keyChain["Username"] {
            Session.Username = Username
        }
        if let CutID = keyChain["CutID"] {
            Session.CutID = CutID
        }
        if let address = keyChain["address"] {
            Session.address = address
        }
        if let avatar      = keyChain["avatar"] {
            Session.avatar = avatar
        }
        if let password      = keyChain["Password"]{
            Session.password = password
        }
        if let accessToken      = keyChain["accessToken"] {
            Session.accessToken = accessToken
        }
        if let refreshToken      = keyChain["refreshToken"] {
            Session.refreshToken = refreshToken
        }
        
    }
    
    static func resetSelectedCountry ()
    {
        defaults.setValue("", forKey: "SelectedCountry")
        defaults.synchronize ()
    }
    static func setSelectedCountry (){
        defaults.setValue(SelectedCountry, forKey: "SelectedCountry")
        defaults.synchronize()
    }
    
    static func getSelectedCountry(){
        Session.SelectedCountry = defaults.integer(forKey: "SelectedCountry")
    }
    
    static func setLang()
    {
        defaults.setValue (Session.Language, forKey: "Language")
        defaults.synchronize()
    }
    
    static func getLang(){
        if let lang = defaults.string(forKey: "Language") {
            Session.Language = lang
            if (Session.Language.isEmpty) {
                Session.Language = "en"
            }
        }else{
            Session.Language = "en"
        }
      
    }
    
}
