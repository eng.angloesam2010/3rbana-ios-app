//
//  AppTheme.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/3/20.
//

import Foundation
import UIKit

class AppTheme{
    class var secondaryColor : UIColor {
        return UIColor(named: "tabbar_color")!
        
    }
    class var blueColor : UIColor {
        return UIColor(named: "indicator_color")!
        
    }

    static func FontArabic(fontSize :CGFloat) -> UIFont{
        return UIFont(name: "tajawal-bold", size: fontSize)!
        
    }
    static func  setGoldenColor() -> UIColor{
        return UIColor(named: "goldenColor")!
    }
    
    static func  setBlackColor() -> UIColor{
        return UIColor.black
    }
    
    static func setHeaderFonts(fontSize :CGFloat) -> UIFont{
        return UIFont(name: "Cennerik-Bold", size: fontSize)!
    }
    
    static func FontExtraBold(fontSize :CGFloat) -> UIFont{
        return UIFont(name: "Gilroy-ExtraBold", size: fontSize)!
        
    }
    
}
