//
//  AuctionChatCell.swift
//  3rbana
//
//  Created by Bilal on 27/04/2021.
//

import UIKit

class AuctionChatCell: UITableViewCell {
    
    
    
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    static let identifier = "AuctionChatCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
