//
//  PostListVC.swift
//  3rbana
//
//  Created by Mac on 18/04/2021.
//

import UIKit
import AnimatableReload
import EmptyKit

class PostListVC: UIViewController,EmptyDataSourceProtocal{
    var typeInt: Int = 0
    @IBOutlet weak var postTableView: UITableView!
    lazy var postListViewModel : PostsViewModel = {
        return PostsViewModel()
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
        startFetchPost()
        NotificationCenter.default.addObserver(self, selector: #selector(startFetchObserver),
                                               name: Notification.Name("startFetchObserver"), object: nil)
    }
    
    
    @objc func startFetchObserver(notification: NSNotification){
        if let governmentId = notification.userInfo?["goverSelectedIndex"] as? Int {
            self.postListViewModel.governoratesId = "\(governmentId)"
            startFetchPost()
        }
    }
    
    
    
}
extension PostListVC {
    // MARK: - Fetch Level Part
    func startFetchPost(){
        // MARK: - when colusre was updaete alert will be shown
        
        self.postListViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.postListViewModel.state {
                case .error:
                    self.typeInt = 2
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                case .empty :
                    self.typeInt = 4
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    DispatchQueue.main.async {
                        self.reloadTableView()
                    }
                case .noconnect:
                    self.typeInt = 1
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                }
            }
        }
        
        self.postListViewModel.fetchPosts()
        
    }
    // MARK: - Reload Collectionview
    func reloadTableView() {
        self.postTableView?.dataSource = self.postListViewModel
        self.postTableView?.delegate   = self.postListViewModel
        self.postTableView?.reloadData()
        self.startAnimation()
        
    }
    
    // MARK: - Set datasource for collectionview
    func setEmptyDataSet(){
        self.postTableView?.dataSource = nil
        self.postTableView?.delegate   = nil
        self.postTableView?.reloadData()
        self.postTableView.ept.dataSource = self as EmptyDataSource
        self.postTableView.ept.delegate   = self as EmptyDelegate
        self.postTableView.ept.reloadData()
        self.postTableView.alwaysBounceVertical = false
        self.postTableView.isScrollEnabled = false
        self.postTableView.isPagingEnabled = false
        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
        
    }
    
    // MARK: - Start Animation for view controller
    func startAnimation(){
        AnimatableReload.reload(tableView:self.postTableView, animationDirection: "left")
    }
    
    // MARK: - Back button tom return in back screen
    func popViewController() {
        dismiss(animated: true, completion: nil)
    }
    
    
    func didSelectCell(countryID: Int) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let tbc   = storyBoard.instantiateViewController(withIdentifier:"tabbar") as! UITabBarController
        tbc.selectedIndex = 0
        tbc.modalPresentationStyle = .fullScreen
        self.present(tbc, animated: true, completion:nil)
        
    }
    
    
    @objc(emptyButton:tappedIn:) func emptyButton(_ button: UIButton, tappedIn view: UIView) {
        self.startFetchPost()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
}
