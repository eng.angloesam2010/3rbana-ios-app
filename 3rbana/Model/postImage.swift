//
//  postImage.swift
//  3rbana
//
//  Created by Mac on 22/04/2021.
//

import Foundation
struct postImage : Codable {
    let thumbnailAvatarPath : String?
    
    enum CodingKeys: String, CodingKey {
        case thumbnailAvatarPath = "thumbnailAvatarPath"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        thumbnailAvatarPath = try values.decodeIfPresent(String.self, forKey: .thumbnailAvatarPath)
        
    }
    
}
