//
//  UpdateBody.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on May 6, 2021

import Foundation

struct UpdateBody : Codable {
    
    let address : String?
    let countryId : String?
    let createdDate : String?
    let email : String?
    let isActive : Bool?
    let isDeleted : Bool?
    let isLoggedIn : Bool?
    let lastLogged : String?
    let modifiedDate : String?
    let name : String?
    let password : String?
    let phone : String?
    let userId : Int?
    let username : String?
    let uuid : String?
    let avatar : Avatar?
    
    enum CodingKeys: String, CodingKey {
        case address = "address"
        case countryId = "countryId"
        case createdDate = "createdDate"
        case email = "email"
        case isActive = "isActive"
        case isDeleted = "isDeleted"
        case isLoggedIn = "isLoggedIn"
        case lastLogged = "lastLogged"
        case modifiedDate = "modifiedDate"
        case name = "name"
        case password = "password"
        case phone = "phone"
        case userId = "userId"
        case username = "username"
        case uuid = "uuid"
        case avatar = "avatar"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        address = try values.decodeIfPresent(String.self, forKey: .address)
        countryId = try values.decodeIfPresent(String.self, forKey: .countryId)
        createdDate = try values.decodeIfPresent(String.self, forKey: .createdDate)
        email = try values.decodeIfPresent(String.self, forKey: .email)
        isActive = try values.decodeIfPresent(Bool.self, forKey: .isActive)
        isDeleted = try values.decodeIfPresent(Bool.self, forKey: .isDeleted)
        isLoggedIn = try values.decodeIfPresent(Bool.self, forKey: .isLoggedIn)
        lastLogged = try values.decodeIfPresent(String.self, forKey: .lastLogged)
        modifiedDate = try values.decodeIfPresent(String.self, forKey: .modifiedDate)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        password = try values.decodeIfPresent(String.self, forKey: .password)
        phone = try values.decodeIfPresent(String.self, forKey: .phone)
        userId = try values.decodeIfPresent(Int.self, forKey: .userId)
        username = try values.decodeIfPresent(String.self, forKey: .username)
        uuid = try values.decodeIfPresent(String.self, forKey: .uuid)
        if let avatar = try? values.decode(Avatar.self, forKey: .avatar){
            self.avatar = avatar
        }else{
            self.avatar = nil
        }
    }
    
}
