//
//  CategoryCell.swift
//  3rbana
//
//  Created by Mac on 31/01/2021.
//

import UIKit
import AnimatableReload
import EmptyKit
//Write the protocol declaration here:
protocol ReloadCellDelegate {
    func reloadCell()
}

//protocal for selected Subcateory
protocol CategoryProtocal {
    func didSelectIndex(id:Int,categoryID:Int,flag:Int)
}


class CategoryTableViewCell : UITableViewCell,EmptyDataSourceProtocal{

    // MARK: - value for status laoding model
    var typeInt: Int = 0
    // MARK: - Model for Loading Category
    lazy var categoryViewModel: CategoryViewModel = {
        return CategoryViewModel()
    }()
    var delegate : ReloadCellDelegate?
    var flag : Int = 0
    
    var delegateCategory : CategoryProtocal?
 
    @IBOutlet weak var categoryCollectionViewHeight: NSLayoutConstraint!
    
    var cellWidth  : CGFloat             =  Util.screenWidth()
    var cellHeight : CGFloat             = 128
    var numberOfItemsInSection : CGFloat = 3
    var spacing   : CGFloat              = 1.0
    @IBOutlet weak var categoryCollectionView : UICollectionView!
    var identifier = "CategoryTableViewCell"
    static var identifier: String {
        return String(describing: self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        initCollectionView()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initCollectionView(){
        // Do any additional setup after loading the view, typically from a nib.
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: cellWidth / 3, height: 128.0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.categoryCollectionView!.collectionViewLayout = layout
    }
    
    func reloadCollectionView(){
        self.categoryCollectionView?.dataSource   = self
        self.categoryCollectionView?.delegate     = self
        self.categoryCollectionView?.reloadData()
        self.categoryCollectionView?.layoutIfNeeded()
        self.categoryCollectionView?.setNeedsFocusUpdate()
  
        self.categoryCollectionViewHeight.constant = self.categoryCollectionView.collectionViewLayout.collectionViewContentSize.height
        delegate?.reloadCell()
        self.categoryCollectionView?.layoutIfNeeded()
        self.layoutIfNeeded()
        self.setNeedsFocusUpdate()
    }
    
    // MARK: - Fetch Level Part
    func startFetchCategory(){
        // MARK: - when colusre was updaete alert will be shown
        if let flagPointer = "\(flag)" as String? {
//            print("\(flagPointer)")
            self.categoryViewModel.rootID = flagPointer
        }
    
        self.categoryViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.categoryViewModel.state {
                case .empty, .error:
                    self.typeInt = 2
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    self.reloadCollectionView()
                case .noconnect:
                    self.typeInt = 1
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                }
            }
        }
  
        self.categoryViewModel.fetcCategory()
        
    }
    
    // MARK: - Set datasource for collectionview
    func setEmptyDataSet(){
        self.categoryCollectionView.ept.dataSource = self as EmptyDataSource
        self.categoryCollectionView.ept.delegate   = self as EmptyDelegate
        self.categoryCollectionView.ept.reloadData()
        self.categoryCollectionView.alwaysBounceVertical = false
        self.categoryCollectionView.isScrollEnabled = false
        self.categoryCollectionView.isPagingEnabled = false
        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
    }
    
    // MARK: - Start Animation for view controller
    func startAnimation(){
        AnimatableReload.reload(collectionView:self.categoryCollectionView, animationDirection: "top")
    }
    


    
    

}
