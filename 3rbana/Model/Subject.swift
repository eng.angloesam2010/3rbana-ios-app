//
//  Subjectes.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 14, 2020
//
import Foundation

struct Subject : Codable {

    let Stage: String?
    let SubjectName: String?
    let SubjectImage: String?
    let IslandImage: String?
    let IslandNameAR: String?
    let IslandNameEN: String?
    let SubjectID : Int
    let StageID: Int
    let Isdeleted: Bool

    private enum CodingKeys: String, CodingKey {
        case Stage = "Stage"
        case SubjectName = "SubjectName"
        case SubjectImage = "SubjectImage"
        case IslandImage = "IslandImage"
        case IslandNameAR = "IslandNameAR"
        case IslandNameEN = "IslandNameEN"
        case StageID    = "StageID"
        case SubjectID    = "SubjectID"
        case Isdeleted = "Isdeleted"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        Stage = try values.decodeIfPresent(String.self, forKey: .Stage)
        SubjectName = try values.decodeIfPresent(String.self, forKey: .SubjectName)
        if let subjectImage = try values.decodeIfPresent(String.self, forKey: .SubjectImage) {
            SubjectImage = subjectImage
                }else {
                    SubjectImage = ""
                }
        if let subjectImage = try values.decodeIfPresent(String.self, forKey: .IslandImage) {
            IslandImage = subjectImage
                }else {
                    IslandImage = ""
                }
      
      
        IslandNameAR = try values.decodeIfPresent(String.self, forKey: .IslandNameAR)
        IslandNameEN = try values.decodeIfPresent(String.self, forKey: .IslandNameEN)
        StageID = try values.decode(Int.self, forKey: .StageID)
        SubjectID = try values.decode(Int.self, forKey: .SubjectID)
        Isdeleted = try values.decode(Bool.self, forKey: .Isdeleted)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(Stage, forKey: .Stage)
        try container.encode(SubjectName, forKey: .SubjectName)
        try container.encode(SubjectImage, forKey: .SubjectImage)
        try container.encode(IslandImage, forKey: .IslandImage)
        try container.encode(IslandNameAR, forKey: .IslandNameAR)
        try container.encode(IslandNameEN, forKey: .IslandNameEN)
        try container.encode(StageID, forKey: .StageID)
        try container.encode(SubjectID, forKey: .SubjectID)
        try container.encode(Isdeleted, forKey: .Isdeleted)
    }

}
