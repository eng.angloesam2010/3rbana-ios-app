import Foundation
import UIKit
import ZKProgressHUD

protocol ZVProgressHUDProtocal {
    var progressTitle     : String { get set }
    var font : UIFont { get set }
    func showGProgressHUD(timeFor delayTime : Double)
    func hideGProgressHUD()
    
}

class ProgressHUDViewModel : NSObject, ZVProgressHUDProtocal {
    var progressTitle: String
    var font : UIFont
    static let sharedInstance = ProgressHUDViewModel()
    private override init(){
        self.font              =  AppTheme.FontArabic(fontSize: 12)
        self.progressTitle     =  Progress
        super.init()
        
    }
    
    func showGProgressHUD(timeFor delayTime : Double = 10 ) {
        ZKProgressHUD.setCornerRadius(5.0)
        ZKProgressHUD.dismiss(10)
        ZKProgressHUD.setBackgroundColor(UIColor(named: "goldenColor")!)
        ZKProgressHUD.setForegroundColor(.black)
        ZKProgressHUD.show()
    }
    
    func hideGProgressHUD() {
        ZKProgressHUD.dismiss()
    }

    
}
