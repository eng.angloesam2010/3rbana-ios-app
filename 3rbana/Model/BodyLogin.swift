//
//  Body.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on March 30, 2021
//
import Foundation

struct BodyLogin: Codable {

	let accessToken: String
	let refreshToken: String
	let user: User


	private enum CodingKeys: String, CodingKey {
		case accessToken = "accessToken"
		case refreshToken = "refreshToken"
		case user = "user"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		accessToken = try values.decode(String.self, forKey: .accessToken)
		refreshToken = try values.decode(String.self, forKey: .refreshToken)
		user = try values.decode(User.self, forKey: .user)
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(accessToken, forKey: .accessToken)
		try container.encode(refreshToken, forKey: .refreshToken)
		try container.encode(user, forKey: .user)
	}

}
