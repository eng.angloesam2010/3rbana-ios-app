//
//  UIViewController+Extenstion+ALert.swift
//  بري و بحري
//
//  Created by Eng Angelo E. Saber  on 3/22/20.
//  Copyright © 2020 MacBook. All rights reserved.
//

import Foundation
import UIKit
import SwiftMessages


// MARK: - protocal for create show alert method
protocol AlertProtocal {
    func showAlert(title :String ,body:String, titleButton:String , idConfigureTheme :Int ) -> Void
}

// MARK: - Enumeration for create alert depend on case
enum AlertActionManager : AlertProtocal {
    
    case noInternet
    case Error
    case succes
    
    func getAlert(errorDescribe:String = ""){
        switch self {
        case .noInternet :
            let titleStr = INTERNET_AR_TITLE
            let bodyStr  =  INTERNET_AR_MESSAGE
            let titleButton = CANCEL_AR_STR
            self.showAlert(title: titleStr , body: bodyStr, titleButton: titleButton, idConfigureTheme: 1)
            
        case .Error :
            let titleStr = TITLE_AR
            let bodyStr  = errorDescribe
            let titleButton =  CANCEL_AR_STR
            self.showAlert(title: titleStr , body: bodyStr, titleButton: titleButton, idConfigureTheme: 1)
        case .succes:
            
            let titleStr = TITLE_AR
            let bodyStr  = errorDescribe
            let titleButton =  CANCEL_AR_STR
            self.showAlert(title: titleStr , body: bodyStr, titleButton: titleButton, idConfigureTheme: 4)
            
        }
    }
    
    func showAlert(title: String, body: String, titleButton: String, idConfigureTheme: Int) {
        let messageView = MessageView.viewFromNib(layout: .cardView)
        messageView.configureDropShadow()
        switch idConfigureTheme {
        case 1:
            var infoConfig = SwiftMessages.defaultConfig
            messageView.button?.isHidden = true
            infoConfig.presentationStyle = .center
            messageView.configureTheme(.error)
        case 2:
            messageView.configureTheme(.warning)
            var infoConfig = SwiftMessages.defaultConfig
            infoConfig.presentationStyle = .center
        case 3 :
            messageView.configureTheme(.error)
            messageView.button?.isHidden = true
            var infoConfig = SwiftMessages.defaultConfig
            infoConfig.presentationStyle = .center
            
        case 4 :
            messageView.configureTheme(.success)
            messageView.button?.isHidden = true
            var infoConfig = SwiftMessages.defaultConfig
            infoConfig.presentationStyle = .center
        default:
            break
            
        }
        
        messageView.bodyLabel?.font   = AppTheme.FontArabic(fontSize: 15)
        messageView.titleLabel?.font =  AppTheme.FontArabic(fontSize: 12)
        messageView.bodyLabel?.textAlignment = .center
        messageView.titleLabel?.textAlignment = .center
        
        
        messageView.button?.setTitle(titleButton, for: .normal)
        messageView.configureContent(title: title, body: body)
        SwiftMessages.show(view: messageView)
    }
    
}
