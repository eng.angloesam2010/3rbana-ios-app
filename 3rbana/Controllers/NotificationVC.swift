//
//  NotificationVC.swift
//  3rbana
//
//  Created by Mac on 30/03/2021.
//

import UIKit

class NotificationVC : UIViewController,BackNavigationDelegate {
    
    @IBOutlet weak var backNavigationBar: BackNavigationBar!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backNavigationBar?.delegateProfile = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func backViewController() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func notificationViewController() {
        self.pushViewController(id: 6)
    }
}
