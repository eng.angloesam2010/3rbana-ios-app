//
//  PostsViewModel.swift
//  3rbana
//
//  Created by Mac on 19/04/2021.
//

import Foundation
import UIKit
import Alamofire




public struct AdvertiseStru{

    var avatarName    : String?

}

// MARK: - Class for Model View
final class AdvertViewModel : NSObject {
    var widthPostCollectV  : CGFloat {
        get{
            return  ( Util.screenWidth() / 2.0 - 9.0 )
        }
    }
    
    var heightPostCollectV  : CGFloat {
        get{
            return  250.0
        }
    }
    
    var postsArray          = [AdvertiseStru]()
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var delegate : CountryTableViewDelegate?
    var alertMessage : String? = ""  {
        didSet {
            self.showAlertClosure?()
        }
    }
    
    var governoratesId : String? = "0"
    
    var Url: String = ""
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    // MARK: - Fetch Data
    public func fetchPosts() {
        
        let params  = [ : ] as NSDictionary
        let headers: HTTPHeaders = [:]
        self.state  = .loading
        
        // MARK: - Request for api
        var endPoint  = EndPoint.fetchAdvertiseUrl(governoratesId: "1", limit: "1", offset:  "1", count:  "1")
        if self.governoratesId != "0" {
            endPoint =  EndPoint.fetchAdvertiseUrl(governoratesId: "1", limit: "1", offset:  "1", count:  "1")
        }
        self.Url     = endPoint.url
        print("\(self.Url)")
        self.apiService.fetchGenericData(url: Url , withParameters: params, httpMethod: .get, withHeader: headers ) {
            (response: ResultApi<Advertise, APIServiceError>) in
            switch response {
            case .failure(let error):
                if error.localizedDescription == "unKnown" {
                    self.state        = .noconnect
                    self.alertMessage = (Session.Language == "en") ? INTERNET_ENG_MESSAGE : INTERNET_AR_MESSAGE
                }else{
                    self.state        = .error
                    self.alertMessage = error.localizedDescription
                }
                
                break
            case .success(let result):
                if let status = result.status {
                    if status {
                        if let body = result.body?.data {
                            if !body.isEmpty {
                                self.state = .populated
                                self.appendGovernmente(body: body)
                            }else{
                                self.state        = .empty
                            }
                        }else{
                            self.state        = .empty
                        }
                    }else{
                        self.state        = .error
                        self.alertMessage = Login_Ar_Msg
                    }
                    
                }else{
                    self.state        = .error
                    self.alertMessage = Login_Ar_Msg
                }
                
                break
            }
        }
    }
    
    private func appendGovernmente(body : [AdvertiseData]?) {
        body?.forEach {
            let post = $0
            print("\($0)")
            if let avatar = post.avatar {
                let pathPth = !(avatar.thumbnailAvatarPath!.isEmpty) ? avatar.thumbnailAvatarPath : ""
                postsArray.append(AdvertiseStru.init(avatarName:pathPth))
            }
    
        }
    }
    
    
    
}
extension AdvertViewModel : UICollectionViewDelegate,UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int{
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return self.postsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:AdvertiseGridCell.identifier, for: indexPath) as! AdvertiseGridCell
        cell.configureCell(imgPath:self.postsArray[indexPath.row].avatarName!)
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: widthPostCollectV, height: heightPostCollectV)
    }
    
}

extension AdvertViewModel : UITableViewDataSource ,UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: AdvertiseListCell.identifier , for: indexPath) as! AdvertiseListCell
        let imgPath     = self.postsArray[indexPath.row].avatarName      as String?
        cell.configureCell(imgPath:imgPath!)
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.postsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 275.0
    }
    
}




