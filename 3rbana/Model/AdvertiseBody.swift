//
//  AdvertiseBody.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 28, 2021

import Foundation

struct AdvertiseBody : Codable {
    let data : [AdvertiseData]?
    let total : Int?
    
    enum CodingKeys: String, CodingKey {
        case data = "data"
        case total = "total"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        data = try values.decodeIfPresent([AdvertiseData].self, forKey: .data)
        total = try values.decodeIfPresent(Int.self, forKey: .total)
    }
    
}
