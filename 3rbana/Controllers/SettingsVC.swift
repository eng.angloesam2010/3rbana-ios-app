//
//  ConatinerProfileVC.swift
//  3rbana
//
//  Created by Mac on 30/03/2021.
//

import UIKit
import PMAlertController
class SettingsVC: UIViewController,ProfileProtocal,NavigationBarDelegate,UpdateProfileDelegate{
    lazy var logoutViewModel : LogoutViewModel = {
        return LogoutViewModel()
    }()
    

    var flag = 0
    @IBOutlet weak var navigationBar: NavigationBar!
    @IBOutlet weak var profileTableView: UITableView!{
        didSet {
            self.profileTableView?.register(UINib(nibName: ProfileCell.identifier, bundle: nil), forCellReuseIdentifier: ProfileCell.identifier)
            self.profileTableView?.register(UINib(nibName: LoginProfileCell.identifier, bundle: nil), forCellReuseIdentifier: LoginProfileCell.identifier)
            self.profileTableView?.register(UINib(nibName: HeaderProfileCell.identifier, bundle: nil), forCellReuseIdentifier: HeaderProfileCell.identifier)
            self.profileTableView?.dataSource = self as! UITableViewDataSource
            self.profileTableView?.delegate = self as! UITableViewDelegate
            self.profileTableView.backgroundColor = .black
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileTableView.delegate = self
        self.profileTableView.dataSource = self
        self.navigationBar.delegate       = self
        self.profileTableView.reloadData()
        
        
        // Notification for logout method call
        NotificationCenter.default.addObserver(self, selector: #selector(logoutUser), name: Notification.Name("logoutUser"), object: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.profileTableView.delegate = self
        self.profileTableView.dataSource = self
        self.profileTableView.reloadData()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.profileTableView.delegate = self
        self.profileTableView.dataSource = self
        self.profileTableView.reloadData()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    
    func navigateRegisterVC() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let registerVC = storyBoard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        registerVC.modalPresentationStyle = .fullScreen
        // self.present(registerVC, animated: true, completion: nil)
        self.navigationController?.pushViewController(registerVC, animated: false)
    }
    
    func navigateLoginVC() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        loginVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(loginVC, animated: false)
    }
    
    func popViewController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let notificationVC = storyBoard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        notificationVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(notificationVC, animated: false)
    }
    
    func goUpdateProfileVC() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let updateProfileVC = storyBoard.instantiateViewController(withIdentifier: "UpdateProfileVC") as! UpdateProfileVC
        updateProfileVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(updateProfileVC, animated: false)
    }
    @objc func logoutUser (notification: NSNotification){
        self.pushViewController(id: 0)
        Session.reset()
        // MARK: - when colusre was updaete alert will be shown
        self.logoutViewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.logoutViewModel.alertMessage {
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    let alert = AlertActionManager.Error
                    alert.getAlert(errorDescribe: message)
                }
            }
        }
        
        self.logoutViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.logoutViewModel.state {
                case .empty, .error, .noconnect:

                        if let message = self.logoutViewModel.alertMessage {
                            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                            let alert = AlertActionManager.Error
                            alert.getAlert(errorDescribe: message)
                        }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    
                }
            }
        }
        self.logoutViewModel.logoutUser()
    }

}

