//
//  UpdateProfileVC+UIImagePickerControllerDelegate.swift
//  3rbana
//
//  Created by Mac on 07/04/2021.
//

import Foundation
import UIKit
import Alamofire

extension UpdateProfileVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate,BackNavigationDelegate{
    func openGalleryAndCamera(){
        let alertController = UIAlertController(title: "Default Style", message: "A standard alert.", preferredStyle:.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.openCamera()
        }
        alertController.addAction(cameraAction)
        
        let galleryAction = UIAlertAction(title: "Gallery", style: .default) { (action) in
            self.openPhotoLibraryButton()
        }
        
        alertController.addAction(galleryAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            //            self.openPhotoLibraryButton()
        }
        alertController.addAction(cancelAction)
        
        self.present(alertController, animated: true) {
        }
    }
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera;
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func openPhotoLibraryButton() {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary;
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[.originalImage] as? UIImage else {
            fatalError("Expected a dictionary containing an image, but was provided the following: \(info)")
        }
        
        self.profileImge.image = image.resized(withPercentage: 50.0)
        self.updateViewModel.imageRequest = ImageRequest(image: image.resized(withPercentage: 50.0), fileName: "\(UUID().uuidString).jpg")
    }
    
    func backViewController() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func notificationViewController() {
        self.pushViewController(id: 6)
    }
    
}


