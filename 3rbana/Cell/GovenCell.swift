//
//  GovenCell.swift
//  3rbana
//
//  Created by Mac on 18/04/2021.
//

import UIKit

class GovenCell: UICollectionViewCell {
    @IBOutlet weak var goverNameLbl: UILabel!{
        didSet {
            self.goverNameLbl.font = (Session.Language == "en") ?  AppTheme.FontArabic(fontSize: 20.0) : AppTheme.FontArabic(fontSize: 20.0)
            self.goverNameLbl.textAlignment = .center
        }
    }
    
    var identifier = "GovernCell"
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCellConfigure(governStr : String)  {
        if let governStr = governStr as String?  {
            self.goverNameLbl.text = "\(governStr)"
        }
        
    }
    
}
