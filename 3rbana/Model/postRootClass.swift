//
//  postRootClass.swift
//  3rbana
//
//  Created by Mac on 19/04/2021.
//

import Foundation

struct postRootClass : Codable {
    let body : [postBody]?
    let message : String?
    let status : Bool?
    
    enum CodingKeys: String, CodingKey {
        case body = "body"
        case message = "message"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        body = try values.decodeIfPresent([postBody].self, forKey: .body)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
    }
    
}

