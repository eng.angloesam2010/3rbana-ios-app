//
//  UpdateViewModel.swift
//  3rbana
//
//  Created by Mac on 08/04/2021.
//

import Foundation
import UIKit
import Alamofire


// MARK: - Company Protocal
internal protocol LogoutProtocal {
    var  accessToken                 : String?                 { get set}
    var  apiService                  : APIServiceType          { get }
    var  state                       : State                   { get set }
    var  updateLoadingStatus         : (()->())?               { get }
    var  showAlertClosure            : (()->())?               { get }
    var  alertMessage                : String?                 { get set }
    mutating func resetSession()
    mutating func logoutUser()
}

// MARK: - Class for Model View
final class LogoutViewModel : NSObject, LogoutProtocal {
    var validation          : Validation?{
        return Validation()
    }
    var accessToken: String? {
        didSet{
            if let accessToken  =  self.registerModel?.refreshToken {
//                print("\(accessToken)")
                self.accessToken = accessToken
            }
        }
    }
    var registerModel       : RegisterModel?
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var alertMessage : String? = "" {
        didSet {
            self.showAlertClosure?()
        }
    }
    var Url : String!
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    // MARK: - Fetch Data
    func resetSession() {
        Session.start()
        Session.reset()
    
    }
    
    public func logoutUser() {
        Session.start()
        self.state  = .loading
        print("\(Session.refreshToken)")
        let endPoint = EndPoint.logoutUrl(token: Session.refreshToken)
        self.Url     = endPoint.url
        print("\(self.Url!)")
        self.apiService.fetchGenericToken(url: self.Url!, access_token:Session.refreshToken){
            [self] (response: ResultApi<Logout, APIServiceError>) in
            switch response {
            case .failure(let error):
                if error.localizedDescription == "unKnown" {
                    self.state        = .noconnect
                    self.alertMessage = (Session.Language == "en") ? INTERNET_ENG_MESSAGE : INTERNET_AR_MESSAGE
                }else{
                    self.state        = .error
                    self.alertMessage = ServerIssuesMsg
                }
                break
                
            case .success(let _):
                self.state = .populated
                self.resetSession()
                self.state = .populated
                self.alertMessage     = (Session.Language == "en") ? Logout_User_En : Logout_User_Ar
                break
            }
            
            
        }
    }
}

