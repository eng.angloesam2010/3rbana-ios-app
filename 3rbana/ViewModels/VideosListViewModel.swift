//
//  SubjectsListViewModel.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/29/20.
//
import Foundation
import UIKit
import Alamofire
// MARK: - Model View for Cell
public struct VideoCellViewModel {
    let VideoPathUrl         : String
    let VideoTitle           : String
    let VideoDescription     : String
    let SubjectTitle         : String
    let VideoImagePathUrl    : String
}

// MARK: - Company Protocal
internal protocol SubjectsListProtocal  {
    var  reloadTableViewClosure      : (()->())?               { get }
    var  emptyTableViewClosure       : (()->())?               { get }
    var  showAlertClosure            : (()->())?               { get }
    var  updateLoadingStatus         : (()->())?               { get }
    var  videosArray                 : Array<VideoCellViewModel>  { get set }
    var  numberOfCells               : Int                     { get }
    var  apiService                  : APIServiceType          { get }
    var  state                       : State                   { get set }
    var  alertMessage                : String                  { get set }
    var  subjectID                     : String                  { get set }
    mutating func startFetchSubjectList()
}

// MARK: - Delegate for click cell
protocol VideoTableViewDelegate {
    func didSelectCell(titleVideo:String,urlVideo :String)
}
// MARK: - Class for Model View
final class VideosListViewModel : NSObject,SubjectsListProtocal {
    
    // MARK: - define the properties of model
    var numberOfCells: Int {
        return videosArray.count
    }
    var delegate : VideoTableViewDelegate?
    let SubjectCellHeight : CGFloat = 130.0
    var reloadTableViewClosure: (() -> ())?
    var emptyTableViewClosure: (() -> ())?
    var showAlertClosure: (() -> ())?
    var updateLoadingStatus: (() -> ())?
    var videosArray: Array<VideoCellViewModel> = Array<VideoCellViewModel>() {
        didSet {
            if videosArray.count > 0 {
                self.reloadTableViewClosure?()
            }else{
                self.emptyTableViewClosure?()
            }
        }
    }
    var subjectID : String = ""
    var alertMessage: String = ""  {
        didSet {
            self.showAlertClosure?()
        }
    }
    var apiService: APIServiceType
    var state : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    
    var Url : String {
        get {

            let endPoint = EndPoint.videoListUrl(subjectID: subjectID)
            return endPoint.url
        }
    }
    
    
        // MARK: - initialization of class Level Model View
        init(apiService: APIServiceType = APIServiceRequest() ) {
            self.apiService      = apiService
        }
        // MARK: - Fetch Data
    func startFetchSubjectList() {
        let params = [ : ] as NSDictionary
        let headers: HTTPHeaders = [:]
        self.state = .loading
        // MARK: - Request for api
        // print("\(Url)")
        self.apiService.fetchGenericData(url: Url , withParameters: params, httpMethod: .get, withHeader: headers ) {
            (response: ResultApi<VideoRoot, APIServiceError>) in
            switch response {
            case .failure(let error):
                if error.localizedDescription == "unKnown" {
                    self.state        = .noconnect
                    self.alertMessage = INTERNET_AR_MESSAGE
                }else{
                    self.state        = .error
                    self.alertMessage = error.localizedDescription
                }
                break
            case .success(let result):
                if result.status! {
                    if (result.videos?.isEmpty != nil){
                        self.state = .populated
                        self.appendVideoModelArray(videosModel: result.videos!)
                    }else{
                        self.emptyTableViewClosure?()
                        self.state         = .empty
                        self.alertMessage  = NoVideoDescibAr
                    }
                 
                      
                    }else{
                        self.emptyTableViewClosure?()
                        self.state         = .empty
                        self.alertMessage  = NoVideoDescibAr
                    }


                break

            }
        }
    }
    // MARK: - append request data to cell model
    private func appendVideoModelArray(videosModel : [VideosModel]) {
        for videos in videosModel {
            self.videosArray.append(VideoCellViewModel.init(VideoPathUrl: videos.lessonURL!, VideoTitle: videos.DescriptionAR!, VideoDescription: videos.islandNameAR!, SubjectTitle: videos.subjectName!, VideoImagePathUrl: videos.lessonURL!))
        }

    }
    

    
}
// MARK: - extension for TableView to manage datasource and delegate
//
extension VideosListViewModel : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  self.videosArray.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell        = tableView.dequeueReusableCell(withIdentifier:VideosCell.identifier, for: indexPath) as! VideosCell
        let videoModel = self.videosArray[indexPath.row]
        cell.configureCell(model: videoModel)
        cell.selectionStyle = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
      return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SubjectCellHeight
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let videoModel = self.videosArray[indexPath.row]
        self.delegate?.didSelectCell(titleVideo: videoModel.VideoTitle, urlVideo: videoModel.VideoPathUrl)
    }

}


