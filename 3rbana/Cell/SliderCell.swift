//
//  SliderCell.swift
//  3rbana
//
//  Created by Mac on 31/01/2021.
//

import UIKit
import FSPagerView
protocol StoryProtocal{
    func navigateStoryScreen()
}

protocol collectionViewDelegate {
    func selectedCell(index : Int,flag :Int)
}

class SliderCell : UITableViewCell, FSPagerViewDataSource,FSPagerViewDelegate {
    var delegate : StoryProtocal?
    @IBOutlet weak var sliderView: FSPagerView! {
        didSet {

            self.sliderView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
                       self.sliderView.transformer = FSPagerViewTransformer(type: .linear)
                       //self.sliderView.itemSize = CGSize(width: 300, height: 150)
                       self.sliderView.isInfinite = true
                       self.sliderView.contentMode = .scaleAspectFit
            self.sliderView.backgroundColor = .black
                       self.sliderView.automaticSlidingInterval = 3.0
        }
    }
    
    lazy var sliderViewModel : SliderViewModel = {
        return SliderViewModel()
    }()
    
    @IBOutlet weak var pageControl: UIPageControl!
    var identifier = "SliderCell"
    static var identifier: String {
        return String(describing: self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
//        fetchSlider()
        
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func fetchSlider(){
        // MARK: - when colusre was updaete alert will be shown
        
        self.sliderViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch
                    self.sliderViewModel.state {
                case .loading: break
                //ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    //ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    DispatchQueue.main.async {
                        self.pageControl.hidesForSinglePage = true
                        self.sliderView.dataSource = self
                        self.sliderView.delegate   = self
                        self.sliderView.reloadData()
                        
                    }
                    break
                    
                case .empty:
                    break
                }
                
            }
        }
        
        self.sliderViewModel.fetcSlider()
        
    }
    

    @IBAction func addNewStory(_ sender: Any) {
        delegate?.navigateStoryScreen()
    }
    
    
    
}


