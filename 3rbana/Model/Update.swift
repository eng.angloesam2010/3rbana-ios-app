





import Foundation

struct Update : Codable {
    
    let body : UpdateBody?
    let message : String?
    let status : Bool?
    
    enum CodingKeys: String, CodingKey {
        case body = "body"
        case message = "message"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        body = try values.decode(UpdateBody.self, forKey: .body)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
    }
    
}
