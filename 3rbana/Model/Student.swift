//
//  Student.swift
//
//  Generated using https://jsonmaster.github.io
//  Created on December 14, 2020
//
import Foundation

struct Student: Codable {

    let CivilID: Int
    let School: String
    let DisplayName: String
    let email: String
    let sex: String
    let moeusersex: String
    let postofficebox: String
    let department: String
    let POBox: Int
    let classname: String

    private enum CodingKeys: String, CodingKey {
  
        case CivilID = "CivilID"
        case School = "School"
        case DisplayName = "DisplayName"
        case email = "email"
        case moeusersex = "moeusersex"
        case sex = "sex"
        case postofficebox = "postofficebox"
        case department = "department"
        case POBox = "POBox"
        case classname = "classname"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        CivilID = try values.decode(Int.self, forKey: .CivilID)
        School = try values.decode(String.self, forKey: .School)
        DisplayName = try values.decode(String.self, forKey: .DisplayName)
        email = try values.decode(String.self, forKey: .email)
        moeusersex = try values.decode(String.self, forKey: .moeusersex)
        sex     = try values.decode(String.self, forKey: .sex)
        postofficebox = try values.decode(String.self, forKey: .postofficebox)
        department = try values.decode(String.self, forKey: .department)
        POBox = try values.decode(Int.self, forKey: .POBox)
        classname = try values.decode(String.self, forKey: .classname)
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(CivilID, forKey: .CivilID)
        try container.encode(School, forKey: .School)
        try container.encode(DisplayName, forKey: .DisplayName)
        try container.encode(email, forKey: .email)
        try container.encode(moeusersex, forKey: .moeusersex)
       try container.encode(sex, forKey: .sex)
        try container.encode(postofficebox, forKey: .postofficebox)
        try container.encode(department, forKey: .department)
        try container.encode(POBox, forKey: .POBox)
        try container.encode(classname, forKey: .classname)
    }

}
