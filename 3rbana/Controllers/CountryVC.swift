//
//  CountryVC.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 12/7/20.
//

import Foundation
import UIKit
import AnimatableReload
import EmptyKit
class CountryVC :  UIViewController , EmptyDataSourceProtocal{
    @IBOutlet weak var countryCollectionView: UICollectionView!
    // MARK: - Property of view controller
    @IBOutlet weak var countryLbl: UILabel! {
        didSet {
            self.countryLbl.text = (Session.Language == "en" ) ? selectCountryEn :  selectCountryEn
        }
    }
    var typeInt: Int = 0
    //var levelCellViewModel : countrysModel?
    lazy var countryListViewModel : CountryViewModel = {
        return CountryViewModel()
    }()
    var titleSubject : String!
    // MARK: - init for Level VC
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        startFetchCountry()
        self.countryListViewModel.delegate = self
    }
    // MARK: - Fetch Level Part
    func startFetchCountry(){
        // MARK: - when colusre was updaete alert will be shown
        self.countryListViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.countryListViewModel.state {
                case .empty, .error:
                    self.typeInt = 2
                    if let message = self.countryListViewModel.alertMessage {
                        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                        let alert = AlertActionManager.Error
                        alert.getAlert(errorDescribe: message)
                    }
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    DispatchQueue.main.async {
                        self.reloadTableView()
                    }
                case .noconnect:
                    self.typeInt = 1
                    DispatchQueue.main.async {
                        self.setEmptyDataSet()
                    }
                }
            }
        }
        self.countryListViewModel.fetchCountry()
    }
    // MARK: - Reload Collectionview
    func reloadTableView() {
        self.countryCollectionView?.dataSource = self.countryListViewModel
        self.countryCollectionView?.delegate   = self.countryListViewModel
        self.countryCollectionView?.reloadData()
        self.startAnimation()
        
    }
    
    // MARK: - Set datasource for collectionview
    func setEmptyDataSet(){
        self.countryCollectionView.ept.dataSource = self as EmptyDataSource
        self.countryCollectionView.ept.delegate   = self as EmptyDelegate
        self.countryCollectionView.ept.reloadData()
        self.countryCollectionView.alwaysBounceVertical = false
        self.countryCollectionView.isScrollEnabled = false
        self.countryCollectionView.isPagingEnabled = false
        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
    }
    
    // MARK: - Start Animation for view controller
    func startAnimation(){
        AnimatableReload.reload(collectionView:self.countryCollectionView, animationDirection: "left")
    }
    
    // MARK: - Back button tom return in back screen
    func popViewController() {
        dismiss(animated: true, completion: nil)
    }
}
extension CountryVC : EmptyDelegate , CountryTableViewDelegate {
    func didSelectCell(countryID: Int) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let tbc   = storyBoard.instantiateViewController(withIdentifier:"tabbar") as! UITabBarController
        tbc.selectedIndex = 1
        tbc.modalPresentationStyle = .fullScreen
        self.present(tbc, animated: true, completion:nil)

    }
    @objc(emptyButton:tappedIn:) func emptyButton(_ button: UIButton, tappedIn view: UIView) {
        self.startFetchCountry()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

}




