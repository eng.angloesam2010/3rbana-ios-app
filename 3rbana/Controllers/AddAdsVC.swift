//
//  AddAdsVC.swift
//  3rbana
//
//  Created by Mac on 09/03/2021.
//

import Foundation
import UIKit
import BSImagePicker
import Photos
class AddAdsVC: UIViewController {
    // MARK: - UIViewController Fields
    @IBOutlet weak var titleLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.titleLbl.text = "addpost".localizableString(loc: "en")
               // self.postTitleLbl.font = AppTheme.FontArabic(fontSize: 18.0)
                
            }else{
                
                self.titleLbl.text = "addpost".localizableString(loc: "ar-EG")
                self.titleLbl.font = AppTheme.FontArabic(fontSize: 18.0)
            }
        }
    }

    @IBOutlet weak var uploadLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.uploadLbl.text = "uploadimagetitle".localizableString(loc: "en")
               // self.postTitleLbl.font = AppTheme.FontArabic(fontSize: 18.0)
                
            }else{
                
                self.uploadLbl.text = "uploadimagetitle".localizableString(loc: "ar-EG")
                self.uploadLbl.font = AppTheme.FontArabic(fontSize: 14.0)
            }
        }
    }
    @IBOutlet weak var navigationBar: NavigationBar!
    // MARK: - Collection View
    @IBOutlet weak var photoCollection: UICollectionView!
    var rowsNumCollectionView = 1
    // MARK: - UIImagePickerController
    var evenAssets = [PHAsset]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initCollectionView()
    }
    
   
}

