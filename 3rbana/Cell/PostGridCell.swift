//
//  PostGridCell.swift
//  3rbana
//
//  Created by Mac on 20/04/2021.
//

import UIKit
import Kingfisher
class PostGridCell : UICollectionViewCell {
    var cellIdentifier = "PostGridCell"
    @IBOutlet weak var priceLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.priceLbl.font = AppTheme.FontArabic(fontSize: 15.0)
                self.priceLbl.textAlignment = .left
                
            }else{
                self.priceLbl.font = AppTheme.FontArabic(fontSize: 15.0)
                self.priceLbl.textAlignment = .right
            }
        }
    }
    @IBOutlet weak var titlePostLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.titlePostLbl.font = AppTheme.FontArabic(fontSize: 28.0)
                self.titlePostLbl.textAlignment = .center
                
            }else{
                self.titlePostLbl.font = AppTheme.FontArabic(fontSize:28.0)
                self.titlePostLbl.textAlignment = .center
            }
        }
    }
    @IBOutlet weak var viewsNumLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.viewsNumLbl.font = AppTheme.FontArabic(fontSize: 20.0)
                self.viewsNumLbl.textAlignment = .left
                
            }else{
                self.viewsNumLbl.font = AppTheme.FontArabic(fontSize: 20.0)
                self.viewsNumLbl.textAlignment = .right
            }
        }
    }
    @IBOutlet weak var teleLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.teleLbl.font = AppTheme.FontArabic(fontSize: 20.0)
                self.teleLbl.textAlignment = .left
                
            }else{
                self.teleLbl.font = AppTheme.FontArabic(fontSize: 20.0)
                self.teleLbl.textAlignment = .right
            }
        }
    }
    @IBOutlet weak var imgPost: UIImageView!
    @IBOutlet weak var ribbonView : UIImageView!{
        didSet {
            self.ribbonView.image = (Session.Language == "en" ) ? UIImage(named: "ribbon-1") : UIImage(named: "ribbon_right")
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        self.imgPost.roundCorners(corners: [.topLeft, .topRight], radius: 20)
    }
    static var identifier: String {
        return String(describing: self)
    }
    
    func configureCell(titlePost:String,pricsStr :String,viewsNumStr:String,teleStr:String,imgPath:String,isSpeaical:Int,dateStr : String){
        self.titlePostLbl.text = "\(titlePost)"
        self.priceLbl.text     = "\(pricsStr)"
        
        if !viewsNumStr.isEmpty {
            self.viewsNumLbl.text  = viewsNumStr
        }else{
            self.viewsNumLbl.text  = "0"
        }
        self.teleLbl.text      = "\(teleStr)"
        if let imagePath = imgPath as String? {
            self.imgPost.contentMode = .scaleToFill
            let endPointUrl = EndPoint.pathImage(pathImage:imagePath)
            self.imgPost?.kf.setImage(with: URL(string:endPointUrl.url),placeholder:UIImage(named: "new_logo"))
        }
        if isSpeaical == 0 {
            self.ribbonView.removeFromSuperview()
        }

    }
}
