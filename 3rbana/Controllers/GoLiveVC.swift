//
//  GoLiveVC.swift
//  3rbana
//
//  Created by Bilal on 26/04/2021.
//

import UIKit
import AVFoundation
import AgoraRtcKit
import Firebase
import Combine




struct BidOrders {
    let id: String
    let bidderId: String
    let bidderImage: String
    let bidderName: String
    let meetingId: String
    let meetingOrganizer: String
    let productId: String
    let productName: String
    let productPrice: Float
}






enum AuctionMessageType: String {
    case simple = "simple"
    case bidding = "bidding"
}

struct Product {
    let id: String
    let name: String
    let price: Float
    let isSold: Bool
}

struct AuctionMessage {
    let id: String
    let messageType: AuctionMessageType
    let senderId: String
    let senderName: String
    let senderImage: String
    let message: String
    let biddingPrice: Float
    var biddingAccepted: Bool
    
    init(id: String = UUID().uuidString,messageType: AuctionMessageType = .simple, senderId: String, senderName: String, senderImage: String, message: String, price: Float = .zero, isBiddingAccepted: Bool = false) {
        self.id = id
        self.messageType = messageType
        self.senderId = senderId
        self.senderName = senderName
        self.senderImage = senderImage
        self.message = message
        self.biddingPrice = price
        self.biddingAccepted = isBiddingAccepted
    }
    
}

class AuctionViewModel {
    private lazy var ref: DatabaseReference = Database.database().reference()
    
    enum State {
        case isLoading
        case loaded(AuctionMessage)
    }
    
    private var cancellables: [AnyCancellable] = []
    @Published private(set) var state: State = .isLoading
    
    func sendMessage(message: AuctionMessage, organizerId: String, meetingId: String) {
        let messageDict: [String: Any] = ["id": message.id,"messageType": message.messageType.rawValue ,"senderId": message.senderId, "senderName": message.senderName, "senderImage": message.senderImage, "message": message.message, "price": message.biddingPrice, "biddingStatus": message.biddingAccepted]
        self.ref.child("live").child(organizerId).child(meetingId).child("chat").child(message.id).updateChildValues(messageDict)
    }
    
    
    
    
    
    
    
    func observeMessages(organizerId: String, meetingId: String, completion: @escaping (AuctionMessage) -> Void) {
        self.ref.child("live").child(organizerId).child(meetingId).child("chat").observe(.childAdded) { (snapshot) in
            if let messageDictionary = snapshot.value as? [String: Any] {
                let messageId = messageDictionary["id"] as? String ?? ""
                let messageType = messageDictionary["messageType"] as? String ?? ""
                let senderId = messageDictionary["senderId"] as? String ?? ""
                let senderName = messageDictionary["senderName"] as? String ?? ""
                let senderImage = messageDictionary["senderImage"] as? String ?? ""
                let message = messageDictionary["message"] as? String ?? ""
                let biddingPrice = messageDictionary["price"] as? Float ?? 0
                let biddingStatus = messageDictionary["biddingStatus"] as? Bool ?? false
                
                completion(AuctionMessage(id: messageId, messageType: AuctionMessageType(rawValue: messageType) ?? .simple ,senderId: senderId, senderName: senderName, senderImage: senderImage, message: message, price: biddingPrice, isBiddingAccepted: biddingStatus))
            }
        }
    }
    
    
    
    
    func exitStreaming(organizerId: String) {
        self.ref.child("users").observeSingleEvent(of: .value) { (snapshot) in
            for child in snapshot.children {
                if let childSnapshot = child as? DataSnapshot {
                    if let childDictionary = childSnapshot.value as? [String: Any] {
                        if let name = childDictionary["name"] as? String {
                            if name == organizerId {
                                childSnapshot.ref.updateChildValues(["isLive":false])
                            }
                        }
                    }
                }
            }
        }
        //self.ref.child("live").child(organizerId).removeValue()
    }
    
    func leaveStreaming(organizerId: String, meetindId: String, userId: String) {
        
        ref.child("live").child(organizerId).child(meetindId).child("usersOnline").child(userId).removeValue()
        
    }
    
    
    func addNewProduct(meetingId: String, product: Product, completion: @escaping (Bool) -> Void) {
        let key = self.ref.child("live").child(Session.Username).child(meetingId).child("products").childByAutoId().key ?? UUID().uuidString
        let productDictionary = ["id": key, "name": product.name, "price": product.price, "isSold": product.isSold] as [String : Any]
        self.ref.child("live").child(Session.Username).child(meetingId).child("products").child(key).updateChildValues(productDictionary) { (error, ref) in
            if error != nil {
                completion(false)
            }else {
                completion(true)
            }
        }
    }
    
    
    func startSteraming(productName: String, productPrice: Float, completion: @escaping (String) -> Void) {
        ref.child("users").observeSingleEvent(of: .value) { (snapshot) in
            for (_, child) in snapshot.children.enumerated() {
                if let childSnapshot = child as? DataSnapshot {
                    if let childDict = childSnapshot.value as? [String: Any] {
                        if let name = childDict["name"] as? String {
                            if name == Session.Username {
                                let id = UUID().uuidString
                                let values = ["id":id,"online":0] as [String : Any]
                                let productId = self.ref.child("live").child(Session.Username).child(id).child("products").childByAutoId().key
                                self.ref.child("live").child(Session.Username).child(id).updateChildValues(values) { (error, ref) in
                                    childSnapshot.ref.updateChildValues(["isLive":true])
                                    childSnapshot.ref.updateChildValues(["currentLive": id])
                                    let products: [String: Any] = ["id":productId ?? UUID().uuidString, "name": productName, "price": productPrice, "isSold": false]
                                    self.ref.child("live").child(Session.Username).child(id).child("products").child(productId ?? UUID().uuidString).updateChildValues(products)
                                    completion(id)
                                }
                                
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    
    func getOnlineUsers(organizerUserName: String, meetingId: String, completion: @escaping (Int) -> Void) {
        ref.child("live").child(organizerUserName).child(meetingId).child("usersOnline").observe(.value) { (snapshot) in
            completion(snapshot.children.allObjects.count)
        }
    }
    
    
    func observeAllProducts(organizerUserName: String, meetingId: String, completion: @escaping (([Product]) -> Void)) {
        var products: [Product] = []
        ref.child("live").child(organizerUserName).child(meetingId).child("products").observe(.value) { (snapshot) in
            products.removeAll()
            for child in snapshot.children {
                if let childSnapshot = child as? DataSnapshot {
                    if let childDict = childSnapshot.value as? [String:Any] {
                        let productId = childDict["id"] as? String ?? ""
                        let productName = childDict["name"] as? String ?? ""
                        let productPrice = childDict["price"] as? Float ?? 0.0
                        let productisSold = childDict["isSold"] as? Bool ?? false
                        products.append(Product(id: productId, name: productName, price: productPrice, isSold: productisSold))
                    }
                }
            }
            
            completion(products)
        }
    }
    
    
    func getBiddingOrders(organizerId: String, meetindId: String, completion: @escaping ([BidOrders]) -> (Void)) {
        var orders: [BidOrders] = []
        ref.child("live").child(organizerId).child(meetindId).child("productOrder").observeSingleEvent(of: .value) { (snapshot) in
            for child in snapshot.children {
                if let childSnapshot = child as? DataSnapshot {
                    if let childDict = childSnapshot.value as? [String: Any] {
                        let id = childDict["id"] as? String ?? ""
                        let productId = childDict["productId"] as? String ?? ""
                        let productName = childDict["productName"] as? String ?? ""
                        let productPrice = childDict["productPrice"] as? Float ?? 0.0
                        let bidderId = childDict["bidderId"] as? String ?? ""
                        let bidderImage = childDict["bidderImage"] as? String ?? ""
                        let bidderName = childDict["bidderName"] as? String ?? ""
                        let orderMeetindId = childDict["meetindId"] as? String ?? ""
                        let organizer = childDict["organizer"] as? String ?? ""
                        
                        orders.append(.init(id: id, bidderId: bidderId, bidderImage: bidderImage, bidderName: bidderName, meetingId: orderMeetindId, meetingOrganizer: organizer, productId: productId, productName: productName, productPrice: productPrice))
                        
                    }
                }
            }
            completion(orders)
            
        }
    }
    
    
    func joinStreaming(organizerUserName: String, meetingId: String, userName: String) {
        let usersOnline = [userName: true]
        ref.child("live").child(organizerUserName).child(meetingId).child("usersOnline").updateChildValues(usersOnline)
    }
    
    
    func acceptBiddingOffer(organizerName: String, meetingId: String, productId: String, productName: String, bidderId: String, bidderName: String, message: AuctionMessage, completion: @escaping (Bool, String, String) -> Void) {
        let dict = ["isSold": true]
        ref.child("live").child(organizerName).child(meetingId).child("products").child(productId).updateChildValues(dict) { (error, ref) in
            
            if error == nil {
                
                let soldProductId = UUID().uuidString
                let soldProductInfoDictionary = ["id": soldProductId ,"productId": productId, "productName": productName, "productPrice": message.biddingPrice, "bidderImage":message.senderImage, "bidderId": bidderId, "bidderName":bidderName, "meetindId": meetingId, "organizer": organizerName] as [String : Any]
                
                self.ref.child("live").child(organizerName).child(meetingId).child("productOrder").child(soldProductId).updateChildValues(soldProductInfoDictionary) { (error, ref) in
                    if error == nil {
                        
                        self.ref.child("live").child(organizerName).child(meetingId).child("chat").child(message.id).updateChildValues(["biddingStatus":true]) { (error, ref) in
                            if error == nil {
                                completion(true, productName, bidderName)
                            }else {
                                completion(false, "", "")
                            }
                        }
                    }else {
                        completion(false, "", "")
                        // fails
                    }
                }
            }else {
                //fails
                completion(false, "", "")
            }
        }
    }
    
    
    func markBidMessageAsAssepted() {
        
    }
}




protocol GoLiveDelegate: class {
    func VCDismissed(role: AgoraClientRole, meetindId: String, organizerId: String)
}

class GoLiveVC: UIViewController {
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var startPriceTextField: AppTextField!
    @IBOutlet weak var productNameTextField: AppTextField!
    
    @IBOutlet weak var backBtn: UIButton!
    var videoDataOutput: AVCaptureVideoDataOutput!
    var videoDataOutputQueue: DispatchQueue!
    var previewLayer:AVCaptureVideoPreviewLayer!
    var captureDevice : AVCaptureDevice!
    let session = AVCaptureSession()
    private var settings = LiveSettings()
    var initialBiddingPrice: Int = 0
    private var viewModel = AuctionViewModel()
    weak var delegate: GoLiveDelegate?
    
    private lazy var agoraKit: AgoraRtcEngineKit = {
        let engine = AgoraRtcEngineKit.sharedEngine(withAppId: AgoraAppId, delegate: nil)
        engine.setLogFilter(AgoraLogFilter.info.rawValue)
        engine.setLogFile(FileCenter.logFilePath())
        return engine
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupAVCapture()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        self.stopCamera()
    }
    
    
    class func GoLiveVC() -> GoLiveVC {
        return self.initFromStoryboard() as! GoLiveVC
    }
    
    
    @IBAction func goLiveTapped(_ sender: Any) {
        if productNameTextField.text!.count > 0 && startPriceTextField.text!.count > 0  {
            if let price = Float(startPriceTextField.text!) {
                
                viewModel.startSteraming(productName: productNameTextField.text!, productPrice: price) { (meetingId) in
                    self.settings.roomName = "\(Session.Username)-live"
                    self.settings.frameRate = .fps30
                    self.settings.role = .broadcaster
                    let vc = LiveRoomViewController.LiveRoomViewController()
                    vc.delegate = self
                    vc.liveInfo = StreamingInfo(organizerUserName: Session.Username, meetingId: meetingId, userImageUrl: Session.avatar, organizerImageUrl: Session.avatar)
                    vc.dataSource = self
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }else {
            Util.showAlert(caller: self, title: "Error", message: "You must provide product details to bid")
        }
        
        
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension GoLiveVC:  AVCaptureVideoDataOutputSampleBufferDelegate{
    func setupAVCapture(){
        session.sessionPreset = AVCaptureSession.Preset.vga640x480
        guard let device = AVCaptureDevice
                .default(AVCaptureDevice.DeviceType.builtInWideAngleCamera,
                         for: .video,
                         position: AVCaptureDevice.Position.front) else {
            return
        }
        captureDevice = device
        beginSession()
    }
    
    func beginSession(){
        var deviceInput: AVCaptureDeviceInput!
        
        do {
            deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            guard deviceInput != nil else {
                print("error: cant get deviceInput")
                return
            }
            
            if self.session.canAddInput(deviceInput){
                self.session.addInput(deviceInput)
            }
            
            videoDataOutput = AVCaptureVideoDataOutput()
            videoDataOutput.alwaysDiscardsLateVideoFrames=true
            videoDataOutputQueue = DispatchQueue(label: "VideoDataOutputQueue")
            videoDataOutput.setSampleBufferDelegate(self, queue:self.videoDataOutputQueue)
            
            if session.canAddOutput(self.videoDataOutput){
                session.addOutput(self.videoDataOutput)
            }
            
            videoDataOutput.connection(with: .video)?.isEnabled = true
            
            previewLayer = AVCaptureVideoPreviewLayer(session: self.session)
            previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
            
            let rootLayer :CALayer = self.cameraView.layer
            rootLayer.masksToBounds = true
            previewLayer.masksToBounds = true
            previewLayer.frame = rootLayer.bounds
            previewLayer.cornerRadius = 10
            rootLayer.cornerRadius = 10
            rootLayer.addSublayer(self.previewLayer)
            session.startRunning()
        } catch let error as NSError {
            deviceInput = nil
            print("error: \(error.localizedDescription)")
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        // do stuff here
    }
    
    // clean up AVCapture
    func stopCamera(){
        session.stopRunning()
    }
    
}


extension GoLiveVC: LiveVCDataSource {
    func liveVCNeedSettings() -> LiveSettings {
        return settings
    }
    
    func liveVCNeedAgoraKit() -> AgoraRtcEngineKit {
        return agoraKit
    }
}






class FileCenter {
    static func logDirectory() -> String {
        let directory = documentDirectory() + "/AgoraLogs"
        checkAndCreateDirectory(at: directory)
        return directory
    }
    
    static func logFilePath() -> String {
        return logDirectory() + "/agora-rtc.log"
    }
    
    static func audioFilePath() -> String {
        return Bundle.main.path(forResource: "Them", ofType: "mp3")!
    }
}

private extension FileCenter {
    static func documentDirectory() -> String {
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
    }
    
    static func checkAndCreateDirectory(at path: String) {
        var isDirectory: ObjCBool = false
        let exists = FileManager.default.fileExists(atPath: path, isDirectory: &isDirectory)
        if !exists || !isDirectory.boolValue {
            try? FileManager.default.createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
        }
    }
}


extension GoLiveVC: LiveRoomVCDelegate {
    func VCDismissed(role: AgoraClientRole, meetindId: String, organizerId: String) {
        delegate?.VCDismissed(role: role, meetindId: meetindId, organizerId: organizerId)
        self.navigationController?.popViewController(animated: true)
    }
}
