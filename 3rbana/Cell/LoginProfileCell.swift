//
//  LoginProfileCell.swift
//  3rbana
//
//  Created by Mac on 31/03/2021.
//

import UIKit
protocol ProfileProtocal {
    func navigateRegisterVC()
    func navigateLoginVC()
   
}

class LoginProfileCell : UITableViewCell {
    var delegateProfile : ProfileProtocal?
    var identifier = "LoginProfileCell"

    // MARK: - Define Parameter
    @IBOutlet weak var titleLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.titleLbl.text = "LoginDescribeLbl".localizableString(loc:"en")
                self.titleLbl.font = AppTheme.FontArabic(fontSize: 20.0)
            }else{
                self.titleLbl.font = AppTheme.FontArabic(fontSize: 20.0)
                self.titleLbl.text = "LoginDescribeLbl".localizableString(loc:"ar-EG")
            }
        }
    }
    
    // MARK: - Define Parameter
    @IBOutlet weak var registerTitleLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.registerTitleLbl.text = "no_account".localizableString(loc:"en")
                
            }else{
                self.registerTitleLbl.font = AppTheme.FontArabic(fontSize: 22.0)
                self.registerTitleLbl.text =  "no_account".localizableString(loc:"ar-EG")
            }
        }
    }
    @IBOutlet weak var LoginUser: UIButton!{
        didSet {
            self.LoginUser.setTitleColor(AppTheme.setBlackColor(), for: UIControl.State.normal)
            if Session.Language == "en"{
                self.LoginUser.setTitle("loginBttn".localizableString(loc:"en"), for: UIControl.State.normal)
                
            }
            else{
                self.LoginUser.titleLabel?.font = AppTheme.FontArabic(fontSize: 18.0)
                self.LoginUser.setTitle("loginBttn".localizableString(loc:"ar-EG"), for: UIControl.State.normal)
            }
        }
    }

    @IBOutlet weak var createBttn: UIButton!{
        didSet {
            if Session.Language == "en"{
                self.createBttn.setTitle( "registerBttn".localizableString(loc:"en"), for: UIControl.State.normal)
            }
            else{
                self.createBttn.titleLabel?.font = AppTheme.FontArabic(fontSize: 18.0)
                self.createBttn.setTitle( "registerBttn".localizableString(loc:"ar-EG"), for: UIControl.State.normal)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
    
    @IBAction func navigateRegisterVC(_ sender: Any) {
        delegateProfile?.navigateRegisterVC()
    }
    @IBAction func navigateLoginVC(_ sender: Any) {
        delegateProfile?.navigateLoginVC()
    }
}
