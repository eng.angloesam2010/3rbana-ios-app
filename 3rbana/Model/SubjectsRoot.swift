//
//  LevelCell.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/5/20.
//


import Foundation
struct SubjectsRoot : Codable  {
    let subjects      : [Subjects]?
	let subjectsCount : Int?
    
    
    enum CodingKeys : String, CodingKey {
     
		case subjects      = "Subjects"
		case subjectsCount = "SubjectsCount"
	}
    init(from decoder: Decoder) throws {
        let values    = try decoder.container(keyedBy: CodingKeys.self)
        subjects      = try values.decodeIfPresent([Subjects].self, forKey: .subjects)
        subjectsCount = try values.decodeIfPresent(Int.self, forKey: .subjectsCount)
   }
}






