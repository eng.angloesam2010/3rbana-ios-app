//
//  ProfileCell.swift
//  3rbana
//
//  Created by Mac on 31/03/2021.
//

import UIKit

public struct CellModel {
    var menuTitle   : String
    var iconName    : String

}

class ProfileCell : UITableViewCell {
    var cellIdentifier = "ProfileCell"
    @IBOutlet weak var titleLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.titleLbl.font = AppTheme.FontArabic(fontSize: 20.0)
                self.titleLbl.textAlignment = .left
                
            }else{
            
                self.titleLbl.font = AppTheme.FontArabic(fontSize: 20.0)
                self.titleLbl.textAlignment = .right
            }
        }
    }
    
    
    @IBOutlet weak var iconImageView: UIImageView!{
        didSet {
            if Session.Language == "en" {
                self.iconImageView.image = UIImage(named:"right-arrow 7")
            }else{
                self.iconImageView.image = UIImage(named:"left-arrow 1-1")
            }
        }
    }
    static var identifier: String {
        return String(describing: self)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            contentView.backgroundColor = AppTheme.setBlackColor()
        } else {
            contentView.backgroundColor = AppTheme.setBlackColor()
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    
    func setCellData(cellModel : CellModel){
        self.titleLbl.text = cellModel.menuTitle
        self.iconImageView.image = UIImage(named: "\(cellModel.iconName)")
    }
    
    
}
