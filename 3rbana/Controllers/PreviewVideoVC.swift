//
//  PreviewVideoVC.swift
//  al_Jalboot_app
//
//  Created by Mac on 12/21/20.
//

import UIKit
import WebKit
class PreviewVideoVC: UIViewController ,NavigationBarProfileDelegate{
    @IBOutlet weak var navBarView: NavigationBar!
    var UrlVideo : String!
    var titleVideo : String!

    @IBOutlet weak var bgView: UIImageView!{
        didSet{
            Session.start()
            if !Session.avatar.isEmpty {
                self.bgView.image = (Session.avatar == "1") ?  UIImage(named:"bg_b") :  UIImage(named:"bg_g")
            }
        }
    }
    @IBOutlet weak var webView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        setPropertyTitleForNav()
        startShowVideo(videoUrl: UrlVideo)
        // Do any additional setup after loading the view.
    }
    
    // MARK: - set property of custome navigator
    fileprivate func setPropertyTitleForNav(){
        self.navBarView.title           = titleVideo
        self.navBarView.screenTitleStr  = VIDEOS_TITLE_HOME
        self.navBarView.BackButtonImage = "back_bttn"
        self.navBarView.delegateProfile = self // set delegate for Navigator bar
    }
    // MARK: - function for delegate to dismis
    func backViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    func startShowVideo(videoUrl:String){
        if let url = URL(string:videoUrl) {
            webView.load(URLRequest(url: url))
            webView.navigationDelegate = self as WKNavigationDelegate
            webView.allowsBackForwardNavigationGestures = true
        }
    }
        
}
extension  PreviewVideoVC : WKNavigationDelegate{
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
     }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        ProgressHUDViewModel.sharedInstance.showGProgressHUD()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
     }
}

