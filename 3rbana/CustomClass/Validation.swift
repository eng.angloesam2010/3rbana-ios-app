//
//  Validation.swift
//  3rbana
//
//  Created by Mac on 28/03/2021.
//
import Foundation
class Validation {
    public func validateEmpty(name: String) ->Bool {
        return name.isEmpty
    }
    public func validateName(name: String) ->Bool {
        // Length be 18+ characters max and 3 characters minimum, you can always modify.
        let nameRegex = "^\\w{3,20}$"
        let trimmedString = name.trimmingCharacters(in: .whitespaces)
        let validateName = NSPredicate(format: "SELF MATCHES %@", nameRegex)
        let isValidateName = validateName.evaluate(with: trimmedString)
        return isValidateName
    }
    
    public func checkPasswordLength(passStr: String) -> Bool {
        return (passStr.count < 5 )
    }
    public func validaPhoneNumber(phoneNumber: String) -> Bool {
        return phoneNumber.isPhone()
    }

       public func validateEmailId(emailID: String) -> Bool {
          let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
          let trimmedString = emailID.trimmingCharacters(in: .whitespaces)
          let validateEmail = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
          let isValidateEmail = validateEmail.evaluate(with: trimmedString)
          return isValidateEmail
       }
    //   public func validatePassword(password: String) -> Bool {
    //      //Minimum 8 characters at least 1 Alphabet and 1 Number:
    //      let passRegEx = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$"
    //      let trimmedString = password.trimmingCharacters(in: .whitespaces)
    //      let validatePassord = NSPredicate(format:"SELF MATCHES %@", passRegEx)
    //      let isvalidatePass = validatePassord.evaluate(with: trimmedString)
    //      return isvalidatePass
    //   }
    //   public func validateAnyOtherTextField(otherField: String) -> Bool {
    //      let otherRegexString = "Your regex String"
    //      let trimmedString = otherField.trimmingCharacters(in: .whitespaces)
    //      let validateOtherString = NSPredicate(format: "SELF MATCHES %@", otherRegexString)
    //      let isValidateOtherString = validateOtherString.evaluate(with: trimmedString)
    //      return isValidateOtherString
    //   }
}
