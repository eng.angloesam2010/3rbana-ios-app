//
//  AdvteriseListCell.swift
//  3rbana
//
//  Created by Mac on 28/04/2021.
//

import UIKit

class AdvertiseListCell : UITableViewCell {
    
    var cellIdentifier = "AdvertiseListCell"
    @IBOutlet weak var imgPost: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    static var identifier: String {
        return String(describing: self)
    }
    
    func configureCell(imgPath:String){
        if let imagePath = imgPath as String? {
            self.imgPost.contentMode = .scaleToFill
            let endPointUrl = EndPoint.pathImage(pathImage:imagePath)
            self.imgPost?.kf.setImage(with: URL(string:endPointUrl.url),placeholder:UIImage(named: "new_logo"))
        }
    }
    
    
}
