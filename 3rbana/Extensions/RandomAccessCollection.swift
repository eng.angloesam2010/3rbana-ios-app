//
//  RandomAccessCollection + Extension.swift
//  Block6 Academy
//
//  Created by Bilal Mustafa on 04/11/2020.
//

import Foundation
extension RandomAccessCollection {
    func element(at index: Index) -> Element? {
        guard indices.contains(index) else {
            return nil
        }

        return self[index]
    }
}

