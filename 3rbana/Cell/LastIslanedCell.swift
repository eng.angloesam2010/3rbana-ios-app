//
//  LastIslanedCell.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 12/6/20.
//

import UIKit

class LastIslanedCell : UITableViewCell {
    
    var identifier = "LastIslanedCell"

    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
