//
//  EmptyDataSet.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/11/20.
//

import Foundation
import EmptyKit

protocol EmptyDataSourceProtocal : EmptyDataSource , EmptyDelegate{
    var  typeInt : Int { set get }
    func imageForEmpty(in view: UIView) -> UIImage?
    func titleForEmpty(in view: UIView) -> NSAttributedString?
    func descriptionForEmpty(in view: UIView) -> NSAttributedString?
    func buttonImageForEmpty(forState state: UIControl.State, in view: UIView) -> UIImage?
}
extension EmptyDataSourceProtocal {
    
    func imageForEmpty(in view: UIView) -> UIImage? {
        switch typeInt {
        case 1:
            return UIImage(named: "Placeholder")
        case 2:
            return UIImage(named: "Placeholder")
        case 3:
            return UIImage(named: "Placeholder")
    
        default:
            return UIImage(named: "Placeholder")
        }
    }
    
    func titleForEmpty(in view: UIView) -> NSAttributedString? {
        var title = ""
        let font =  AppTheme.FontArabic(fontSize: CGFloat(fontSizeEmptyLbl))
        switch typeInt {
        case 1:
            title = internetConnectionAr
            break
        case 2:
            title = (getLang() == "en") ? IssueLoadingEn : IssueLoadingAr
        case 3:
            title = ConnectionIssueAr
        case 4:
            title = (getLang() == "en") ? NoPostEn : NoPostAr
            break
        default:
            title = ConnectionIssueAr
        }
        let attributes: [NSAttributedString.Key : Any] = [.foregroundColor:AppTheme.setGoldenColor(), .font: font]
        return NSAttributedString(string: title, attributes: attributes)
    }
    
    func buttonTitleForEmpty(forState state: UIControl.State, in view: UIView) -> NSAttributedString? {
        let font  = AppTheme.FontArabic(fontSize: CGFloat(fontSizeEmptyBttn))
        let title = (getLang() == "en") ? TitleBttnEn : TitleBttnAr
        let attributes: [NSAttributedString.Key : Any] = [.foregroundColor:AppTheme.setBlackColor(), .font: font]
        return NSAttributedString(string: title, attributes: attributes)
    }
    

    func buttonBackgroundImageForEmpty(forState state: UIControl.State, in view: UIView) -> UIImage? {
        return UIImage(named: "goldenBttn")
    }
    
    func getLang() -> String {
        Session.start()
        return Session.Language
    }



}


