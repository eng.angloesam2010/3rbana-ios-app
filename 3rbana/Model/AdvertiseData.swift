//
//  AdvertiseData.swift
//  3rbana
//
//  Created by Mac on 28/04/2021.
//

import Foundation

struct AdvertiseData : Codable {
    var avatar : AdvertiseAvatar?
    enum CodingKeys: String, CodingKey {
        case avatar = "avatar"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        if let avatar = try values.decodeIfPresent(AdvertiseAvatar.self, forKey: .avatar){
            self.avatar = avatar
        }else {
            self.avatar = nil
        }
    }
    
}
