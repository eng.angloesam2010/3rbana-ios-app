//
//  RegisterVC.swift
//  3rbana
//
//  Created by Mac on 28/03/2021.
//

import UIKit
import AYPopupPickerView

class RegisterVC : UIViewController,BackNavigationDelegate {

    @IBOutlet weak var navBarView: BackNavigationBar!
    var countryArr : [String] = []
    lazy var countryListViewModel : CountryViewModel = {
        return CountryViewModel()
    }()
    
    lazy var loginViewModel: LoginViewModel = {
        return LoginViewModel()
    }()
    
    lazy var registerViewModel : RegisterViewModel = {
        return RegisterViewModel()
    }()

    
    var doneStr : String!{
        get {
            if Session.Language == "en" {
                return "done".localizableString(loc:"en")
                
            }else{
                return "done".localizableString(loc:"ar-EG")
            }
        }
    }
    
    var cancelStr: String!{
        get {
            if Session.Language == "en" {
                return "cancel".localizableString(loc:"en")
                
            }else{
                return "cancel".localizableString(loc:"ar-EG")
            }
        }
    }
    
    var countryFlag = 0
    var typeInt: Int = 0
    @IBOutlet weak var titleLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.titleLbl.text = "RegisterTitle".localizableString(loc:"en")
                
            }else{
                self.titleLbl.font = AppTheme.FontArabic(fontSize: 22.0)
                self.titleLbl.text = "RegisterTitle".localizableString(loc:"ar-EG")
            }
        }
    }
    @IBOutlet weak var emailTxtField: PaddableTextField!{
        didSet {
            if Session.Language == "en" {
                self.emailTxtField.attributedPlaceholder = self.emailTxtField.setPlaceholder(string: "Email".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.emailTxtField.textAlignment = .left
                self.emailTxtField.setIconRightView(UIImage(systemName:"envelope.open")!)
                self.emailTxtField.left = 40
                
            }else{
             
                self.emailTxtField.attributedPlaceholder = self.emailTxtField.setPlaceholder(string: "Email".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.emailTxtField.textAlignment = .right
                self.emailTxtField.setIconleftView(UIImage(systemName:"envelope.open")!)
                self.emailTxtField.right = 40
            }
            self.emailTxtField.textColor = AppTheme.setGoldenColor()
            self.emailTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    
    @IBOutlet weak var userNameTxtField : PaddableTextField!{
        didSet {
            if Session.Language == "en" {
                self.userNameTxtField.attributedPlaceholder = self.passwordTxtField.setPlaceholder(string: "UserName".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.userNameTxtField.textAlignment = .left
                self.userNameTxtField.setIconRightView(UIImage(named: "profile")!)
                self.userNameTxtField.left = 40
                
            }else{
             
                self.userNameTxtField.attributedPlaceholder = self.passwordTxtField.setPlaceholder(string: "UserName".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.userNameTxtField.textAlignment = .right
                self.userNameTxtField.setIconleftView(UIImage(named: "profile")!)
                self.userNameTxtField.right = 40
            }
            self.userNameTxtField.textColor = AppTheme.setGoldenColor()
            self.userNameTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    @IBOutlet weak var passwordTxtField: PaddableTextField!{
        didSet {

            if Session.Language == "en" {
                self.passwordTxtField.attributedPlaceholder = self.passwordTxtField.setPlaceholder(string: "Password".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.passwordTxtField.textAlignment = .left
                self.passwordTxtField.setIconRightView(UIImage(named: "image 37")!)
                self.passwordTxtField.left = 40
                
            }else{
                self.passwordTxtField.attributedPlaceholder = self.passwordTxtField.setPlaceholder(string: "Password".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.passwordTxtField.textAlignment = .right
                self.passwordTxtField.setIconleftView(UIImage(named: "image 37")!)
                self.passwordTxtField.right = 40
            }
            self.passwordTxtField.textColor = AppTheme.setGoldenColor()
            self.passwordTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    @IBOutlet weak var confirmTxtField: PaddableTextField!{
        didSet {

            if Session.Language == "en" {
                self.confirmTxtField.attributedPlaceholder =  self.confirmTxtField.setPlaceholder(string: "ConfirmPassword".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.confirmTxtField.textAlignment = .left
                self.confirmTxtField.setIconRightView(UIImage(named: "image 37")!)
                self.confirmTxtField.left = 40
                
            }else{
                
                self.confirmTxtField.attributedPlaceholder = self.confirmTxtField.setPlaceholder(string: "ConfirmPassword".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.confirmTxtField.textAlignment = .right
                self.confirmTxtField.setIconleftView(UIImage(named: "image 37")!)
                self.confirmTxtField.right = 40
            }
            self.confirmTxtField.textColor = AppTheme.setGoldenColor()
            self.confirmTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    @IBOutlet weak var telephoneTxtField: PaddableTextField!{
        didSet {
            if Session.Language == "en" {
                self.telephoneTxtField.attributedPlaceholder = self.telephoneTxtField.setPlaceholder(string: "Telep".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.telephoneTxtField.textAlignment = .left
                self.telephoneTxtField.setIconRightView(UIImage(named: "call 1")!)
                self.telephoneTxtField.left = 40
            }else{
                
                self.telephoneTxtField.attributedPlaceholder = self.telephoneTxtField.setPlaceholder(string: "Telep".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.telephoneTxtField.textAlignment = .right
                self.telephoneTxtField.setIconleftView(UIImage(named: "call 1")!)
                self.telephoneTxtField.right = 40
            }
            self.telephoneTxtField.textColor = AppTheme.setGoldenColor()
            self.telephoneTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    @IBOutlet weak var createBttn: UIButton!{
        didSet {
            self.createBttn.setTitleColor(AppTheme.setBlackColor(), for: UIControl.State.normal)
            if Session.Language == "en"{
                self.createBttn.setTitle( "RegisterTitle".localizableString(loc:"en"), for: UIControl.State.normal)
              
            }
            else{
                self.createBttn.titleLabel?.font = AppTheme.FontArabic(fontSize: 18.0)
                self.createBttn.setTitle( "RegisterTitle".localizableString(loc:"ar-EG"), for: UIControl.State.normal)
            }
        }
    }
    @IBOutlet weak var countryBttn: UIButton!{
        didSet {
          //  self.countryBttn.setTitleColor(AppTheme.setGoldenColor(), for: UIControl.State.normal)
            self.countryBttn.setTitleColor(AppTheme.setGoldenColor(), for: UIControl.State.normal)
            if Session.Language == "en"{
                self.countryBttn.setTitle("Country".localizableString(loc:"en"), for: UIControl.State.normal)
//                self.userNameTxtField.text = "RegisterTitle".localizableString(loc:"en")
            }
            else{
                self.countryBttn.setTitle( "Country".localizableString(loc:"ar-EG"), for: UIControl.State.normal)
            }
        }
    }
 
    override func viewDidLoad() {
        super.viewDidLoad()
        startFetchCountry()
        //self.telephoneTxtField.delegate = self
        // dismiss Keyboard
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        self.navBarView?.delegateProfile = self

    }

    
    @IBAction func showPickerView(_ sender: Any) {
        dismissKeyboard()
        let popupPickerView = AYPopupPickerView()
        popupPickerView.headerView.backgroundColor = .black
        popupPickerView.doneButton.setTitle(doneStr, for: .normal)
        popupPickerView.cancelButton.setTitle(cancelStr, for: .normal)
        popupPickerView.doneButton.setTitleColor(AppTheme.setGoldenColor(), for: .normal)
        popupPickerView.cancelButton.setTitleColor(AppTheme.setGoldenColor(),for: .normal)
     //   popupPickerView.backgroundColor  = .black
//        pop
        popupPickerView.display(itemTitles: countryArr, doneHandler: { [self] in
            let selectedIndex  = popupPickerView.pickerView.selectedRow(inComponent: 0)
            self.countryBttn.setTitle("\(self.countryArr[selectedIndex])", for: .normal)
            self.countryFlag = self.countryListViewModel.getSelectedCountryID(countryNameStr: "\(self.countryArr[selectedIndex])")
        })
    }

    fileprivate func clearFields() {
        self.emailTxtField.text     = ""
        self.passwordTxtField.text  = ""
        self.telephoneTxtField.text = ""
        self.confirmTxtField.text   = ""
        self.userNameTxtField.text  = ""
    }
    
    @IBAction func createNewAccount(_ sender: Any) {
        dismissKeyboard()
        // pass UserName and Password to LoginModel
        self.registerViewModel.registerModel = RegisterModel(password: self.passwordTxtField.text!, name: self.userNameTxtField.text! , email:self.emailTxtField.text!, phone: self.telephoneTxtField.text! , countryId: "\(countryFlag)", block: "1", avenue:  "1", street:  "1", houseNumber:  "1", avatar:  "1", confirmPass:  self.confirmTxtField.text!,accessToken: "",refreshToken:"")


        // MARK: - when colusre was updaete alert will be shown
        self.registerViewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.registerViewModel.alertMessage {
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    let alert = AlertActionManager.Error
                    alert.getAlert(errorDescribe: message)
                }
            }
        }

        self.registerViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.registerViewModel.state {
                case .empty, .noconnect:
                    DispatchQueue.main.async {
                        if let message = self.registerViewModel.alertMessage {
//                            /self.clearFields()
                            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                            let alert = AlertActionManager.Error
                            alert.getAlert(errorDescribe: message)
                        }
                    }
                case .error :
                    DispatchQueue.main.async {
                        if let message = self.registerViewModel.alertMessage {
                            self.clearFields()
                            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                            let alert = AlertActionManager.Error
                            alert.getAlert(errorDescribe: message)
                        }
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    self.loginMethod(CivilIDString: self.emailTxtField.text!, PasswordString: self.passwordTxtField.text!)
                    
                }
            }
        }
        self.registerViewModel.registerNewUser()
    }
    // MARK: - set property of custome navigator
    fileprivate func setPropertyTitleForNav() {
        self.navBarView.delegateProfile = self // set delegate for Navigator bar
    }
    func backViewController() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func notificationViewController() {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let notificationVC = storyBoard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        notificationVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
}

extension RegisterVC {
    
    
    func loginMethod(CivilIDString : String , PasswordString: String){
        loginViewModel.loginArray = LoginModel(CivilIDString:CivilIDString, PasswordString: PasswordString)
        // MARK: - when colusre was updaete alert will be shown
        self.loginViewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.loginViewModel.alertMessage {
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    let alert = AlertActionManager.Error
                    alert.getAlert(errorDescribe: message)
                }
            }
        }
        
        self.loginViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.loginViewModel.state {
                case .empty, .error, .noconnect:
                    DispatchQueue.main.async {
                        if let message = self.loginViewModel.alertMessage {
                            self.userNameTxtField.text = ""
                            self.passwordTxtField.text = ""
                            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                            let alert = AlertActionManager.Error
                            alert.getAlert(errorDescribe: message)
                        }
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    self.navigateSettingsVC()
                }
            }
        }
        
        self.loginViewModel.CheckLoginUser()
    }
    
    // MARK: - Fetch Level Part
    func startFetchCountry(){
        // MARK: - when colusre was updaete alert will be shown
        self.countryListViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.countryListViewModel.state {
                case .empty, .error:
                    self.typeInt = 2
                    if let message = self.countryListViewModel.alertMessage {
                        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                        let alert = AlertActionManager.Error
                        alert.getAlert(errorDescribe: message)
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    DispatchQueue.main.async {
                        self.fillArrayCountry()
                    }
                case .noconnect:
                    self.typeInt = 1
                 
                }
            }
        }

        self.countryListViewModel.fetchCountry()
        
    }
    
    // MARK: fill array country
    func fillArrayCountry() {
        for i in 0..<self.countryListViewModel.countryArr.count {
            let country = self.countryListViewModel.countryArr[i]
            if let name = country.countryEnName{
               countryArr.append(name)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          if let x = string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) {
             return true
          } else {
          return false
       }
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    

    
}

