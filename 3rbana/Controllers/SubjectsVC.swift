//
//  LevelDetailsVC.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/17/20.
//

import Foundation
import UIKit
import AnimatableReload
import EmptyKit
import CFAlertViewController

class CategoryVC: UIViewController, EmptyDataSourceProtocal, NavigationBarDelegate {
    
    var isAnimating = false
  
    // MARK: - Property of view controller
    @IBOutlet weak var navBarView: NavigationBar!
    @IBOutlet weak var bgView: UIImageView! {
        didSet{
            Session.start()
            if !Session.Sex.isEmpty {
                self.bgView.image = (Session.Sex == "1") ?  UIImage(named:"bg_b") :  UIImage(named:"bg_g")
            }else{
                self.bgView.image = (Sex == "1") ?  UIImage(named:"bg_b") :  UIImage(named:"bg_g")
            }
        }
    }
    var islandModel = SubjectCellViewModel()
    var civilID : String!, StageName : String!,Gender : String!,StudenSchoolName:String!,StudentName:String!,Sex:String!,password:String!
    var typeInt: Int = 0
    @IBOutlet weak var SubjectsTbleView: UITableView! {
        didSet {
            self.SubjectsTbleView?.register(UINib(nibName: IslandCell.identifier, bundle: nil), forCellReuseIdentifier: IslandCell.identifier)
        }
    }
    var flagMotion: Int = 0
    var studentInfoModel: StudentInfoViewModel?
    let heightCellIphone: CGFloat = 160.0
    let heightCellIpad: CGFloat = 250.0
    lazy var subjectViewModel: CategoryViewModel = {
        return CategoryViewModel()
    }()
    // MARK: - init for Level VC
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        initTableView()
        // set property of custome navigator
        setPropertyTitleForNav()
        fetchSubject()
    }
    
    func fetchSubject(){
        Session.start()
        if !Session.CivilID.isEmpty {
            self.subjectViewModel.updateLoadingStatus = { [weak self] () in
                guard let self = self else {
                    return
                }
                
                DispatchQueue.main.async { [weak self] in
                    guard let self = self else {
                        return
                    }
                    switch self.subjectViewModel.state {
                    case .noconnect:
                        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                        self.typeInt = 1
                        self.setEmptyDataSet()
                    case .error:
                        DispatchQueue.main.async {
                            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                            self.typeInt = 2
                            self.setEmptyDataSet()
                        }
                    case .empty:
                        DispatchQueue.main.async {
                            self.typeInt = 4
                            self.setEmptyDataSet()
                        }
                    case .loading:
                        ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                    case .populated:
                        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                        self.studentInfoModel = self.subjectViewModel.sudentInfoArray!
                        // reload TableView Method
                        self.loadTableView()
                
                }
                }
            }
            
            self.subjectViewModel.fetchSubject()
            
            
        }else{
            // reload TableView Method
            loadTableView()
            fillSession()
            
        }
    }
    func fillSession(){
        Session.start()
        Session.CivilID     = self.civilID
        Session.password    = self.password
        Session.StageName   = self.StageName
        Session.Sex         = self.Sex
        Session.Gender      = self.Gender
        Session.SchoolName  = self.StudenSchoolName
        Session.StudentName = self.StudentName
        Session.Sex         = self.StudentName
        Session.save()
    }
    func loadTableView() {
        if (studentInfoModel?.subjects.count)! > 0 {
            startAnimation()
            Session.start()
            Session.removePositionBoat()
            reloadTableView()
            
        } else {
            self.typeInt = 2
            setEmptyDataSet()
        }
    }
    // MARK: - set property of custome navigator
    fileprivate func setPropertyTitleForNav() {
        
        self.navBarView.screenTitleStr = VIDEOS_TITLE_HOME
        Session.start()
        if !Session.CivilID.isEmpty {
            self.navBarView.title = Session.StageName
        }else{
            if let StudenStage = studentInfoModel?.StudenStage {
                self.navBarView.title = StudenStage
            }
        }
        
        //Session.StageName
        
        self.navBarView.delegate = self // set delegate for Navigator bar
    }
    
    // MARK: - init Collectionview
    func initTableView() {
        self.SubjectsTbleView?.register(UINib(nibName: IslandCell.identifier, bundle: nil), forCellReuseIdentifier: IslandCell.identifier)
        self.SubjectsTbleView?.register(UINib(nibName: LastIslanedCell.identifier, bundle: nil), forCellReuseIdentifier: LastIslanedCell.identifier)
    }
    // MARK: - Reload Collectionview
    func reloadTableView() {
        initTableView()
        self.SubjectsTbleView?.dataSource = self
        self.SubjectsTbleView?.delegate = self
        self.SubjectsTbleView?.reloadData()
        self.startAnimation()
        
    }
    
    // MARK: - Set datasource for collectionview
    func setEmptyDataSet() {
        self.SubjectsTbleView.ept.dataSource = self as EmptyDataSource
        self.SubjectsTbleView.ept.delegate   = self as EmptyDelegate
        self.SubjectsTbleView.ept.reloadData()
        self.SubjectsTbleView.alwaysBounceVertical = false
        self.SubjectsTbleView.isScrollEnabled = false
        self.SubjectsTbleView.isPagingEnabled = false
        
    }
    
    // MARK: - Start Animation for view controller
    func startAnimation() {
        AnimatableReload.reload(tableView: self.SubjectsTbleView, animationDirection: "down")
    }
    
    // MARK: - go to Login When Press Login Button
    func popViewController() {
        // Create Alet View Controller
        // Create Alet View Controller
        let alertController = CFAlertViewController(title: Application_Title,
                                                    message: nil,
                                                    textAlignment: .center,
                                                    preferredStyle: .actionSheet,
                                                    didDismissAlertHandler: nil)
        
        // Create Upgrade Action
        let viewProfileAction = CFAlertAction(title: View_Pofile_Title,
                                              style: .Destructive,
                                              alignment: .justified,
                                              backgroundColor: .systemBlue,
                                              textColor: nil,
                                              handler: { (action) in
                                                self.viewProfileVC()
                                              })
        let logoutAction = CFAlertAction(title: Logout_Title,
                                         style: .Destructive,
                                         alignment: .justified,
                                         backgroundColor:  .systemBlue,
                                         textColor:nil,
                                         handler: { (action) in
                                            self.logout()
                                         })
        let cancelAction = CFAlertAction(title: Cancel_Title,
                                         style: .Cancel,
                                         alignment: .justified,
                                         backgroundColor: .white,
                                         textColor:nil,
                                         handler: nil)
        
        // Add Action Button Into Alert
        alertController.addAction(viewProfileAction)
        alertController.addAction(logoutAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func viewProfileVC(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let profileVC = storyBoard.instantiateViewController(withIdentifier: "ProfileVC") as! ProfileVC
        profileVC.modalPresentationStyle = .fullScreen
        self.present(profileVC, animated: true, completion: nil)
    }
    
    
    func logout(){
        
        
        
        let alertController = CFAlertViewController(title: Logout_Title,
                                                    message: Logout_Message_Ar,
                                                    textAlignment: .center,
                                                    preferredStyle: .notification,
                                                    didDismissAlertHandler: nil)
        
        // Create Upgrade Action
        let yesAction = CFAlertAction(title: Yes_AR,
                                      style: .Destructive,
                                      alignment: .justified,
                                      backgroundColor: .systemBlue,
                                      textColor: nil,
                                      handler: { (action) in
                                        Session.start()
                                        Session.removePositionBoat()
                                        
                                        Session.reset()
                                        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                        let subjectsVC = storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                                        subjectsVC.modalPresentationStyle = .fullScreen
                                        self.present(subjectsVC, animated: true, completion: nil)
                                      })
        let noAction = CFAlertAction(title: NO_AR,
                                     style: .Destructive,
                                     alignment: .justified,
                                     backgroundColor:  .systemBlue,
                                     textColor:nil,
                                     handler: { (action) in
                                        
                                     })
        
        
        // Add Action Button Into Alert
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        
        present(alertController, animated: true, completion: nil)
        
    }
    
    
}
extension CategoryVC: EmptyDelegate {
    
    @objc(emptyButton: tappedIn:) func emptyButton(_ button: UIButton, tappedIn view: UIView) {
        self.fetchSubject()
    }
    
}

