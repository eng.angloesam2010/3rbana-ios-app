//
//  UpdateViewModel.swift
//  3rbana
//
//  Created by Mac on 08/04/2021.
//

import Foundation
import UIKit
import Alamofire
// MARK: - Model View for Cell
struct PasswordModel {
    var oldPassword    : String
    var newPassword    : String
    var confirmPassword    : String
    
}

// MARK: - Company Protocal
internal protocol UpdatePasswordProtocal {
    
    var  apiService                  : APIServiceType          { get }
    var  state                       : State                   { get set }
    var  updateLoadingStatus         : (()->())?               { get }
    var  showAlertClosure            : (()->())?               { get }
    var  alertMessage                : String?                 { get set }
    var  oldPasswordString           : String?                 { get set }
    mutating func UpdatePasswordUser()
    mutating func UpdateCoreData(newPassword : String)
}

// MARK: - Class for Model View
final class UpdatePasswordViewModel : NSObject, UpdatePasswordProtocal {
    var accessToken: String? {
        get{
            Session.start()
            return Session.accessToken
        }
    }
    
    var validation          : Validation?{
        return Validation()
    }
    
    var registerModel       : RegisterModel?
    var passwordModel       : PasswordModel?
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var alertMessage : String? = "" {
        didSet {
            self.showAlertClosure?()
        }
    }
    var Url : String!{
        get{
            let endPoint = EndPoint.changePassword
            print("\(endPoint.url)")
            return endPoint.url
        }
    }
    var oldPasswordString : String? = "" {
        didSet {
            Session.start()
            self.oldPasswordString = Session.password
        }
    }
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    
    public func UpdatePasswordUser() {
        
        self.state  = .loading
        
        // validation classe
        guard let password = passwordModel?.oldPassword ,let confirmPass = passwordModel?.newPassword ,let confirmPass2 =  passwordModel?.confirmPassword else {
            self.state         = .empty
            self.alertMessage  = Empty_Filed_Arabic
            
            return
        }
        
        if (password.isEmpty  || confirmPass.isEmpty    || confirmPass2.isEmpty ) {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? Empty_Filed_English : Empty_Filed_Arabic
            return
        }
        
        let passwordLengthValidation = self.validation!.checkPasswordLength(passStr: password)
        
        
        if  passwordLengthValidation {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? PASSWORDLENTHEN : PASSWORDLENTHAR
            return
        }
        
        
        var isValidateConfirmPassword = (password == oldPasswordString)
        
        if  isValidateConfirmPassword {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? Current_Password_Message_En : Current_Password_Message_En
            return
        }
        
        isValidateConfirmPassword = (confirmPass != confirmPass2)
        
        if  isValidateConfirmPassword {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? Confirm_Password_Message_En : Confirm_Password_Message_Ar
            return
        }
        
        isValidateConfirmPassword = (confirmPass == password)
        
        if  isValidateConfirmPassword {
            self.state         = .empty
            self.alertMessage  = (Session.Language == "en") ? NewPassSameOldPassEN : NewPassSameOldPassAR
            return
        }else{
            
            let params  = [ "oldPassword"  : passwordModel?.oldPassword as Any,
                            "newPassword"  : passwordModel?.newPassword  as Any] as NSDictionary
            self.state  = .loading
            let header = [:] as HTTPHeaders
            print("\(self.accessToken!)")
            self.apiService.requestWithToken(url: self.Url, method: .post, withParameters: params, withHeader: header, access_token: self.accessToken!){
                (response: ResultApi<Message, APIServiceError>) in
                switch response {
                case .failure(let error):
                    print("\(error.localizedDescription)")
                    if error.localizedDescription == "unKnown" {
                        self.state        = .noconnect
                        self.alertMessage = (Session.Language == "en") ? INTERNET_ENG_MESSAGE : INTERNET_AR_MESSAGE
                    }else{
                        self.state        = .error
                        self.alertMessage = (Session.Language == "en") ?    ConnectionIssueEn : ConnectionIssueAr
                    }
                    break
                    
                case .success(let result):
                    if let status = result.status {
                        if status {
                            self.state = .populated
                            self.UpdateCoreData(newPassword: confirmPass)
                            
                        }else{
                            self.state        = .error
                            if let msg = result.message {
                                self.alertMessage     = msg
                            }
                        }
                        
                    }else{
                        self.state        = .error
                        if let msg = result.message {
                            self.alertMessage     = msg
                        }
                    }
                    
                    break
                }
            }
        }
    }
    
    // MARK: - Fetch Data
    func UpdateCoreData(newPassword : String) {
        Session.start()
        if let name  = Session.Username as String? {
            Session.Username       = name
        }
        if let pass  = newPassword as String? {
            Session.password       = pass
        }
        if let Phone  = Session.Phone as String? {
            Session.Phone       = Phone
        }
        if let email  = Session.UserMail as String? {
            Session.UserMail       = email
        }
        if let address  = Session.address as String? {
            Session.address       = address
        }
        
        Session.CutID       = Session.CutID
        
        if let accessToken  = Session.accessToken as String? {
            Session.accessToken       = accessToken
        }
        if let refreshToken  = Session.refreshToken as String? {
            Session.refreshToken       = refreshToken
        }
        if let avatar  = Session.avatar as String? {
            Session.avatar       = avatar
        }
        Session.save()
    }
}

