//
//  LoginViewModel.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 12/8/20.
//

import Foundation
import UIKit
import Alamofire

// MARK: - Model View for Cell
struct LoginModel {
    var CivilIDString    : String
    var PasswordString   : String
    
}

public struct StudentInfoViewModel {
    var StudentID         : Int?
    var StudentName        : String?
    var StudenStage       : String?
    var StudenGender      : String?
    var StudenSchoolName  : String?
    var Sex               : String?
    var subjects          : [CatModel]
}

public struct CatModel {
    var ID          : Int?
    var imagePath   : String?
    var nameAr      : String?
    var nameEn      : String?
    
    //
}

// MARK: - Company Protocal
internal protocol LoginProtocal  {
    var  registerModel               : RegisterModel?             { get set }
    var  loginArray                  : LoginModel?             { get set }
    var  apiService                  : APIServiceType          { get }
    var  state                       : State                   { get set }   //enum to change UI/UX
    var  updateLoadingStatus         : (()->())?               { get }       // clouserr to update status for UI
    var  showAlertClosure            : (()->())?               { get }       // clouserr to  alert for UI
    var  alertMessage                : String?                  { get set }
    var  sudentInfoArray             : StudentInfoViewModel?   { get set }
    mutating func CheckLoginUser()
}

// MARK: - Class for Model View
final class LoginViewModel : NSObject, LoginProtocal {
    var registerModel       : RegisterModel?
    var sudentInfoArray     : StudentInfoViewModel?
    var loginArray          : LoginModel?
    var showAlertClosure    : (() -> ())?
    var updateLoadingStatus : (() -> ())?
    var apiService          : APIServiceType
    var state               : State = .empty {
        didSet {
            self.updateLoadingStatus?()
        }
    }
    var alertMessage : String? = ""  {
        didSet {
            self.showAlertClosure?()
        }
    }
    var Url: String = ""
    // MARK: - initialization of class Level Model View
    init(apiService: APIServiceType = APIServiceRequest() ) {
        self.apiService      = apiService
    }
    // MARK: - Fetch Data
    fileprivate func saveDataPlist(_ result: (Login)) {
        Session.start()
        print("\(result)")
        if let name  = result.bodyLogin?.user.name as String? {
            Session.Username       = name
        }
        if let pass  = self.loginArray?.PasswordString as String? {
            Session.password       = pass
        }
        if let Phone  = result.bodyLogin?.user.phone as String? {
            Session.Phone       = Phone
        }
        if let email  = result.bodyLogin?.user.email as String? {
            Session.UserMail       = email
        }
        if let address  = result.bodyLogin?.user.block as String? {
            Session.address       = address
        }
        if let CutID  = result.bodyLogin?.user.countryId as String? {
            Session.CutID       = CutID
        }
        if let accessToken  = result.bodyLogin?.accessToken as String? {
            Session.accessToken       = accessToken
        }
        if let refreshToken  = result.bodyLogin?.refreshToken as String? {
            Session.refreshToken       = refreshToken
        }
        if let avatar  = result.bodyLogin?.user.avatar?.thumbnailAvatarPath as String? {
            Session.avatar       = avatar
        }
        print("\(Session.avatar)")
        Session.save()
    }
    
    public func CheckLoginUser() {
        self.state  = .loading
        if (self.loginArray?.CivilIDString == "" || self.loginArray?.PasswordString == "" ){
            self.state         = .empty
            self.alertMessage  = Empty_Filed_Arabic
        }else if ((self.loginArray?.CivilIDString.isEmpty) != nil || (self.loginArray?.PasswordString.isEmpty) != nil ) {
            
            let endPoint = EndPoint.loginUserUrl
            self.Url     = endPoint.url
            print("\(self.Url)")
            let params  = [    "username" : self.loginArray?.CivilIDString,
                               "password" : self.loginArray?.PasswordString ] as NSDictionary
            let headers: HTTPHeaders = [:]
            
            self.apiService.fetchGenericData(url: Url , withParameters: params, httpMethod:.post, withHeader: headers ) { [self]
                (response: ResultApi<Login, APIServiceError>) in
                switch response {
                case .failure(let error):
                    if error.localizedDescription == "unKnown" {
                        self.state        = .noconnect
                        self.alertMessage = (Session.Language == "en") ? INTERNET_ENG_MESSAGE : INTERNET_AR_MESSAGE
                    }else{
                        self.state        = .error
                        self.alertMessage = (Session.Language == "en") ?    ConnectionIssueEn : ConnectionIssueAr
                    }
                    break
                    
                case .success(let result):
                    if let status = result.status {
                        if status {
                            self.state = .populated
                            self.saveDataPlist(result)
                        }else{
                            self.state        = .error
                            self.alertMessage = result.message
                        }
                        
                    }else{
                        self.state        = .error
                        self.alertMessage = result.message
                    }
                    
                    break
                }
            }
        }
    }
}
