//
//  LevelCell.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/5/20.
//


import Foundation

struct Subjects : Codable {
    
    let Id: Int
	let Name: String
	let ImageName: String
	let IslandImageName: String
	let IslandName: String

	private enum CodingKeys: String, CodingKey {
		case Id = "Id"
		case Name = "Name"
		case ImageName = "ImageName"
		case IslandImageName = "IslandImageName"
		case IslandName = "IslandName"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		Id = try values.decode(Int.self, forKey: .Id)
		Name = try values.decode(String.self, forKey: .Name)

		ImageName = try values.decode(String.self, forKey: .ImageName)
		IslandImageName = try values.decode(String.self, forKey: .IslandImageName)
		IslandName = try values.decode(String.self, forKey: .IslandName)
	}

	func encode(to encoder: Encoder) throws {
		var container = encoder.container(keyedBy: CodingKeys.self)
		try container.encode(Id, forKey: .Id)
		try container.encode(Name, forKey: .Name)

		try container.encode(ImageName, forKey: .ImageName)
		try container.encode(IslandImageName, forKey: .IslandImageName)
		try container.encode(IslandName, forKey: .IslandName)
	}

}


