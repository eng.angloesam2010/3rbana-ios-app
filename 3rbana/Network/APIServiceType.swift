//
//  ApiServices.swift
//  Combine_MVVM
//
//  Created by Mac on 12/30/19.
//  Copyright © 2019 Mac. All rights reserved.
//

import Foundation
import Alamofire


enum APIServiceError : Error, LocalizedError {
    case Nil,unknown, apiError(reason: String), parserError(reason: String), networkError (from: URLError),urlError(reason: String)
    var errorDescription: String? {
        switch self {
        case .unknown:
            return "unKnown"
        case .apiError(let reason), .parserError(let reason):
            return reason
        case .networkError(let from):
            return from.localizedDescription
        case .urlError(let reason):
            return reason
        case .Nil:
            return nil
        }
    }
}
enum ResultApi<Value,APIServiceError> {
    case success(Value)
    case failure(APIServiceError)
}
protocol APIServiceType {
    func fetchGenericData<T : Decodable>(url : String, withParameters Parameters: NSDictionary,httpMethod: HTTPMethod ,withHeader header : HTTPHeaders,completion: @escaping  (ResultApi<T, APIServiceError>) -> ())
    func fetchGenericDataPostWithBody<T : Decodable>(url : String, withParameters parameters: NSDictionary,access_token : String,completion: @escaping  (ResultApi<T, APIServiceError>) -> ())

    func requestWithToken<T : Decodable>(url : String,method: HTTPMethod , withParameters parameters: NSDictionary,withHeader header : HTTPHeaders,access_token : String,completion: @escaping  (ResultApi<T, APIServiceError>) -> ())
    
    func fetchGenericToken<T : Decodable>(url : String,access_token : String,completion: @escaping  (ResultApi<T, APIServiceError>) -> ())
    func apiPostMultipartFormData<T : Decodable>(url : String, withParameters parameters: NSDictionary,access_token : String ,imageRequest:ImageRequest,completion: @escaping  (ResultApi<T, APIServiceError>) -> ())
    
   
}
