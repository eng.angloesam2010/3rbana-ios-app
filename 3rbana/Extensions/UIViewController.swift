//
//  UIViewController.swift
//  3rbana
//
//  Created by Mac on 29/03/2021.
//

import Foundation
import UIKit
import PMAlertController

extension UIViewController :UITextFieldDelegate{
    
    typealias MethodHandler1 = ()->Void
    typealias MethodHandler2 = ()
    //Calls this function when the tap is recognized.
    @objc func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func navigateSettingsVC(){
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let profileVC = storyBoard.instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        profileVC.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(profileVC, animated: true)
    }
    
    fileprivate func changeDirection(lang:String){
        DirectionViewModel.sharedManager.changeLanguage(lang: lang)
        ProgressHUDViewModel.sharedInstance.showGProgressHUD(timeFor: 10.0)
        ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
        self.pushViewController(id: 0)
    }
    
    fileprivate func showAlert(title : String,description:String,image : UIImage,firstTitleButton:String,secondTitleButton:String,firstSelector:@escaping(String) -> Void,secondSelector:@escaping() -> Void,typeAlert:Int ) {
        let alertVC = PMAlertController(title:title, description: description, image:image, style: .alert)
        //alertVC.alertMaskBackground.backgroundColor = .black
        alertVC.alertActionStackView.backgroundColor = .black
        alertVC.alertImage.backgroundColor = .black
        alertVC.alertView.backgroundColor  = .black
        alertVC.alertTitle.textColor = AppTheme.setGoldenColor()
        
        switch typeAlert {
        case 1:
            alertVC.addAction(PMAlertAction(title:firstTitleButton, style: .cancel, action: { () -> Void in
                firstSelector("en")
            }))
            alertVC.addAction(PMAlertAction(title:secondTitleButton, style: .cancel, action: { () -> Void in
                firstSelector("ar")
            }))
        default:
            alertVC.addAction(PMAlertAction(title:firstTitleButton, style: .default, action: secondSelector))
            alertVC.addAction(PMAlertAction(title:secondTitleButton, style: .cancel, action: { () -> Void in
                alertVC.dismiss(animated: true, completion:nil)
            }))
        }
        
        self.present(alertVC, animated: true, completion: nil)
    }
    
    
    
    fileprivate func LogoutMethod() -> Void {
        NotificationCenter.default.post(name: Notification.Name("logoutUser"), object: nil)
        
    }
    
    
    class func initFromStoryboard() -> UIViewController? {
        let fileURLs :[URL] = Bundle.main.urls(forResourcesWithExtension: "storyboardc", subdirectory: nil)!
        
        for storyboardURL in fileURLs {
            let actualStoryboard = UIStoryboard(name: (storyboardURL.lastPathComponent as NSString).deletingPathExtension, bundle: nil)
            var controller: UIViewController?
            
            do {
                try TLExceptionHandler.catchException {
                    controller = actualStoryboard.instantiateViewController(withIdentifier: String(describing: self))
                }
            }
            catch {
                debugPrint("An error ocurred: \(error)")
                continue
            }
            return controller
        }
        
        return nil
    }
    
    func add(_ child: UIViewController) {
        addChild(child)
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }

    func remove() {
        // Just to be safe, we check that this view controller
        // is actually added to a parent before removing it.
        guard parent != nil else {
            return
        }

        willMove(toParent: nil)
        view.removeFromSuperview()
        removeFromParent()
    }
    
    func pushViewController(id:Int,categoryID : Int? = nil,flag: Int? = nil ){
        switch id {
        case 1:
            let questionsVC = QuestionsVC.instantiate(fromAppStoryboard: .main)
            self.navigationController?.pushViewController(questionsVC, animated: true)
        case 2:
            let helpVC = HelpVC.instantiate(fromAppStoryboard: .main)
            self.navigationController?.pushViewController(helpVC, animated: true)
        case 3:
            let title : String = (Session.Language == "en") ? Change_Lang_title_En : Change_Lang_tilte_Ar
            showAlert(title: title, description: "", image: UIImage(named: "main-logo")!, firstTitleButton: Change_Lang_Msg_En, secondTitleButton: Change_Lang_Msg_Ar, firstSelector: self.changeDirection, secondSelector: self.LogoutMethod, typeAlert: 1)
        case 4:
            let countryVC = CountryVC.instantiate(fromAppStoryboard: .main)
            self.navigationController?.pushViewController(countryVC, animated: true)
            
        case 5:
            let favoriteVC = FavoriteVC.instantiate(fromAppStoryboard: .main)
            self.navigationController?.pushViewController(favoriteVC, animated: true)
            
        case 6:
            let notificationVC = NotificationVC.instantiate(fromAppStoryboard: .main)
            self.navigationController?.pushViewController(notificationVC, animated: true)
        case 7:
            let updatePassVC = UpdatePassVC.instantiate(fromAppStoryboard: .main)
            self.navigationController?.pushViewController(updatePassVC, animated: true)
        case 8:
            let title = (Session.Language == "en") ?  Logout_Message_En :  Logout_Message_Ar
            let ysStr = (Session.Language == "en") ?  Yes_EN :  Yes_AR
            let noStr = (Session.Language == "en") ?  NO_EN :  NO_AR
            
            showAlert(title: title, description: "", image: UIImage(named: "main-logo")!, firstTitleButton: ysStr, secondTitleButton: noStr, firstSelector: self.changeDirection, secondSelector:  self.LogoutMethod, typeAlert: 0)
            
        case 9:
            let subCategoryVC = SubCategoryVC.instantiate(fromAppStoryboard: .main)
            subCategoryVC.categoryID =  categoryID
            subCategoryVC.flag       = flag
            self.navigationController?.pushViewController(subCategoryVC, animated: true)
            
            
        case 10:
            let subCategoryVC = SubCategoryVC.instantiate(fromAppStoryboard: .main)
            subCategoryVC.categoryID =  categoryID
            subCategoryVC.flag       = flag
            self.navigationController?.pushViewController(subCategoryVC, animated: true)
            
        case 11 :
            let uploadStoryVC = UploadStoryVC.instantiate(fromAppStoryboard: .main)
            uploadStoryVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(uploadStoryVC, animated: true)
            
        case 12 :
            let postVC = PostVC.instantiate(fromAppStoryboard: .main)
            postVC.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(postVC, animated: true)
            
        default:
            let logoVC = LogoVC.instantiate(fromAppStoryboard: .main)
            self.present(logoVC, animated: true, completion: nil)
        }
    }
    
    func empty() -> Void{
        
    }
    
}

