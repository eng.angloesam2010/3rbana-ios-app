//
//  LoadUIUtils.swift
//  3rbana
//
//  Created by Mac on 04/04/2021.
//

import Foundation
import UIKit

enum AppStoryboard: String {
    case main = "Main"
 
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
 
    func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
 
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            fatalError("ViewController with identifier , not found in  Storyboard.\nFile ")
        }
        return scene
    }
 
    func initialViewController() -> UIViewController? {
 
        return instance.instantiateInitialViewController()
    }
}
 
extension UIViewController {
    // Not using static as it wont be possible to override to provide custom storyboardID then
    class var storyboardID: String {
        return "\(self)"
    }
 
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        return appStoryboard.viewController(viewControllerClass: self)
    }
}
    
