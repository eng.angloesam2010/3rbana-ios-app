//
//  LoginVC.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 12/7/20.
//

import UIKit

class LoginVC: UIViewController,BackNavigationDelegate{
  
    @IBOutlet weak var backNavigationBar: BackNavigationBar!
    // MARK: - Define Parameter
    @IBOutlet weak var titleLbl: UILabel!{
        didSet {
            if Session.Language == "en" {
                self.titleLbl.text = "LoginTitleLbl".localizableString(loc:"en")
                
            }else{
                self.titleLbl.font = AppTheme.FontArabic(fontSize: 22.0)
                self.titleLbl.text = "LoginTitleLbl".localizableString(loc:"ar-EG")
            }
        }
    }
    @IBOutlet weak var userNameTxtField : PaddableTextField!{
        didSet {
            if Session.Language == "en" {
                self.userNameTxtField.attributedPlaceholder = self.passwordTxtField.setPlaceholder(string: "UserName".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.userNameTxtField.setIconRightView(UIImage(named: "profile")!)
                self.userNameTxtField.left = 40
                self.userNameTxtField.textAlignment = .left
                
            }else{
                
                self.userNameTxtField.attributedPlaceholder = self.passwordTxtField.setPlaceholder(string: "UserName".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.userNameTxtField.textAlignment =  .left
                self.userNameTxtField.setIconleftView(UIImage(named: "profile")!)
                self.userNameTxtField.right = 40
                self.userNameTxtField.textAlignment = .right
            }
            self.userNameTxtField.textColor = AppTheme.setGoldenColor()
            self.userNameTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    @IBOutlet weak var passwordTxtField: PaddableTextField!{
        didSet {
            
            if Session.Language == "en" {
                self.passwordTxtField.attributedPlaceholder = self.passwordTxtField.setPlaceholder(string: "Password".localizableString(loc:"en"), color: AppTheme.setGoldenColor())
                self.passwordTxtField.textAlignment = .right
                self.passwordTxtField.setIconRightView(UIImage(named: "image 37")!)
                self.passwordTxtField.left = 40
                self.passwordTxtField.textAlignment = .left
            }else{
                self.passwordTxtField.attributedPlaceholder = self.passwordTxtField.setPlaceholder(string: "Password".localizableString(loc:"ar-EG"), color: AppTheme.setGoldenColor())
                self.passwordTxtField.textAlignment = .left
                self.passwordTxtField.setIconleftView(UIImage(named: "image 37")!)
                self.passwordTxtField.textAlignment = .right
                self.passwordTxtField.right = 40
            }
            self.passwordTxtField.textColor = AppTheme.setGoldenColor()
            self.passwordTxtField.tintColor = AppTheme.setGoldenColor()
        }
    }
    @IBOutlet weak var LoginUser: UIButton!{
        didSet {
            self.LoginUser.setTitleColor(AppTheme.setBlackColor(), for: UIControl.State.normal)
            if Session.Language == "en"{
                self.LoginUser.setTitle("loginBttn".localizableString(loc:"en"), for: UIControl.State.normal)
                
            }
            else{
                self.LoginUser.titleLabel?.font = AppTheme.FontArabic(fontSize: 18.0)
                self.LoginUser.setTitle("loginBttn".localizableString(loc:"ar-EG"), for: UIControl.State.normal)
            }
        }
    }
    @IBOutlet weak var createBttn: UIButton!{
        didSet {
            //            self.createBttn.setTitleColor(AppTheme.setBlackColor(), for: UIControl.State.normal)
            if Session.Language == "en"{
                self.createBttn.setTitle( "RegisterTitle".localizableString(loc:"en"), for: UIControl.State.normal)
                
            }
            else{
                self.createBttn.titleLabel?.font = AppTheme.FontArabic(fontSize: 18.0)
                self.createBttn.setTitle( "RegisterTitle".localizableString(loc:"ar-EG"), for: UIControl.State.normal)
            }
        }
    }
    lazy var loginViewModel: LoginViewModel = {
        return LoginViewModel()
    }()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //Looks for single or multiple taps.
        self.backNavigationBar?.delegateProfile = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
        
    }
    
    
    // MARK: - Login User Event , user will be write CIVILID and add S start with s fo so that I have to check for s with start CIVIL ID
    @IBAction func LoginUser(_ sender: Any) {
        dismissKeyboard()
        // pass UserName and Password to LoginModel
    
        loginMethod(CivilIDString:  ((self.userNameTxtField.text == "") ? "" : self.userNameTxtField.text)!, PasswordString: ((self.passwordTxtField.text == "") ? "" : self.passwordTxtField.text)!)
    }
    func loginMethod(CivilIDString : String , PasswordString: String){
        loginViewModel.loginArray = LoginModel(CivilIDString:CivilIDString, PasswordString: PasswordString)
        // MARK: - when colusre was updaete alert will be shown
        self.loginViewModel.showAlertClosure = { [weak self] () in
            DispatchQueue.main.async {
                if let message = self?.loginViewModel.alertMessage {
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    let alert = AlertActionManager.Error
                    alert.getAlert(errorDescribe: message)
                }
            }
        }
        
        self.loginViewModel.updateLoadingStatus = { [weak self] () in
            guard let self = self else {
                return
            }
            DispatchQueue.main.async { [weak self] in
                guard let self = self else {
                    return
                }
                switch self.loginViewModel.state {
                case .empty, .error, .noconnect:
                    DispatchQueue.main.async {
                        if let message = self.loginViewModel.alertMessage {
                            self.userNameTxtField.text = ""
                            self.passwordTxtField.text = ""
                            ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                            let alert = AlertActionManager.Error
                            alert.getAlert(errorDescribe: message)
                        }
                    }
                case .loading:
                    ProgressHUDViewModel.sharedInstance.showGProgressHUD()
                case .populated:
                    ProgressHUDViewModel.sharedInstance.hideGProgressHUD()
                    self.navigateSettingsVC()
                }
            }
        }
        
        self.loginViewModel.CheckLoginUser()
    }
    
    
    func navigateSubjectVC() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func regsiterNewUser(_ sender: Any) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let registerVC = storyBoard.instantiateViewController(withIdentifier: "RegisterVC") as! RegisterVC
        registerVC.modalPresentationStyle = .fullScreen
        self.navigationController?.popToViewController(registerVC, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    func backViewController() {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func notificationViewController() {
        self.pushViewController(id: 6)
    }
}


