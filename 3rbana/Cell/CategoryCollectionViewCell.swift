//
//  StoryCollectionViewCell.swift
//  3rbana
//
//  Created by Mac on 31/01/2021.
//

import UIKit

class CategoryCollectionViewCell : UICollectionViewCell {
   
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    var cellIdentifier = "CategoryCollectionViewCell"
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       
    }
    func configureCell(with catModel : CatModel) {
        self.titleLbl.text = (Session.Language == "en") ? catModel.nameEn : catModel.nameAr
        if let imgPath = catModel.imagePath {
            let endPointUrl = EndPoint.pathImage(pathImage:imgPath)
            print("Linked : \(imgPath)")
            self.imgView.kf.setImage(with: URL(string:endPointUrl.url),placeholder:UIImage(named: "new_logo"))
        }
    }

}
