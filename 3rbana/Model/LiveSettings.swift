//
//  LiveSettings.swift
//  3rbana
//
//  Created by Bilal on 27/04/2021.
//

import Foundation

import Foundation
import AgoraRtcKit

struct LiveSettings {
    var roomName: String?
    var role = AgoraClientRole.broadcaster
    var dimension = CGSize.defaultDimension()
    var frameRate = AgoraVideoFrameRate.defaultValue
}
