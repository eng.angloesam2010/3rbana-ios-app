//
//  UIButton.swift
//  villa vanilla
//
//  Created by Eng Angelo E Saber on 1/20/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation
import UIKit
private var __maxLengths = [UITextField: Int]()
@IBDesignable extension UITextField {
    @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ?
                                                                self.placeholder! : "",
                                                            attributes:[NSAttributedString.Key.foregroundColor : newValue!])
        }
    }
    
    @IBInspectable override var  borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable override var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable override var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
    
    private func getKeyboardLanguage() -> String? {
        return "en" // here you can choose keyboard any way you need
    }
    
    
    
    override open var textInputMode: UITextInputMode? {
        if let language = getKeyboardLanguage() {
            for tim in UITextInputMode.activeInputModes {
                if tim.primaryLanguage!.contains(language) {
                    return tim
                }
            }
        }
        return super.textInputMode
    }
    
    
    
    func setIconRightView(_ image: UIImage) {
        let iconView = UIImageView(frame:
                                    CGRect(x: 5, y: 5, width: 20, height: 20))
        iconView.image = image
        iconView.tintColor = AppTheme.setGoldenColor()
        let iconContainerView: UIView = UIView(frame:
                                                CGRect(x: 30, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        rightView = iconContainerView
        rightViewMode = .always
    }
    
    func setIconleftView(_ image: UIImage) {
        let iconView = UIImageView(frame:
                                    CGRect(x: self.bounds.width - 20, y: 5, width: 20, height: 20))
        iconView.image = image
        iconView.tintColor = AppTheme.setGoldenColor()
        let iconContainerView: UIView = UIView(frame:
                                                CGRect(x: 30, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        rightView = iconContainerView
        rightViewMode = .always
    }
    func setIcon(_ image: UIImage) {
        let iconView = UIImageView(frame:CGRect(x: 10, y: 5, width: 20, height: 20))
        iconView.image = image
        let iconContainerView: UIView = UIView(frame:CGRect(x: 20, y: 0, width: 30, height: 30))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
        
    }
    
    func setPlaceholder(string:String ,color : UIColor) ->NSAttributedString{
        return  NSAttributedString(string:string,attributes: [NSAttributedString.Key.foregroundColor: AppTheme.setGoldenColor()])
    }
}




