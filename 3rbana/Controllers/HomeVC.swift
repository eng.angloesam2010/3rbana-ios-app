//
//  LevelDetailsVC.swift
//  al_Jalboot_app
//
//  Created by Eng Angelo E Saber on 11/17/20.
//

import Foundation
import UIKit
import AnimatableReload
import EmptyKit
import CFAlertViewController



class HomeVC : UIViewController,NavigationBarDelegate,CategoryProtocal {
 
    @IBOutlet weak var mainTableView: UITableView!{
        didSet {
            self.mainTableView?.register(UINib(nibName: SliderCell.identifier, bundle: nil), forCellReuseIdentifier: SliderCell.identifier)
        }
    }
  
    // MARK: - Property of view controller
    @IBOutlet weak var navBarView: NavigationBar!
    // MARK: - init for Level VC
    override func viewDidLoad() {
        super.viewDidLoad()
        initTableView()
        reloadTableView()
       
    }

    // MARK: - set property of custome navigator
    fileprivate func setPropertyTitleForNav() {
        self.navBarView.delegate = self // set delegate for Navigator bar
    }
    // MARK: - init Collectionview
    func initTableView() {
        self.mainTableView?.register(UINib(nibName: SliderCell.identifier, bundle: nil), forCellReuseIdentifier: SliderCell.identifier)
        self.mainTableView.rowHeight = UITableView.automaticDimension
        self.mainTableView.estimatedRowHeight = self.mainTableView.rowHeight
        self.mainTableView.separatorColor     = UIColor.clear
    }
    
    // MARK: - Reload Collectionview
    func reloadTableView() {
        initTableView()
        self.mainTableView?.dataSource = self
        self.mainTableView?.delegate = self
        self.mainTableView?.reloadData()
        self.startAnimation()
        
    }
    // MARK: - Start Animation for view controller
    func startAnimation() {
        AnimatableReload.reload(tableView: self.mainTableView, animationDirection: "down")
    }
    
    func popViewController(){
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    func didSelectIndex(id: Int, categoryID: Int,flag:Int) {
        self.pushViewController(id: id, categoryID: categoryID,flag : flag)
    }
    

    
}

