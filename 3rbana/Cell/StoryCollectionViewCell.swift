//
//  StoryCollectionViewCell.swift
//  3rbana
//
//  Created by Mac on 31/01/2021.
//

import UIKit

class StoryCollectionViewCell : UICollectionViewCell {
   
    @IBOutlet weak var storyItemBttn: UIButton!
    var cellIdentifier = "StoryCollectionViewCell"
    
    @IBOutlet weak var storyImage: UIImageView!
    @IBOutlet weak var storyBttn: UIButton!
    
    static var identifier: String {
        return String(describing: self)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(indexPath:Int) {
        if indexPath != 0 {
            self.storyImage.image = UIImage(named: "dog")
            self.storyBttn.setImage(nil, for: UIControl.State.normal)
        }else{
            self.storyBttn.setImage(UIImage(named: "add_icon"), for: UIControl.State.normal)
        }
      
    }

}
