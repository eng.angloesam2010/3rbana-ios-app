//
//  RootClass.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 30, 2021

import Foundation

struct Country : Codable {

        let body : [Body]?
        let message : String?
        let status : Bool?

        enum CodingKeys: String, CodingKey {
                case body = "body"
                case message = "message"
                case status = "status"
        }
    
        init(from decoder: Decoder) throws {
                let values = try decoder.container(keyedBy: CodingKeys.self)
                body = try values.decodeIfPresent([Body].self, forKey: .body)
                message = try values.decodeIfPresent(String.self, forKey: .message)
                status = try values.decodeIfPresent(Bool.self, forKey: .status)
        }

}
