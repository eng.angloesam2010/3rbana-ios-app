//
//  SliderAvatar.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 14, 2021

import Foundation

struct SliderAvatar : Codable {
    let thumbnailAvatarPath : String?
    enum CodingKeys: String, CodingKey {
        case thumbnailAvatarPath = "thumbnailAvatarPath"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        thumbnailAvatarPath = try values.decodeIfPresent(String.self, forKey: .thumbnailAvatarPath)
    }
    
}
