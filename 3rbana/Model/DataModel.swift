//
//  Datum.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on March 29, 2021

import Foundation

struct DataModel : Codable {
    let message : [String]?
    enum CodingKeys: String, CodingKey {
        case message = "message"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        message = try values.decodeIfPresent([String].self, forKey: .message)
    }
    
}
