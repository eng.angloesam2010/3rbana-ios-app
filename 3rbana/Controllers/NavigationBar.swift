import Foundation
import UIKit
//Write the protocol declaration here:
protocol NavigationBarDelegate {
    func popViewController()
}

protocol NavigationBarProfileDelegate {
    func backViewController()
}


final class NavigationBar : UIView{
    
    var BackButtonImage : String! {
        didSet {
            if BackButtonImage != nil {
                self.BackButton.setImage(UIImage(named: BackButtonImage), for: UIControl.State.normal)
            }
        }
    }
    private static let NIB_NAME = "NavigationBar"
    @IBOutlet private var view: UIView!
    @IBOutlet weak var headerTitle: UILabel!
    @IBOutlet weak var screenTitle: UILabel!

    @IBOutlet private weak var BackButton: UIButton!
    var delegate : NavigationBarDelegate?
    var delegateProfile : NavigationBarProfileDelegate?
    
    var title: String = "" {
        didSet {
            headerTitle.text      = title
            self.headerTitle.font = AppTheme.FontArabic(fontSize: 18.0)
        }
    }
    
    var screenTitleStr: String = "" {
        didSet {
            self.screenTitle.text = screenTitleStr
            self.screenTitle.font = AppTheme.FontArabic(fontSize: 13.0)
        }
    }
    
    var isRightFirstButtonEnabled: Bool {
        set {
            BackButton.isEnabled = newValue
        }
        get {
            return BackButton.isEnabled
        }
    }
    
    override func awakeFromNib() {
        initWithNib()
       
    }
 
    @IBAction func popViewController(_ sender: Any) {
        if BackButtonImage != nil {
            delegateProfile?.backViewController()
        }else{
            delegate?.popViewController()
        }
    }
    
    private func initWithNib() {
        Bundle.main.loadNibNamed(NavigationBar.NIB_NAME, owner: self, options: nil)
        view.translatesAutoresizingMaskIntoConstraints = false
        addSubview(view)
        setupLayout()
        
    }
    
    private func setupLayout() {
        NSLayoutConstraint.activate(
            [
                view.topAnchor.constraint(equalTo: topAnchor),
                view.leadingAnchor.constraint(equalTo: leadingAnchor),
                view.bottomAnchor.constraint(equalTo: bottomAnchor),
                view.trailingAnchor.constraint(equalTo: trailingAnchor),
            ]
        )
    }
}
