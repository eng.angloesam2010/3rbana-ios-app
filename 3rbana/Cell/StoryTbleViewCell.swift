//
//  StoryTblviewCell.swift
//  3rbana
//
//  Created by Mac on 31/01/2021.
//

import UIKit

class StoryTbleViewCell : UITableViewCell , UICollectionViewDelegate,UICollectionViewDataSource {
    
    var cellWidth  : CGFloat = 150.0
    var cellHeight : CGFloat = 150.0

    @IBOutlet weak var storyCollectionView: UICollectionView!
    var identifier = "StoryTbleViewCell"
    static var identifier: String {
        return String(describing: self)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        reloadCollectionView()
    }

    func reloadCollectionView(){
        self.storyCollectionView?.dataSource   = self
        self.storyCollectionView?.delegate     = self
        self.storyCollectionView?.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return 20
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: StoryCollectionViewCell.identifier , for: indexPath) as! StoryCollectionViewCell
        cell.configureCell(indexPath: indexPath.row)
        return cell
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width:cellWidth, height:cellHeight)
    }
    


}

