//
//  Body.swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 30, 2021

import Foundation

struct Body : Codable {
    let arabicName : String?
    let avatar : CountryAvatar?
    let countryId : Int?
    let createdDate : String?
    let currency : String?
    let isActive : Bool?
    let name : String?
    let uuid : String?

        enum CodingKeys: String, CodingKey {
            case arabicName = "arabicName"
            case avatar = "avatar"
            case countryId = "countryId"
            case createdDate = "createdDate"
            case currency = "currency"
            case isActive = "isActive"
            case name = "name"
            case uuid = "uuid"
        }
    
        init(from decoder: Decoder) throws {
            let values  = try decoder.container(keyedBy: CodingKeys.self)
            arabicName  = try values.decodeIfPresent(String.self, forKey: .arabicName)
            avatar      = try values.decodeIfPresent(CountryAvatar.self, forKey: .avatar)
            countryId   = try values.decodeIfPresent(Int.self, forKey: .countryId)
            createdDate = try values.decodeIfPresent(String.self, forKey: .createdDate)
            currency    = try values.decodeIfPresent(String.self, forKey: .currency)
            isActive    = try values.decodeIfPresent(Bool.self, forKey: .isActive)
            name        = try values.decodeIfPresent(String.self, forKey: .name)
            uuid        = try values.decodeIfPresent(String.self, forKey: .uuid)
        }

}
