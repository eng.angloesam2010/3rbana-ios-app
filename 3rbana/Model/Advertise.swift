//
//  Advertise.swift
//  Model Generated using http://www.jsoncafe.com/
//  Created on April 28, 2021

import Foundation
import UIKit

struct Advertise : Codable {
    
    let body : AdvertiseBody?
    let message : String?
    let status : Bool?
    enum CodingKeys: String, CodingKey {
        case body = "body"
        case message = "message"
        case status = "status"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        body = try values.decodeIfPresent(AdvertiseBody.self, forKey: .body)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        status = try values.decodeIfPresent(Bool.self, forKey: .status)
    }
}
