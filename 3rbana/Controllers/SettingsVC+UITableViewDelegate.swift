//
//  SettingsVC+UITableViewDelegate.swift
//  3rbana
//
//  Created by Mac on 06/04/2021.
//

import Foundation
import UIKit

extension SettingsVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            if Session.Username.isEmpty{
                let loginProfileCell = tableView.dequeueReusableCell(withIdentifier:LoginProfileCell.identifier, for: indexPath) as! LoginProfileCell
                loginProfileCell.delegateProfile = self
                loginProfileCell.selectionStyle = UITableViewCell.SelectionStyle.none
                loginProfileCell.clearCellColor()
                loginProfileCell.selectedBackgroundView?.backgroundColor = .clear
                return loginProfileCell
            }else{
                let headerProfileCell = tableView.dequeueReusableCell(withIdentifier:HeaderProfileCell.identifier, for: indexPath) as! HeaderProfileCell
                headerProfileCell.selectionStyle = .none
                headerProfileCell.selectionStyle = UITableViewCell.SelectionStyle.none
                headerProfileCell.delegate = self
                headerProfileCell.configureCell(userNameStr: Session.Username, postNumStr: "", specialStr: "", followerStr: "")
                headerProfileCell.clearCellColor()
                headerProfileCell.selectedBackgroundView?.backgroundColor = .clear
                return headerProfileCell
            }
        }else{
            Session.start()
            Session.getLang()
            let profileCell = tableView.dequeueReusableCell(withIdentifier:ProfileCell.identifier, for: indexPath) as! ProfileCell
            let arr = (Session.Username.isEmpty == true ) ?   ( (Session.Language == "en") ? MenuLoginWithoutArray : MenuLoginWithoutArrayAr ): ((Session.Language == "en") ? MenuLoginArrayEn : MenuLoginArrayAr )
            let iconArr = (Session.Username.isEmpty == true ) ? MenuImageWithoutLogin :   MenuImageLogin
            profileCell.setCellData(cellModel: CellModel(menuTitle:arr[indexPath.row - 1 ], iconName: iconArr[indexPath.row - 1]))
//            profileCell.selectedBackgroundView?.backgroundColor = .clear
            return profileCell
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (Session.Username.isEmpty == true ) ? MenuLoginWithoutArray.count + 1 :   MenuLoginArrayEn.count + 1
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return (Session.Username.isEmpty == true ) ?  282 : 120
        }else{
            return 60.0
        }
    }
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row > 1 {
            self.pushViewController(id: indexPath.row)
        }
    }
    
    
}


